/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk;

import java.util.List;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class Package extends Item {

    private String packageMeasures;

    private List<Integer> packageMeasureCodes;

    private List<Integer> packageMeasureCount;
    
    public Package(Integer code, String title, String description, String description_more, List<Infobox> infoboxes, String packageMeasures) {
	super(code, title, description, description_more, infoboxes);
	this.setPackageMeasures(packageMeasures);

    }

    public String getPackageMeasures() {
        return this.packageMeasures;
    }

    public void setPackageMeasures(String packageMeasures) {
        this.packageMeasures = packageMeasures;
	parsePackageMeasures();
    }

    public List<Integer> getPackageMeasureCodes() {
	return packageMeasureCodes;
    }

    public List<Integer> getPackageMeasureCount() {
	return packageMeasureCount;
    }

    private void parsePackageMeasures() {
	if (this.packageMeasures == null) {
	    return;
	}
	packageMeasureCodes = new ArrayList<Integer>();
	packageMeasureCount = new ArrayList<Integer>();
	final StringTokenizer st = new StringTokenizer(this.packageMeasures, " \t\n\r,", false);
	Integer m = null;
	boolean countMode = false;
	while (st.hasMoreElements()) {
	    final String t = st.nextToken();
	    if (!countMode && t.matches("^\\d\\d\\d$")) {
		try {
		    if (m != null) {
			packageMeasureCodes.add(m);
			packageMeasureCount.add(1);
		    }
		    m = Integer.parseInt(t);
		} catch (NumberFormatException e) {
		}
	    } else if (m != null && t.matches("^[xX]$")) {
		countMode = true;
	    } else if (countMode && t.matches("^\\d+$")) {
		try {
		    int count = Integer.parseInt(t);
		    packageMeasureCodes.add(m);
		    packageMeasureCount.add(count);
		    m = null;
		} catch (NumberFormatException e) {
		}
		countMode = false;
	    } else {
		countMode = false;
	    }
	}
	if (m != null) {
	    packageMeasureCodes.add(m);
	    packageMeasureCount.add(1);
	}
    }
	
}
