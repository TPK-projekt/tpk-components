/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.internal;

import se.kreablo.tpk.Search;
import se.kreablo.tpk.SearchResult;

import java.util.List;
import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import static se.kreablo.tpk.schema.Tables.*;
import se.kreablo.tpk.schema.tables.records.*;

import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;

import org.xwiki.script.service.ScriptService;
import org.xwiki.query.Query;
import org.xwiki.query.QueryException;
import org.xwiki.query.script.QueryManagerScriptService;
import org.xwiki.component.annotation.Component;
import org.xwiki.bridge.DocumentAccessBridge;
import org.xwiki.model.EntityType;
import org.xwiki.model.reference.EntityReference;
import org.xwiki.model.reference.DocumentReference;
import org.xwiki.model.reference.EntityReferenceResolver;

@Component
@Singleton
public class TPKSearch extends AbstractTPKService implements Search {

    @Inject
    @Named("query")
    private ScriptService queryScriptService;

    @Inject
    private EntityReferenceResolver<SolrDocument> solrEntityReferenceResolver;

    @Inject
    private DocumentAccessBridge documentAccessBridge;

    @Override
    public Object search(final String queryString, Integer offset, Integer limit) throws QueryException {
	Query query = ((QueryManagerScriptService) queryScriptService).createQuery(queryString, "solr");
	if (offset != null) {
	    query.setOffset(offset);
	}
	if (limit != null) {
	    query.setLimit(limit);
	}
	final String wikiId = xcontextProvider.get().getWikiId();
	query.bindValue("fq", "wiki:" + wikiId + " AND type:DOCUMENT AND (space:Mottagning~.* OR space:Infosidor~.*)");

	List<QueryResponse> queryResponses = query.execute();

	final SearchResult searchResult = new SearchResult();

	long numFound = 0;

	for (QueryResponse qr : queryResponses) {
	    searchResult.addNumFound(qr.getResults().getNumFound());
	    for (SolrDocument solrDoc : qr.getResults()) {
		EntityReference entityReference = solrEntityReferenceResolver.resolve(solrDoc, EntityType.DOCUMENT);
		entityReference = entityReference.extractReference(EntityType.DOCUMENT);
		if (entityReference != null && entityReference.getType() == EntityType.DOCUMENT) {
		    final DocumentReference documentReference = new DocumentReference(entityReference);
		    final String url = documentAccessBridge.getDocumentURL(documentReference, "view", null, null);
		    final Object id = solrDoc.get("id");
		    final Object title = solrDoc.get("title_sv") != null ? solrDoc.get("title_sv") : solrDoc.get("title_");
		    searchResult.addItem(new SearchResult.Item(id ==  null ?  "" : id.toString(),
							       title == null ? "" : title.toString(),
							       url));
		}
	    }
	}
	return searchResult;
    }
}

