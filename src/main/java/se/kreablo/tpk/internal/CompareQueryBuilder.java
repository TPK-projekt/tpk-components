/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.internal;

import org.jooq.Record;
import org.jooq.Record17;
import org.jooq.SelectSelectStep;
import org.jooq.SelectConditionStep;
import org.jooq.SelectJoinStep;
import org.jooq.SelectGroupByStep;
import org.jooq.SelectOrderByStep;
import org.jooq.SelectHavingStep;
import org.jooq.SelectWhereStep;

public interface CompareQueryBuilder
{

    SelectSelectStep<Record17<Integer, String, String, String, String, String, Double, Double, Integer, Integer[], Integer[], Integer[], Boolean[], String[], Boolean[], String[], Integer>> selectStep();

    SelectJoinStep<Record17<Integer, String, String, String, String, String, Double, Double, Integer, Integer[], Integer[], Integer[], Boolean[], String[], Boolean[], String[], Integer>> joinStep(SelectSelectStep<Record17<Integer, String, String, String, String, String, Double, Double, Integer, Integer[], Integer[], Integer[], Boolean[], String[], Boolean[], String[], Integer>> selectStep);

    SelectHavingStep<Record17<Integer, String, String, String, String, String, Double, Double, Integer, Integer[], Integer[], Integer[], Boolean[], String[], Boolean[], String[], Integer>> groupByStep(SelectGroupByStep<Record17<Integer, String, String, String, String, String, Double, Double, Integer, Integer[], Integer[], Integer[], Boolean[], String[], Boolean[], String[], Integer>> groupByStep);

    SelectConditionStep<Record17<Integer, String, String, String, String, String, Double, Double, Integer, Integer[], Integer[], Integer[], Boolean[], String[], Boolean[], String[], Integer>> conditionStep(SelectWhereStep<Record17<Integer, String, String, String, String, String, Double, Double, Integer, Integer[], Integer[], Integer[], Boolean[], String[], Boolean[], String[], Integer>> whereStep);

    SelectOrderByStep<Record17<Integer, String, String, String, String, String, Double, Double, Integer, Integer[], Integer[], Integer[], Boolean[], String[], Boolean[], String[], Integer>> havingStep(SelectHavingStep<Record17<Integer, String, String, String, String, String, Double, Double, Integer, Integer[], Integer[], Integer[], Boolean[], String[], Boolean[], String[], Integer>> havingStep);
}
