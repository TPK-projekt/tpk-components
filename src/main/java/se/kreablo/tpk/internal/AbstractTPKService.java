/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.internal;

import org.jooq.ConnectionProvider;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.exception.DataAccessException;
import org.jooq.impl.DSL;
import org.jooq.SelectQuery;
import org.jooq.Record1;

import java.sql.Connection;

import javax.inject.Provider;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Map;
import java.util.HashMap;
import java.io.Reader;
import java.io.StringReader;
import java.io.Closeable;

import com.xpn.xwiki.store.XWikiHibernateBaseStore;
import com.xpn.xwiki.store.XWikiCacheStoreInterface;
import com.xpn.xwiki.XWikiContext;
import com.xpn.xwiki.XWikiException;
import org.xwiki.bridge.DocumentAccessBridge;
import org.xwiki.model.reference.DocumentReferenceResolver;


import org.xwiki.rendering.parser.Parser;
import org.xwiki.rendering.renderer.BlockRenderer;
import org.xwiki.rendering.parser.ParseException;
import org.xwiki.rendering.renderer.printer.DefaultWikiPrinter;
import org.xwiki.rendering.renderer.printer.WikiPrinter;


import se.kreablo.tpk.TextRenderer;
import se.kreablo.tpk.RendererException;
import static se.kreablo.tpk.schema.Tables.*;
import se.kreablo.tpk.schema.tables.records.*;
import static se.kreablo.tpk.Constants.VERSION_DOCUMENT;

import org.slf4j.Logger;

public class AbstractTPKService implements TextRenderer, Closeable {

    @Inject
    protected Provider<XWikiContext> xcontextProvider;

    @Inject
    @Named("xwiki/2.1")
    private Parser parser;

    @Inject
    @Named("xhtml/1.0")
    private BlockRenderer blockRenderer;

    @Inject
    private DocumentAccessBridge documentAccessBridge;

    @Inject
    private DocumentReferenceResolver<String> documentReferenceResolver;

    @Inject
    private Logger logger;

    private DSLContext ctx;

    /**
     * Create a jOOQ context.
     * @param commitTransaction Setup a transaction on each connection that will be
     *                        committed whe connection is closed.
     */ 
    protected DSLContext create(final boolean commitTransaction) {

        final XWikiHibernateBaseStore store = 
            ((XWikiHibernateBaseStore) ((XWikiCacheStoreInterface) xcontextProvider.get()
                                        .getWiki()
                                        .getStore())
             .getStore());

	    
        return DSL.using(new ConnectionProvider()  {
                @Override
                public Connection acquire() throws DataAccessException {
                    try {
                        logger.trace("acquiring connection {}", commitTransaction);
                        store.beginTransaction(xcontextProvider.get());
                        return store.getSession(xcontextProvider.get())
                            .connection();
                    } catch (Exception e) {
                        throw new DataAccessException("When acquiring connection", e);
                    }
                }

                @Override
                public void release(Connection connection) throws DataAccessException {
                    logger.trace("releasing connection");
                    store.endTransaction(xcontextProvider.get(), commitTransaction);
                }
            }, SQLDialect.POSTGRES);
    }

    /** @return A new jOOQ context. */
    protected DSLContext create() {
        return create(false);
    }
    
    /**
     * @return the DSLContext for constructing queries.
     */
    protected DSLContext getContext() {
        return getContext(false);
    }

    /**
     * @param commithTransaction to commit the transaction when the connection is closed.
     * @return the DSLContext for constructing queries.
     */
    protected DSLContext getContext(boolean commitTransaction) {
        if (ctx == null) {
            ctx = create(commitTransaction);
        }
        return ctx;
    }

    /**
     * Close the current jOOQ context to actually commit.
     */
    protected void closeContext() {
        if (ctx != null) {
            logger.trace("closing jOOQ context");
            try {
                ctx.close();
            } catch (RuntimeException e) {
                logger.error("Exception when closing jooq context", e);
                throw e;
            }
            ctx = null;
        }
    }
    
    @Override
    public void close() {
        closeContext();
    }

    /** @return the logger. */
    protected Logger getLogger() {
        return logger;
    }

    protected SelectQuery<Record1<Integer>> versionQuery(DSLContext create) {
        return create
            .select(TVPRIS_VERSIONCLASS.TPKV_VERSION)
            .from(TVPRIS_VERSIONCLASS)
            .join(XWIKIOBJECTS)
            .using(XWIKIOBJECTS.XWO_ID)
            .where(XWIKIOBJECTS.XWO_NAME.eq(VERSION_DOCUMENT))
            .getQuery();
    }


    @Override
    public String renderText(String text, String contextDoc) throws RendererException {
        if (text == null) {
            return null;
        }
        final Map<String, Object> backup = new HashMap<String, Object>();
        try {
            documentAccessBridge.pushDocumentInContext(backup, documentReferenceResolver.resolve(contextDoc));
        } catch (Exception e) {
            throw new RendererException(e.getMessage());
        }
        try {
            WikiPrinter printer = new DefaultWikiPrinter();
            Reader reader = new StringReader(text);
            blockRenderer.render(parser.parse(reader), printer);
            return printer.toString();
        } catch (ParseException e) {
            throw new RendererException(e.getMessage());
        } finally {
            documentAccessBridge.popDocumentFromContext(backup);
        }
    }
}
