/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.internal;

import org.xwiki.component.annotation.Component;
import org.xwiki.query.QueryException;

import org.jooq.Record3;
import org.jooq.Result;
import org.jooq.SelectLimitStep;
import org.jooq.SelectFinalStep;
import org.jooq.SelectUnionStep;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.impl.DSL;

import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import javax.inject.Inject;
import javax.inject.Singleton;

import se.kreablo.tpk.RegionQuery;
import se.kreablo.tpk.RegionResult;

import static se.kreablo.tpk.schema.Tables.*;
import se.kreablo.tpk.schema.tables.records.*;
import static se.kreablo.tpk.Constants.*;

@Component
@Singleton
public class TPKRegionQuery extends AbstractTPKService implements RegionQuery
{

    private SelectLimitStep<Record3<String, String, String>> query() {
        DSLContext ctx = getContext();
        final Condition isLargeMunicipality =
            TVPRIS_SURGERY.TVPS_MUNICIPALITY.in(ctx.select(TPK_LARGE_MUNICIPALITIES.TLM_MUNICIPALITY)
                                                .from(TPK_LARGE_MUNICIPALITIES)
                                                .where(TVPRIS_SURGERY.TVPS_MUNICIPALITY
                                                       .equal(TPK_LARGE_MUNICIPALITIES.TLM_MUNICIPALITY)));

        final Field<String> countyField = TVPRIS_SURGERY.TVPS_COUNTY.as("county");
        final Field<String> municipalityField = TVPRIS_SURGERY.TVPS_MUNICIPALITY.as("municipality");
        final Field<String> sublocalityField =
            DSL.choose()
            .when(isLargeMunicipality,
                  TVPRIS_SURGERY.TVPS_SUBLOCALITY)
            .otherwise("").as("sublocality");
        
        final SelectUnionStep<Record3<String, String, String>> selectRegions = ctx.selectDistinct(countyField, municipalityField, DSL.val("").as("sublocality"))
            .from(TVPRIS_SURGERY)
            .where(TVPRIS_SURGERY.TVPS_COUNTY.notEqual(""));
        
        final SelectUnionStep<Record3<String, String, String>> selectLargeMunicipalities =
            ctx.selectDistinct(countyField, municipalityField, sublocalityField)
            .from(TVPRIS_SURGERY)
            .innerJoin(TPK_LARGE_MUNICIPALITIES).on(TVPRIS_SURGERY.TVPS_MUNICIPALITY.eq(TPK_LARGE_MUNICIPALITIES.TLM_MUNICIPALITY))
            .where(isLargeMunicipality.and(TVPRIS_SURGERY.TVPS_SUBLOCALITY.isNotNull()
                                           .and(TVPRIS_SURGERY.TVPS_SUBLOCALITY.notEqual("")))
                   .and(TVPRIS_SURGERY.TVPS_COUNTY.notEqual("")));

        return selectRegions.union(selectLargeMunicipalities).orderBy(1, 2, 3);
    }
    
    private Result<Record3<String, String, String>> search_(final String q) throws QueryException
    {
        final SelectFinalStep<Record3<String, String, String>> query = query();
        query.execute();
        return query.getResult();
    }

    @Override
    public Iterable<RegionResult> search(final String query) throws QueryException {
        try {
            final Result<Record3<String, String, String>> result = search_(query);

            final List<RegionResult> list = new ArrayList(result.size());

            final Iterable<RegionResult> iter = new Iterable<RegionResult>() {
                    @Override
                    public Iterator<RegionResult> iterator() {

                        final Iterator<Record3<String, String, String>> records = result.iterator();

                        return new Iterator<RegionResult>() {
                            @Override
                            public RegionResult next() {
                                final Record3<String, String, String> r = records.next();
                                return new RegionResult(r.component1(), r.component2(), r.component3());
                            }

                            @Override
                            public boolean hasNext() {
                                return records.hasNext();
                            }

                            @Override
                            public void remove() {
                                records.remove();
                            }

                        };
                    }
                };

            for (RegionResult r : iter) {
                list.add(r);
            }

            return list;
        } catch (RuntimeException e) {
            getLogger().error("Exception when querying regions.", e);
            throw e;
        } finally {
            close();
        }
    }
}
