/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.internal;

import se.kreablo.tpk.Compare;
import se.kreablo.tpk.CompareResult;
import se.kreablo.tpk.Parameters;
import static se.kreablo.tpk.Constants.*;

import org.xwiki.component.annotation.Component;

import org.jooq.DSLContext;
import org.jooq.impl.DSL;
import org.jooq.TableRecord;
import org.jooq.Condition;
import org.jooq.SortField;
import org.jooq.Table;
import org.jooq.Field;
import org.jooq.Result;
import org.jooq.Record;
import org.jooq.Record2;
import org.jooq.Record17;
import org.jooq.SelectSelectStep;
import org.jooq.SelectJoinStep;
import org.jooq.SelectConditionStep;
import org.jooq.SelectOrderByStep;
import org.jooq.SelectLimitStep;
import org.jooq.SelectHavingStep;
import org.jooq.SelectFinalStep;
import org.jooq.SortOrder;

import javax.inject.Inject;
import javax.inject.Singleton;

import java.util.Iterator;
import java.util.Map;
import java.util.HashMap;


import static se.kreablo.tpk.schema.Tables.*;
import se.kreablo.tpk.schema.tables.records.*;

@Component
@Singleton
public class TPKCompare extends AbstractTPKService implements Compare {

    private CompareQueryBuilder queryBuilderFactory(QueryContext ctx) {
        return ctx.isSingle() ? new SingleCompareQueryBuilder(ctx) : new MultiCompareQueryBuilder(ctx);
    }

    void initSurgeryItemAreas(QueryContext ctx) {
        ctx.setAreaField(ctx.getParameters().isSpecial() ?
                         TVPRIS_MEASURE.TVPM_AREA_SPECIAL :
                         TVPRIS_ITEM.TVPI_AREA);
        ctx.setSurgeryItemAreas(ctx.getCreate()
                                 .select(ctx.getAreaField())
                                 .from(TPK_ITEMPRICE)
                                 .innerJoin(TVPRIS_ITEM).on(TPK_ITEMPRICE.TVPIP_ITEM_ID.equal(TVPRIS_ITEM.TVPI_CODE)
                                                            .and(TPK_ITEMPRICE.TVPIP_IS_MEASURE.equal(TVPRIS_ITEM.IS_MEASURE))
                                                            .and(TVPRIS_ITEM.TVPI_VERSION.equal(ctx.getVersionField())))
                                 .innerJoin(TVPRIS_MEASURE).using(TVPRIS_MEASURE.XWO_ID)
                                 .where(TVPRIS_SURGERY.TVPS_SURGERY_ID.equal(TPK_ITEMPRICE.TVPIP_SURGERY_ID)
                                        .and(ctx.getVersionField().cast(Long.class).equal(TPK_ITEMPRICE.TVPIP_VERSION)))
                                .getQuery());

        final Table<Record2<Integer, Long>> surgeryPackageAreas = ctx.getCreate()
            .select(TPK_ITEMPRICE.TVPIP_SURGERY_ID, TPK_PACKAGE_AREAS.XWO_ID)
            .from(TPK_ITEMPRICE)
            .join(TVPRIS_ITEM).on(
                TVPRIS_ITEM.TVPI_VERSION.cast(Long.class).equal(TPK_ITEMPRICE.TVPIP_VERSION)
                .and(TPK_ITEMPRICE.TVPIP_IS_MEASURE.equal(TVPRIS_ITEM.IS_MEASURE))
                .and(TPK_ITEMPRICE.TVPIP_ITEM_ID.equal(TVPRIS_ITEM.TVPI_CODE)))
            .join(TPK_PACKAGE_AREAS).on(ctx.getAreaField().equal(TPK_PACKAGE_AREAS.TPA_AREA))
            .getQuery().asTable();

        final Field<Boolean> areaActive = DSL.when(TPK_ITEMPRICE.TVPIP_PRICE.isNotNull(), true)
            .when(DSL.condition(TVPRIS_ITEM.IS_MEASURE)
                  .and(ctx.getAreaField().in(ctx.getSurgeryItemAreas())), true)
            .when(DSL.not(DSL.condition(TVPRIS_ITEM.IS_MEASURE))
                  .and(DSL.exists(ctx.getCreate().select().from(surgeryPackageAreas)
                                  .where(surgeryPackageAreas.field(0, Integer.class).equal(TVPRIS_SURGERY.TVPS_SURGERY_ID)
                                         .and(surgeryPackageAreas.field(1, Long.class).equal(TVPRIS_ITEM.XWO_ID))))), true)
            .otherwise(false);

        ctx.setIsAreaActive(areaActive);
    }

    private Result<Record17<Integer, String, String, String, String, String, Double, Double, Integer, Integer[], Integer[], Integer[], Boolean[], String[], Boolean[], String[], Integer>> compare_(final Parameters parameters) {
        int n = parameters.getPackages().size() + parameters.getMeasures().size();

        if (n == 0) {
            parameters.addPackage(DEFAULT_PACKAGE);
            n = 1;
        }

        final int numObjects = n;
        
        final QueryContext ctx = new QueryContext(parameters, create(), numObjects == 1);

        ctx.setVersionTable(versionQuery(ctx.getCreate()).asTable());
        ctx.setVersionField(ctx.getVersionTable().field(0, Integer.class));
        ctx.setSurgeryAddressField(TVPRIS_SURGERYEDIT.TVPSE_ADDRESS.as("s_address"));
        ctx.setZipcodeField(TVPRIS_SURGERYEDIT.TVPSE_ZIPCODE.as("s_zipcode"));
        ctx.setCityField(TVPRIS_SURGERYEDIT.TVPSE_CITY.as("s_city"));
        ctx.setSurgeryDescriptionField(TVPRIS_SURGERYEDIT.TVPSE_DESCRIPTION.as("s_description"));
        ctx.setLongitudeField(TVPRIS_SURGERYEDIT.TVPSE_LONGITUDE.as("lon"));
        ctx.setLatitudeField(TVPRIS_SURGERYEDIT.TVPSE_LATITUDE.as("lat"));
        ctx.setToplevelCommentTable(TPK_AREACOMMENTCLASS.as("toplevel"));
        ctx.setCommentField(DSL.nvl(TVPRIS_PUBLICPRICECOMMENT.TVPPC_RENDERED_TEXT,
                                    DSL.nvl(TPK_AREACOMMENTCLASS.TPKAC_RENDERED_TEXT,
                                            ctx.getToplevelCommentTable().TPKAC_RENDERED_TEXT)));
            

        ctx.setSurgeryIdField(TVPRIS_SURGERY.TVPS_SURGERY_ID.as("s_id"));

        ctx.setSurgeryNameField(DSL.when(TVPRIS_SURGERYEDIT.TVPSE_NAME.isNull()
                                         .or(TVPRIS_SURGERYEDIT.TVPSE_NAME.eq("")),
                                         TVPRIS_SURGERY.TVPS_NAME)
                                .otherwise(TVPRIS_SURGERYEDIT.TVPSE_NAME).as("s_name"));

        initSurgeryItemAreas(ctx);

        final CompareQueryBuilder queryBuilder = queryBuilderFactory(ctx);

        final SelectSelectStep<Record17<Integer, String, String, String, String, String, Double, Double, Integer, Integer[], Integer[], Integer[], Boolean[], String[], Boolean[], String[], Integer>> select
            = queryBuilder.selectStep();

        final SelectJoinStep<Record17<Integer, String, String, String, String, String, Double, Double, Integer, Integer[], Integer[], Integer[], Boolean[], String[], Boolean[], String[], Integer>>
            join = queryBuilder.joinStep(select);

        SelectConditionStep<Record17<Integer, String, String, String, String, String, Double, Double, Integer, Integer[], Integer[], Integer[], Boolean[], String[], Boolean[], String[], Integer>> where =
            queryBuilder.conditionStep(join);

        if (parameters.getSurgeries().size() > 0) {
            where = where.and(TVPRIS_SURGERY.TVPS_SURGERY_ID.in(parameters.getSurgeries()));
        }

        if (parameters.getSublocalities().size() > 0) {
            where = where.and(TVPRIS_SURGERY.TVPS_SUBLOCALITY.in(parameters.getSublocalities()));
        }

        if (parameters.getMunicipalities().size() > 0) {
            where = where.and(TVPRIS_SURGERY.TVPS_MUNICIPALITY.in(parameters.getMunicipalities()));
        }

        if (parameters.getCounties().size() > 0) {
            where = where.and(TVPRIS_SURGERY.TVPS_COUNTY.in(parameters.getCounties()));
        }

        if (parameters.getEveningHours() != null && parameters.getEveningHours()) {
            where = where.and(TVPRIS_SURGERYEDIT.TVPSE_EVENING_HOURS.eq(1));
        }
        
        if (parameters.getWeekendHours() != null && parameters.getWeekendHours()) {
            where = where.and(TVPRIS_SURGERYEDIT.TVPSE_WEEKEND_HOURS.eq(1));
        }

        if (parameters.getSurgeryCategory() != null) {
            where = where.and(TVPRIS_SURGERY.TVPS_CATEGORY.eq(parameters.getSurgeryCategory().equals("privat") ?
                                                              "Privat" : "Landsting"));
        }

        if (parameters.getHaveParking() != null && parameters.getHaveParking()) {
            where = where.and(TVPRIS_SURGERYEDIT.TVPSE_HAVE_PARKING.eq(1));
        }

        if (parameters.getPublicTransport() != null && parameters.getPublicTransport()) {
            where = where.and(TVPRIS_SURGERYEDIT.TVPSE_PUBLIC_TRANSPORT.eq(1));
        }

        if (parameters.getDentalFear() != null && parameters.getDentalFear()) {
            where = where.and(TVPRIS_SURGERYEDIT.TVPSE_DENTAL_FEAR.eq(1));
        }

        if (parameters.getWarranties() != null && parameters.getWarranties()) {
            where = where.and(TVPRIS_SURGERYEDIT.TVPSE_WARRANTY.eq(1));
        }

        final SelectHavingStep<Record17<Integer, String, String, String, String, String, Double, Double, Integer, Integer[], Integer[], Integer[], Boolean[], String[], Boolean[], String[], Integer>>
            having = queryBuilder.groupByStep(where);

        SelectOrderByStep<Record17<Integer, String, String, String, String, String, Double, Double, Integer, Integer[], Integer[], Integer[], Boolean[], String[], Boolean[], String[], Integer>> orderBy = queryBuilder.havingStep(having);

        SortOrder sortOrder = SortOrder.ASC;

        if (parameters.getSortOrder() != null) {
            switch (parameters.getSortOrder()) {
            case ASC:
                sortOrder = SortOrder.ASC;
                break;
            case DESC:
                sortOrder = SortOrder.DESC;
                break;
            case DEFAULT:
                sortOrder = SortOrder.DEFAULT;
                break;
            }
        }

        SelectLimitStep<Record17<Integer, String, String, String, String, String, Double, Double, Integer, Integer[], Integer[], Integer[], Boolean[], String[], Boolean[], String[], Integer>> limit = null;

        final SortField<Integer> no = ctx.getNoPriceItems().sort(SortOrder.DESC);

        if (parameters.getOrderBy() != null) {
            switch (parameters.getOrderBy()) {
            case PRICE:
                limit = orderBy.orderBy(no, ctx.getPriceTotal().sort(sortOrder));
                break;
            case SURGERY_NAME:
                limit = orderBy.orderBy(ctx.getSurgeryNameField().sort(sortOrder));
                break;
            }
        } else {
            limit = orderBy.orderBy(no, ctx.getPriceTotal().sort(sortOrder));
        }

        SelectFinalStep<Record17<Integer, String, String, String, String, String, Double, Double, Integer, Integer[], Integer[], Integer[], Boolean[], String[], Boolean[], String[], Integer>> finalQuery = limit;

        getLogger().debug("Final query: " + finalQuery.getSQL());

        if (parameters.getCount() == null) {
            int count = ctx.getCreate().fetchCount(finalQuery);
            parameters.setCount(count);
        }

        if (parameters.getOffset() != null) {
            if (parameters.getLimit() != null) {
                finalQuery = limit.limit(parameters.getOffset(), parameters.getLimit());
            } else {
                finalQuery = limit.offset(parameters.getOffset());
            }
        } else if (parameters.getLimit() != null) {
            finalQuery = limit.limit(parameters.getLimit());
        }

        finalQuery.execute();
        return finalQuery.getResult();
    }

    @Override
    public Iterable<CompareResult> compare(final Parameters parameters) {

        final Result<Record17<Integer, String, String, String, String, String, Double, Double, Integer, Integer[], Integer[], Integer[], Boolean[], String[], Boolean[], String[], Integer>>
            result = compare_(parameters);
        
        return new Iterable<CompareResult>() {
            @Override
            public Iterator<CompareResult> iterator() {
                final Iterator<Record17<Integer, String, String, String, String, String, Double, Double, Integer, Integer[], Integer[], Integer[], Boolean[], String[], Boolean[], String[], Integer>> records =
                    result.iterator();

                return new Iterator<CompareResult>() {
                    @Override
                    public CompareResult next() {
                        final Record17<Integer, String, String, String, String, String, Double, Double, Integer, Integer[], Integer[], Integer[], Boolean[], String[], Boolean[], String[], Integer> r =
                            records.next();
                        return new CompareResult(TPKCompare.this,
                                                 r.component1(),
                                                 r.component2(),
                                                 r.component3(),
                                                 r.component4(),
                                                 r.component5(),
                                                 r.component6(),
                                                 r.component7(),
                                                 r.component8(),
                                                 r.component9(),
                                                 r.component10(),
                                                 r.component11(),
                                                 r.component12(),
                                                 r.component13(),
                                                 r.component14(),
                                                 r.component15(),
                                                 r.component16(),
                                                 parameters.getCount());
                    }

                    @Override
                    public boolean hasNext() {
                        return records.hasNext();
                    }

                    @Override
                    public void remove() {
                        records.remove();
                    }
                };
            }
        };
    }
}
