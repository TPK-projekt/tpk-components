/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.internal;

import org.jooq.Condition;
import org.jooq.Field;
import org.jooq.Record2;
import org.jooq.Record17;
import org.jooq.SelectSelectStep;
import org.jooq.SelectConditionStep;
import org.jooq.SelectJoinStep;
import org.jooq.SelectGroupByStep;
import org.jooq.SelectHavingStep;
import org.jooq.SelectWhereStep;
import org.jooq.SelectOrderByStep;
import org.jooq.Table;
import org.jooq.impl.DSL;

import static se.kreablo.tpk.schema.Tables.*;
import se.kreablo.tpk.schema.tables.records.*;

public class MultiCompareQueryBuilder extends AbstractCompareQueryBuilder
{

    public MultiCompareQueryBuilder(QueryContext ctx) {
        super(ctx);
    }
    
    @Override
    public SelectSelectStep<Record17<Integer, String, String, String, String, String, Double, Double, Integer, Integer[], Integer[], Integer[], Boolean[], String[], Boolean[], String[], Integer>> selectStep() {

        getCtx().setPriceTotal(DSL.sum(TPK_ITEMPRICE.TVPIP_PRICE).cast(Integer.class).as("price_total"));
        getCtx().setNoPriceItems(DSL.count(TPK_ITEMPRICE.TVPIP_PRICE).as("n"));

        return  getCtx().getCreate()
            .select(getCtx().getSurgeryIdField(),
                    getCtx().getSurgeryNameField(),
                    getCtx().getSurgeryAddressField(),
                    getCtx().getZipcodeField(),
                    getCtx().getCityField(),
                    getCtx().getSurgeryDescriptionField(),
                    getCtx().getLongitudeField(),
                    getCtx().getLatitudeField(),
                    getCtx().getPriceTotal(),
                    DSL.arrayAgg((getCtx().getParameters().isSpecial() ?
                                  TVPRIS_ITEM.TVPI_PRICE_SPECIAL :
                                  TVPRIS_ITEM.TVPI_PRICE)).as("ref_price"),
                    DSL.arrayAgg(TPK_ITEMPRICE.TVPIP_ITEM_ID).as("items"),
                    DSL.arrayAgg(TVPRIS_ITEM.TVPI_CODE).as("ref_items"),
                    DSL.arrayAgg(TVPRIS_ITEM.IS_MEASURE).as("is_m"),
                    DSL.arrayAgg(getCtx().getCommentField()).as("comments"),
                    DSL.arrayAgg(getCtx().getIsAreaActive()).as("area_active"),
                    DSL.arrayAgg(getCtx().getAreaField()).as("areas"),
                    getCtx().getNoPriceItems()
                    );
    }

    @Override
    public SelectHavingStep<Record17<Integer, String, String, String, String, String, Double, Double, Integer, Integer[], Integer[], Integer[], Boolean[], String[], Boolean[], String[], Integer>>
        groupByStep(SelectGroupByStep<Record17<Integer, String, String, String, String, String, Double, Double, Integer, Integer[], Integer[], Integer[], Boolean[], String[], Boolean[], String[], Integer>> groupByStep)
    {
        return groupByStep.groupBy(getCtx().getSurgeryIdField(),
                                   getCtx().getSurgeryNameField(),
                                   getCtx().getSurgeryAddressField(),
                                   getCtx().getZipcodeField(),
                                   getCtx().getCityField(),
                                   getCtx().getSurgeryDescriptionField(),
                                   getCtx().getLongitudeField(),
                                   getCtx().getLatitudeField());
    }


    @Override
    public SelectConditionStep<Record17<Integer, String, String, String, String, String, Double, Double, Integer, Integer[], Integer[], Integer[], Boolean[], String[], Boolean[], String[], Integer>>
        conditionStep(SelectWhereStep<Record17<Integer, String, String, String, String, String, Double, Double, Integer, Integer[], Integer[], Integer[], Boolean[], String[], Boolean[], String[], Integer>> whereStep) {

        Condition packages = null;
        Condition measures = null;
        Condition condition = null;
		
        if (getCtx().getParameters().getPackages().size() > 0) {
            packages = TVPRIS_ITEM.IS_MEASURE.isFalse().and(TVPRIS_ITEM.TVPI_CODE.in(getCtx().getParameters().getPackages()));
        }

        if (getCtx().getParameters().getMeasures().size() > 0) {
            measures = TVPRIS_ITEM.IS_MEASURE.isTrue().and(TVPRIS_ITEM.TVPI_CODE.in(getCtx().getParameters().getMeasures()));
        }

        if (packages != null && measures != null) {
            condition = measures.or(packages);
        } else if (packages != null) {
            condition = packages;
        } else {
            condition = measures;
        }
		
        return whereStep.where(condition);
    }

    @Override
    public SelectOrderByStep<Record17<Integer, String, String, String, String, String, Double, Double, Integer, Integer[], Integer[], Integer[], Boolean[], String[], Boolean[], String[], Integer>> havingStep(SelectHavingStep<Record17<Integer, String, String, String, String, String, Double, Double, Integer, Integer[], Integer[], Integer[], Boolean[], String[], Boolean[], String[], Integer>> havingStep) {
        return havingStep.having(DSL.arrayAgg(getCtx().getIsAreaActive()).contains(new Boolean [] {true}));

    }
}
