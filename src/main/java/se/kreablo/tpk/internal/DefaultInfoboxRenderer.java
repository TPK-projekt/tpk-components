/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.internal;

import se.kreablo.tpk.Infobox;
import se.kreablo.tpk.Measure;
import se.kreablo.tpk.Package;
import se.kreablo.tpk.InfoboxRenderer;
import se.kreablo.tpk.InfoboxRendererException;
import se.kreablo.tpk.RendererException;

import org.xwiki.component.annotation.Component;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.inject.Singleton;

import org.jooq.Record1;
import org.jooq.Record5;
import org.jooq.Record6;
import org.jooq.Record12;
import org.jooq.Result;
import org.jooq.Field;
import org.jooq.Table;
import org.jooq.DSLContext;
import org.jooq.impl.DSL;
import java.sql.Timestamp;

import java.util.SortedSet;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Date;

import static se.kreablo.tpk.schema.Tables.*;
import se.kreablo.tpk.schema.tables.records.*;

@Component
@Singleton
public class DefaultInfoboxRenderer extends AbstractTPKService implements InfoboxRenderer {

    @Override
    public Object renderInfoboxes(SortedSet<String> infoboxes) throws InfoboxRendererException, RendererException {
        final Result<Record5<String, String, String, String, Timestamp>> result = create()
            .select(TPK_INFOTEXT.TINFO_CODE,
                    TPK_INFOTEXT.TINFO_TITLE,
                    TPK_INFOTEXT.TINFO_TEXT,
                    XWIKIOBJECTS.XWO_NAME,
                    XWIKIDOC.XWD_CONTENT_UPDATE_DATE)
            .from(TPK_INFOTEXT.TPK_INFOTEXT)
            .innerJoin(XWIKIOBJECTS.XWIKIOBJECTS)
	        .on(XWIKIOBJECTS.XWO_ID.equal(TPK_INFOTEXT.XWO_ID))
            .innerJoin(XWIKIDOC).on(XWIKIOBJECTS.XWO_NAME.equal(XWIKIDOC.XWD_FULLNAME))
            .where(TPK_INFOTEXT.TINFO_CODE.in(infoboxes.toArray(new String[0])))
            .fetch();

        final List<String> codes = result.getValues(TPK_INFOTEXT.TINFO_CODE);
        final List<String> titles = result.getValues(TPK_INFOTEXT.TINFO_TITLE);
        final List<String> texts = result.getValues(TPK_INFOTEXT.TINFO_TEXT);
        final List<String> renderedTexts = new ArrayList<String>(texts.size());
        final List<String> names = result.getValues(XWIKIOBJECTS.XWO_NAME);
        final List<? extends Date> timestamps = result.getValues(XWIKIDOC.XWD_CONTENT_UPDATE_DATE);
	
        for (int i = 0; i < texts.size(); i++) {
            final String text = texts.get(i);
            final String name = names.get(i);
            renderedTexts.add(renderText(text, name));
        }

        if (codes.size() != titles.size() || titles.size() != renderedTexts.size()) {
            throw new InfoboxRendererException("Unequally sized lists!");
        }

        final List<Infobox> ret = new ArrayList<Infobox>(codes.size());
        for (int i = 0; i < codes.size(); i++) {
            ret.add(new Infobox(codes.get(i), titles.get(i), renderedTexts.get(i), timestamps.get(i)));
        }

        return ret;
    }

    @Override
    public Map<String, List<Infobox>> renderMAreaInfoboxes(SortedSet<String> mareas) throws InfoboxRendererException, RendererException {
        final Field<String> area = DSL.when(TVPRIS_ITEM.TVPI_AREA.in(mareas), TVPRIS_ITEM.TVPI_AREA)
            .otherwise(TVPRIS_MEASURE.TVPM_AREA_SPECIAL).as("marea");
        final Field<String[]> code = DSL.arrayAgg(TPK_LINKABLEINFOTEXT.TLIT_CODE).as("code");
        final Field<String[]> title = DSL.arrayAgg(TPK_LINKABLEINFOTEXT.TLIT_TITLE).as("title");
        final Field<String[]> text = DSL.arrayAgg(TPK_LINKABLEINFOTEXT.TLIT_TEXT).as("text");
        final Field<String[]> name = DSL.arrayAgg(XWIKIOBJECTS.XWO_NAME).as("name");
        final Field<Timestamp[]> timestamp = DSL.arrayAgg(XWIKIDOC.XWD_CONTENT_UPDATE_DATE).as("timestamp");

        Result<Record6<String, String[], String[], String[], String[], Timestamp[]>> result = create()
            .select(area, code, title, text, name, timestamp)
            .from(TVPRIS_ITEM)
            .innerJoin(TVPRIS_MEASURE).on(TVPRIS_MEASURE.XWO_ID.equal(TVPRIS_ITEM.XWO_ID))
            .innerJoin(TPK_ITEMREFERENCE).on(TPK_ITEMREFERENCE.TPKIR_TYPE.eq("marea")
                                             .and(TPK_ITEMREFERENCE.TPKIR_CODE.equal(TVPRIS_ITEM.TVPI_AREA)
                                                  .or(TPK_ITEMREFERENCE.TPKIR_CODE.equal(TVPRIS_MEASURE.TVPM_AREA_SPECIAL))))
            .innerJoin(TPK_LINKABLEINFOTEXT).on(TPK_LINKABLEINFOTEXT.TLIT_CODE.equal(TPK_ITEMREFERENCE.TPKIR_INFOCODE))
            .innerJoin(XWIKIOBJECTS.XWIKIOBJECTS).on(XWIKIOBJECTS.XWO_ID.equal(TPK_LINKABLEINFOTEXT.XWO_ID))
            .innerJoin(XWIKIDOC).on(XWIKIOBJECTS.XWO_NAME.equal(XWIKIDOC.XWD_FULLNAME))
            .groupBy(area)
            .fetch();

        final List<String> areas = result.getValues(area);
        final List<String []> codes = result.getValues(code);
        final List<String []> titles = result.getValues(title);
        final List<String []> texts = result.getValues(text);
        final List<String []> names = result.getValues(name);
        final List<Timestamp []> timestamps = result.getValues(timestamp);

        if (areas.size() != codes.size() || codes.size() != titles.size() || titles.size() != texts.size()) {
            throw new InfoboxRendererException("Unequally sized lists!");
        }

        final Map<String, List<Infobox>> ret = new HashMap<String, List<Infobox>>();

        for (int i = 0; i < areas.size(); i++) {
            String[] c = codes.get(i);
            String[] t = titles.get(i);
            String[] txt = texts.get(i);
            String[] n = names.get(i);
            Date[] ts = timestamps.get(i);
            final List<Infobox> infoboxes = new ArrayList<Infobox>(c.length);
            for (int j = 0; j < c.length; j++) {
                infoboxes.add(new Infobox(c[j], t[j], renderText(txt[j], n[j]), ts[j]));
            }
            ret.put(areas.get(i), infoboxes);
        }

        return ret;
    }

    @Override
    public Map<Integer, Package> renderPackageInfoboxes(SortedSet<Integer> packages) throws InfoboxRendererException, RendererException {
        final Field<Integer> packageField = TVPRIS_ITEM.TVPI_CODE.as("package");
        final Field<String[]> code = DSL.arrayAgg(TPK_LINKABLEINFOTEXT.TLIT_CODE).as("code");
        final Field<String[]> title = DSL.arrayAgg(TPK_LINKABLEINFOTEXT.TLIT_TITLE).as("title");
        final Field<String[]> text = DSL.arrayAgg(TPK_LINKABLEINFOTEXT.TLIT_TEXT).as("text");
        final Field<String[]> name = DSL.arrayAgg(XWIKIOBJECTS.XWO_NAME).as("name");
        final Field<Timestamp[]> timestamp = DSL.arrayAgg(XWIKIDOC.XWD_CONTENT_UPDATE_DATE);
        final Field<Integer> item_code = TVPRIS_ITEM.TVPI_CODE.as("item_code");
        final Field<String> item_title = TVPRIS_ITEM.TVPI_TITLE.as("item_title");
        final Table<XwikiobjectsRecord> itemObjectTable = XWIKIOBJECTS.as("item_object");
        final Field<String> item_description = TVPRIS_ITEM.TVPI_DESCRIPTION.as("item_description");
        final Field<String> item_description_more = TVPRIS_ITEM.TVPI_DESCRIPTION.as("item_description_more");
        final Field<String> item_name = itemObjectTable.field("xwo_name", String.class).as("item_name");
        final Field<String> package_measures = TVPRIS_PACKAGE.TVPP_MEASURES.as("package_measures");

        final DSLContext ctx = create();
        final Table<Record1<Integer>> versionTable = versionQuery(ctx).asTable();
        final Field<Integer> versionField = versionTable.field(0, Integer.class);

        Result<Record12<Integer, Integer, String, String, String, String, String[], String[], String[], String[], Timestamp[], String>> result = create()
            .select(item_code, packageField, item_title, item_description, item_description_more, package_measures, code, title, text, name, timestamp, item_name)
            .from(versionTable)
            .innerJoin(TVPRIS_ITEM).on(TVPRIS_ITEM.TVPI_VERSION.equal(versionField))
            .innerJoin(TVPRIS_PACKAGE).on(TVPRIS_PACKAGE.XWO_ID.equal(TVPRIS_ITEM.XWO_ID))
            .innerJoin(itemObjectTable).on(itemObjectTable.field("xwo_id", Long.class).equal(TVPRIS_ITEM.XWO_ID))
            .leftOuterJoin(TPK_ITEMREFERENCE).on(TPK_ITEMREFERENCE.TPKIR_TYPE.eq("package")
                                                 .and(TPK_ITEMREFERENCE.TPKIR_CODE.equal(TVPRIS_ITEM.TVPI_CODE.cast(String.class))))
            .leftOuterJoin(TPK_LINKABLEINFOTEXT).on(TPK_LINKABLEINFOTEXT.TLIT_CODE.equal(TPK_ITEMREFERENCE.TPKIR_INFOCODE))
            .leftOuterJoin(XWIKIOBJECTS.XWIKIOBJECTS).on(XWIKIOBJECTS.XWO_ID.equal(TPK_LINKABLEINFOTEXT.XWO_ID))
            .leftOuterJoin(XWIKIDOC).on(XWIKIOBJECTS.XWO_NAME.equal(XWIKIDOC.XWD_FULLNAME))
            .where(TVPRIS_ITEM.IS_MEASURE.isFalse().and
                   (TVPRIS_ITEM.TVPI_CODE.in(packages)))
            .groupBy(packageField, item_title, item_description, package_measures, item_name)
            .fetch();

        final List<Integer> packageList = result.getValues(packageField);
        final List<String> itemTitles = result.getValues(item_title);
        final List<Integer> itemCodes = result.getValues(item_code);
        final List<String> itemDescriptions = result.getValues(item_description);
        final List<String> itemDescriptionMores = result.getValues(item_description_more);
        final List<String> packageMeasures = result.getValues(package_measures);
        final List<String> itemNames = result.getValues(item_name);
        final List<String []> codes = result.getValues(code);
        final List<String []> titles = result.getValues(title);
        final List<String []> texts = result.getValues(text);
        final List<String []> names = result.getValues(name);
        final List<Timestamp []> timestamps = result.getValues(timestamp);

        if (packageList.size() != codes.size() || codes.size() != titles.size() || titles.size() != texts.size()) {
            throw new InfoboxRendererException("Unequally sized lists!");
        }

        final Map<Integer, Package> ret = new HashMap<Integer, Package>();

        for (int i = 0; i < packageList.size(); i++) {
            String[] c = codes.get(i);
            String[] t = titles.get(i);
            String[] txt = texts.get(i);
            String[] n = names.get(i);
            Date[] ts = timestamps.get(i);
            final List<Infobox> infoboxes = new ArrayList<Infobox>(c.length);
            for (int j = 0; j < c.length; j++) {
                infoboxes.add(new Infobox(c[j], t[j], renderText(txt[j], n[j]), ts[j]));
            }
            final Package p =
                new Package(itemCodes.get(i), itemTitles.get(i),
                            renderText(itemDescriptions.get(i), itemNames.get(i)),
                            renderText(itemDescriptionMores.get(i), itemNames.get(i)),
                            infoboxes,
                            packageMeasures.get(i));
            ret.put(packageList.get(i), p);
        }

        return ret;
    }
    

    @Override
    public Map<Integer, Measure> renderMeasureInfoboxes(SortedSet<Integer> measures, Boolean special) throws InfoboxRendererException, RendererException {
        final Field<Integer> measureField = TVPRIS_ITEM.TVPI_CODE.as("measure");
        final Field<String[]> code = DSL.arrayAgg(TPK_LINKABLEINFOTEXT.TLIT_CODE).as("code");
        final Field<String[]> title = DSL.arrayAgg(TPK_LINKABLEINFOTEXT.TLIT_TITLE).as("title");
        final Field<String[]> text = DSL.arrayAgg(TPK_LINKABLEINFOTEXT.TLIT_TEXT).as("text");
        final Field<String[]> name = DSL.arrayAgg(XWIKIOBJECTS.XWO_NAME).as("name");
        final Field<Timestamp[]> timestamp = DSL.arrayAgg(XWIKIDOC.XWD_CONTENT_UPDATE_DATE);
        final Field<Integer> item_code = TVPRIS_ITEM.TVPI_CODE.as("item_code");
        final Field<String> item_title = TVPRIS_ITEM.TVPI_TITLE.as("item_title");
        final Table<XwikiobjectsRecord> itemObjectTable = XWIKIOBJECTS.as("item_object");
        final Field<String> item_description = TVPRIS_ITEM.TVPI_DESCRIPTION.as("item_description");
        final Field<String> item_description_more = TVPRIS_ITEM.TVPI_DESCRIPTION.as("item_description_more");
        final Field<String> item_name = itemObjectTable.field("xwo_name", String.class).as("item_name");
        final Field<String> area = (special ? TVPRIS_MEASURE.TVPM_AREA_SPECIAL : TVPRIS_ITEM.TVPI_AREA).as("area");

        final DSLContext ctx = create();
        final Table<Record1<Integer>> versionTable = versionQuery(ctx).asTable();
        final Field<Integer> versionField = versionTable.field(0, Integer.class);

        Result<Record12<Integer, Integer, String, String, String, String, String[], String[], String[], String[], Timestamp[], String>> result = ctx
            .select(item_code, measureField, item_title, item_description, item_description_more, area, code, title, text, name, timestamp, item_name)
            .from(versionTable)
            .innerJoin(TVPRIS_ITEM).on(TVPRIS_ITEM.TVPI_VERSION.equal(versionField))
            .innerJoin(TVPRIS_MEASURE).on(TVPRIS_MEASURE.XWO_ID.equal(TVPRIS_ITEM.XWO_ID))
            .innerJoin(itemObjectTable).on(itemObjectTable.field("xwo_id", Long.class).equal(TVPRIS_ITEM.XWO_ID))
            .leftOuterJoin(TPK_ITEMREFERENCE).on(TPK_ITEMREFERENCE.TPKIR_TYPE.eq("measure")
                                                 .and(TPK_ITEMREFERENCE.TPKIR_CODE.equal(TVPRIS_ITEM.TVPI_CODE.cast(String.class))))
            .leftOuterJoin(TPK_LINKABLEINFOTEXT).on(TPK_LINKABLEINFOTEXT.TLIT_CODE.equal(TPK_ITEMREFERENCE.TPKIR_INFOCODE))
            .leftOuterJoin(XWIKIOBJECTS.XWIKIOBJECTS).on(XWIKIOBJECTS.XWO_ID.equal(TPK_LINKABLEINFOTEXT.XWO_ID))
            .leftOuterJoin(XWIKIDOC).on(XWIKIOBJECTS.XWO_NAME.equal(XWIKIDOC.XWD_FULLNAME))
            .where(TVPRIS_ITEM.IS_MEASURE.isTrue().and
                   (TVPRIS_ITEM.TVPI_CODE.in(measures)))
            .groupBy(measureField, item_title, item_description, area, item_name)
            .fetch();

        final List<Integer> measureList = result.getValues(measureField);
        final List<String> itemTitles = result.getValues(item_title);
        final List<Integer> itemCodes = result.getValues(item_code);
        final List<String> itemDescriptions = result.getValues(item_description);
        final List<String> itemDescriptionMores = result.getValues(item_description_more);
        final List<String> itemNames = result.getValues(item_name);

        final List<String> areas = result.getValues(area);
        final List<String []> codes = result.getValues(code);
        final List<String []> titles = result.getValues(title);
        final List<String []> texts = result.getValues(text);
        final List<String []> names = result.getValues(name);
        final List<Timestamp []> timestamps = result.getValues(timestamp);
        
        if (measureList.size() != codes.size() || codes.size() != titles.size() || titles.size() != texts.size()) {
            throw new InfoboxRendererException("Unequally sized lists!");
        }

        final Map<Integer, Measure> ret = new HashMap<Integer, Measure>();

        for (int i = 0; i < measureList.size(); i++) {
            String[] c = codes.get(i);
            String[] t = titles.get(i);
            String[] txt = texts.get(i);
            String[] n = names.get(i);
            Date[] ts = timestamps.get(i);
            final List<Infobox> infoboxes = new ArrayList<Infobox>(c.length);
            for (int j = 0; j < c.length; j++) {
                infoboxes.add(new Infobox(c[j], t[j], renderText(txt[j], n[j]), ts[j]));
            }
            final Measure m =
                new Measure(itemCodes.get(i), itemTitles.get(i),
                            renderText(itemDescriptions.get(i), itemNames.get(i)),
                            renderText(itemDescriptionMores.get(i), itemNames.get(i)),
                            infoboxes,
                            areas.get(i));
            ret.put(measureList.get(i), m);
        }

        return ret;
    }

}
							      
