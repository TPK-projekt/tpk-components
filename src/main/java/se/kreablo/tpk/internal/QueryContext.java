/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.internal;

import org.jooq.DSLContext;
import org.jooq.SelectQuery;
import org.jooq.Table;
import org.jooq.Field;
import org.jooq.Record1;
import se.kreablo.tpk.Parameters;
import se.kreablo.tpk.schema.tables.TpkAreacommentclass;

public class QueryContext implements AutoCloseable {

    private final Parameters parameters;

    private final DSLContext create;

    private final boolean single;

    private Table<Record1<Integer>> versionTable;

    private Field<Integer> surgeryIdField;

    private Field<String> surgeryNameField;

    private Field<Integer> versionField;

    private Field<Integer> priceTotal;

    private Field<String> areaField;
    
    private Field<Integer> noPriceItems;
    
    private Field<Double> longitudeField;

    private Field<Double> latitudeField;
    
    private Field<String> surgeryAddressField;
    
    private Field<String> surgeryDescriptionField;

    private SelectQuery<Record1<String>> surgeryItemAreas;

    private Field<String> commentField;
    
    private TpkAreacommentclass toplevelCommentTable;

    private Field<String> zipcodeField;

    private Field<String> cityField;

    private Field<Boolean> isAreaActive;

    public QueryContext(Parameters parameters, DSLContext create, boolean single) {
        this.parameters = parameters;
        this.single = single;
        this.create = create;
    }

    
    public SelectQuery<Record1<String>> getSurgeryItemAreas() {
        return this.surgeryItemAreas;
    }

    public void setSurgeryItemAreas(SelectQuery<Record1<String>> surgeryItemAreas) {
        this.surgeryItemAreas = surgeryItemAreas;
    }

    public Parameters getParameters() {
        return this.parameters;
    }

    public DSLContext getCreate() {
        return this.create;
    }

    public boolean isSingle() {
        return this.single;
    }

    public Table<Record1<Integer>> getVersionTable() {
        return this.versionTable;
    }

    public void setVersionTable(Table<Record1<Integer>> versionTable) {
        this.versionTable = versionTable;
    }

    public Field<Integer> getSurgeryIdField() {
        return this.surgeryIdField;
    }

    public void setSurgeryIdField(Field<Integer> surgeryIdField) {
        this.surgeryIdField = surgeryIdField;
    }

    public Field<String> getSurgeryNameField() {
        return this.surgeryNameField;
    }

    public void setSurgeryNameField(Field<String> surgeryNameField) {
        this.surgeryNameField = surgeryNameField;
    }

    public Field<Integer> getVersionField() {
        return this.versionField;
    }

    public void setVersionField(Field<Integer> versionField) {
        this.versionField = versionField;
    }

    
    public Field<Integer> getPriceTotal() {
        return this.priceTotal;
    }

    public void setPriceTotal(Field<Integer> priceTotal) {
        this.priceTotal = priceTotal;
    }
    
    public Field<String> getAreaField() {
        return this.areaField;
    }

    public void setAreaField(Field<String> areaField) {
        this.areaField = areaField;
    }

    public Field<Integer> getNoPriceItems() {
        return this.noPriceItems;
    }

    public void setNoPriceItems(Field<Integer> noPriceItems) {
        this.noPriceItems = noPriceItems;
    }

    public Field<String> getSurgeryAddressField() {
        return this.surgeryAddressField;
    }

    public void setSurgeryAddressField(Field<String> surgeryAddressField) {
        this.surgeryAddressField = surgeryAddressField;
    }

    public Field<String> getSurgeryDescriptionField() {
        return this.surgeryDescriptionField;
    }

    public void setSurgeryDescriptionField(Field<String> surgeryDescriptionField) {
        this.surgeryDescriptionField = surgeryDescriptionField;
    }

    public Field<Double> getLatitudeField() {
        return this.latitudeField;
    }

    public void setLatitudeField(Field<Double> latitudeField) {
        this.latitudeField = latitudeField;
    }

    public Field<Double> getLongitudeField() {
        return this.longitudeField;
    }

    public void setLongitudeField(Field<Double> longitudeField) {
        this.longitudeField = longitudeField;
    }

    public Field<String> getCommentField() {
        return this.commentField;
    }

    public void setCommentField(Field<String> commentField) {
        this.commentField = commentField;
    }

    public TpkAreacommentclass getToplevelCommentTable() {
        return this.toplevelCommentTable;
    }

    public void setToplevelCommentTable(TpkAreacommentclass toplevelCommentTable) {
        this.toplevelCommentTable = toplevelCommentTable;
    }

    public Field<String> getZipcodeField() {
        return this.zipcodeField;
    }

    public void setZipcodeField(Field<String> zipcodeField) {
        this.zipcodeField = zipcodeField;
    }

    public Field<String> getCityField() {
        return this.cityField;
    }

    public void setCityField(Field<String> cityField) {
        this.cityField = cityField;
    }

    public Field<Boolean> getIsAreaActive() {
        return this.isAreaActive;
    }

    public void setIsAreaActive(Field<Boolean> isAreaActive) {
        this.isAreaActive = isAreaActive;
    }

    public void close() {
        if (create != null) {
            create.close();
        }
    }
}

