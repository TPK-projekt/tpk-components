/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.internal;

import static se.kreablo.tpk.schema.Tables.*;
import se.kreablo.tpk.schema.tables.records.*;
import se.kreablo.tpk.Constants;

import org.jooq.SelectSelectStep;
import org.jooq.SelectJoinStep;
import org.jooq.Record;
import org.jooq.Record17;
import org.jooq.impl.DSL;

public abstract class AbstractCompareQueryBuilder
    implements CompareQueryBuilder {

    private final QueryContext ctx;

    public AbstractCompareQueryBuilder(QueryContext ctx)
    {
        this.ctx = ctx;
    }

    /*
      public TableLike<Record3<Integer, String, Boolean>> commentTable() {
      return getCtx().getCreate()
      .select(getCtx().getSurgeryIdField(), TVPRIS_PUBLIC_PRICECOMMENT.TVPPC_RENDERED_TEXT, TVPPC_VERSION)
      .from(TVPRIS_PUBLICPRICECOMMENT)
      .where(getCtx.getSurgeryIdField().equal(TVPRIS_PUBLICPRICECOMMENT.TVPPC_SURGERY_ID))
      .union(getCtx.getCreate()
      .select(getCtx().getSurgeryIdField(), 
	    
      .leftOuterJoin(TPK_AREACOMMENTCLASS)
      .on(TPK_AREACOMMENTCLASS.TPKAC_SURGERY_ID.equal(getCtx().getSurgeryIdField()))
      .leftOuterJoin(getCtx().getToplevelCommentTable())
      .on
      }*/

    @Override
    public SelectJoinStep<Record17<Integer, String, String, String, String, String, Double, Double, Integer, Integer[], Integer[], Integer[], Boolean[], String[], Boolean[], String[], Integer>>
        joinStep(SelectSelectStep<Record17<Integer, String, String, String, String, String, Double, Double, Integer, Integer[], Integer[], Integer[], Boolean[], String[], Boolean[], String[], Integer>> select) {
        return select.from(getCtx().getVersionTable())
            .crossJoin(TVPRIS_SURGERY)
            .innerJoin(TVPRIS_SURGERYEDIT)
            .on(TVPRIS_SURGERY.TVPS_SURGERY_EDITABLE_ID.equal(TVPRIS_SURGERYEDIT.XWO_ID))
            .innerJoin(TVPRIS_ITEM)
            .on(TVPRIS_ITEM.TVPI_VERSION.equal(getCtx().getVersionField()))
            .leftOuterJoin(TPK_ITEMPRICE)
            .on(TPK_ITEMPRICE.TVPIP_SURGERY_ID
                .equal(TVPRIS_SURGERY.TVPS_SURGERY_ID)
                .and(TVPRIS_ITEM.TVPI_CODE.equal(TPK_ITEMPRICE.TVPIP_ITEM_ID))
                .and(TPK_ITEMPRICE.TVPIP_IS_MEASURE.equal(TVPRIS_ITEM.IS_MEASURE))
                .and(TPK_ITEMPRICE.TVPIP_TYPE.ne(getCtx().getParameters().isSpecial() ? 1 : 2))
                .and(getCtx().getVersionField().cast(Long.class).equal(TPK_ITEMPRICE.TVPIP_VERSION)))
            .leftOuterJoin(TVPRIS_PUBLICPRICECOMMENT)
            .on(TVPRIS_SURGERY.TVPS_SURGERY_ID.equal(TVPRIS_PUBLICPRICECOMMENT.TVPPC_SURGERY_ID)
                .and(TVPRIS_ITEM.TVPI_CODE.equal(TVPRIS_PUBLICPRICECOMMENT.TVPPC_ITEM_ID))
                .and(DSL.condition(TVPRIS_ITEM.IS_MEASURE).and(TVPRIS_PUBLICPRICECOMMENT.TVPPC_IS_MEASURE.eq(1))
                     .or(DSL.not(DSL.condition(TVPRIS_ITEM.IS_MEASURE))).and(TVPRIS_PUBLICPRICECOMMENT.TVPPC_IS_MEASURE.ne(1))))
                .and(TVPRIS_PUBLICPRICECOMMENT.TVPPC_VERSION.equal(getCtx().getVersionField().cast(Long.class)))
            .leftOuterJoin(getCtx().getToplevelCommentTable())
            .on(TVPRIS_SURGERY.TVPS_SURGERY_ID.equal(getCtx().getToplevelCommentTable().TPKAC_SURGERY_ID)
                .and(getCtx().getToplevelCommentTable().TPKAC_AREA.eq(Constants.TOPLEVEL_COMMENT))
                .and(getCtx().getVersionField().equal(getCtx().getToplevelCommentTable().TPKAC_VERSION.cast(Integer.class))))
            .leftOuterJoin(TVPRIS_MEASURE).on(TVPRIS_MEASURE.XWO_ID.equal(TVPRIS_ITEM.XWO_ID))
            .leftOuterJoin(TVPRIS_PACKAGE).on(TVPRIS_PACKAGE.XWO_ID.equal(TVPRIS_ITEM.XWO_ID))
            .leftOuterJoin(TPK_AREACOMMENTCLASS)
            .on(TVPRIS_SURGERY.TVPS_SURGERY_ID.equal(TPK_AREACOMMENTCLASS.TPKAC_SURGERY_ID)
                .and(TPK_AREACOMMENTCLASS.TPKAC_VERSION.cast(Integer.class).equal(getCtx().getVersionField()))
                .and(TVPRIS_ITEM.TVPI_AREA.equal(TPK_AREACOMMENTCLASS.TPKAC_AREA)
                     .and(DSL.condition(TVPRIS_ITEM.IS_MEASURE).and(TPK_AREACOMMENTCLASS.TPKAC_IS_MEASURE.eq(1))
                          .or(DSL.condition(TVPRIS_ITEM.IS_MEASURE).and(TPK_AREACOMMENTCLASS.TPKAC_IS_MEASURE.eq(0))))));

    }

    protected QueryContext getCtx()
    {
        return ctx;
    }

}
