/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.office.internal;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import java.io.File;
import java.io.Closeable;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.FileOutputStream;
import java.io.IOException;


import org.xwiki.component.annotation.Component;
import org.xwiki.component.annotation.InstantiationStrategy;
import org.xwiki.component.descriptor.ComponentInstantiationStrategy;
import org.xwiki.model.reference.AttachmentReference;
import org.xwiki.model.reference.AttachmentReferenceResolver;
import org.xwiki.bridge.DocumentAccessBridge;
import org.xwiki.configuration.ConfigurationSource;


import se.kreablo.tpk.office.LetterTemplateSource;
import se.kreablo.tpk.office.LetterMakerException;

@Component
@Named("default")
@InstantiationStrategy(ComponentInstantiationStrategy.PER_LOOKUP)
public class DefaultLetterTemplateSource implements LetterTemplateSource, Closeable {

    public static String CONFIG_KEY = "tpk.lettertemplate";

    @Inject
    private ConfigurationSource configuration;

    @Inject
    @Named("current")
    private AttachmentReferenceResolver<String> attachmentReferenceResolver;
	
    @Inject
    private DocumentAccessBridge documentAccessBridge;

    private File tempfile = null;

    @Override
    public String getLetterTemplate() throws LetterMakerException {

	if (tempfile == null) {
	    final String attachment = configuration.getProperty("tpk.lettertemplate", "TVPris.Infotexter.WebHome@LetterTemplate.docx");

	    final AttachmentReference ref =  attachmentReferenceResolver.resolve(attachment);
	    InputStream content = null;
	    OutputStream out = null;

	    try {
		content = documentAccessBridge.getAttachmentContent(ref);

		tempfile = File.createTempFile("lettertemplate", ".docx");

		out = new FileOutputStream(tempfile);

		final byte [] buf = new byte[1024];

		int n = content.read(buf);
		while (n > 0) {
		    out.write(buf, 0, n);
		    n = content.read(buf);
		}

	    } catch (IOException e) {
		this.close();
		throw new LetterMakerException(e);
	    } catch (Exception e) {
		this.close();
		throw new LetterMakerException(e);
	    } finally {
		if (content != null) {
		    try {
			content.close();
		    } catch (IOException e0) {
			throw new LetterMakerException(e0);
		    }
		}
		if (out != null) {
		    try {
			out.close();
		    } catch (IOException e0) {
			throw new LetterMakerException(e0);
		    }
		}
	    }
	} 

	return "file:///" + tempfile.getAbsolutePath().replace( '\\', '/' );
    }


    @Override
    public void close() {
	if (tempfile != null) {
	    tempfile.delete();
	    tempfile = null;
	}
    }
	
}
