/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.office.internal;

import com.sun.star.beans.XPropertySet;
import com.sun.star.uno.XComponentContext;
import com.sun.star.lang.XMultiComponentFactory;
import com.sun.star.comp.helper.Bootstrap;
import com.sun.star.connection.XConnection;
import com.sun.star.connection.XConnector;
import com.sun.star.bridge.XBridge;
import com.sun.star.bridge.XBridgeFactory;
import com.sun.star.lang.XComponent;
import com.sun.star.uno.UnoRuntime;
import com.sun.star.lang.XEventListener;
import com.sun.star.lang.EventObject;
import com.sun.star.bridge.XUnoUrlResolver;
import com.sun.star.frame.XComponentLoader;

import org.xwiki.officeimporter.server.OfficeServerConfiguration;
import org.xwiki.officeimporter.server.OfficeServer;
import org.xwiki.officeimporter.server.OfficeServerException;

import javax.inject.Inject;

class AbstractOfficeComponent {

    @Inject
    private OfficeServerConfiguration officeServerConfiguration;

    @Inject
    private OfficeServer officeServer;

    private XComponentContext localOfficeContext;

    private XComponentContext remoteOfficeContext;

    private XMultiComponentFactory localServiceManager;

    private XMultiComponentFactory remoteServiceManager;

    private XComponentLoader remoteComponentLoader;

    protected void assureOfficeServerStarted() throws Exception {
        if (officeServer.getState() != OfficeServer.ServerState.CONNECTED) {
            officeServer.start();
        }
    }

    protected XComponentLoader getRemoteComponentLoader() throws Exception {
        if (remoteComponentLoader == null) {
            setup();
        }
        return remoteComponentLoader;
    }

    protected XComponentContext getRemoteOfficeContext() throws Exception {
        if (remoteOfficeContext == null) {
            setup();
        }

        return remoteOfficeContext;
    }

    protected XComponentContext getLocalOfficeContext() throws Exception {
        if (localOfficeContext == null) {
            setup();
        }

        return localOfficeContext;
    }

    protected XMultiComponentFactory getLocalServiceManager() throws Exception {
        if (localServiceManager == null) {
            setup();
        }
        return localServiceManager;
    }

    protected XMultiComponentFactory getRemoteServiceManager() throws Exception {
        if (remoteServiceManager == null) {
            setup();
        }
        return remoteServiceManager;
    }

    private void setup() throws Exception {
        assureOfficeServerStarted();
	    
        final int port = officeServerConfiguration.getServerPort();

        localOfficeContext = Bootstrap.createInitialComponentContext(null);
        localServiceManager = localOfficeContext.getServiceManager();

        XConnector connector =
            UnoRuntime.
            queryInterface(
                XConnector.class,
                localServiceManager.createInstanceWithContext(
                    "com.sun.star.connection.Connector",
                    localOfficeContext));

        XConnection connection = connector.connect("socket,host=127.0.0.1,port=" + port);
        XBridgeFactory bridgeFactory =
            UnoRuntime.queryInterface(
                XBridgeFactory.class,
                localServiceManager.createInstanceWithContext(
                    "com.sun.star.bridge.BridgeFactory",
                    localOfficeContext));

        String bridgeName = "officetest";
        XBridge bridge = bridgeFactory.createBridge("", "urp", connection, null);

        final XEventListener bridgeListener = new XEventListener()
            {
                public void disposing(EventObject event)
                {
                    remoteOfficeContext = null;
                    remoteComponentLoader = null;
                    localOfficeContext = null;
                    localServiceManager = null;
                    remoteServiceManager = null;
                }
            };
	    
        XComponent bridgeComponent = UnoRuntime.queryInterface(XComponent.class, bridge);
        bridgeComponent.addEventListener(bridgeListener);
        remoteServiceManager =
            UnoRuntime.queryInterface(
                XMultiComponentFactory.class,
                bridge.getInstance("StarOffice.ServiceManager"));
            
        XPropertySet properties = UnoRuntime.queryInterface(XPropertySet.class, remoteServiceManager);

        XComponentContext componentContext =
            UnoRuntime.queryInterface(
                XComponentContext.class,
                properties.getPropertyValue("DefaultContext"));

        remoteOfficeContext = componentContext;

        Object oDesktop = remoteServiceManager.createInstanceWithContext(
            "com.sun.star.frame.Desktop", remoteOfficeContext);

        remoteComponentLoader = UnoRuntime.queryInterface(
            com.sun.star.frame.XComponentLoader.class,
            oDesktop);

    }

}
