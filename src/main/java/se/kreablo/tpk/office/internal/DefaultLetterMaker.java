/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.office.internal;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import java.io.File;
import java.io.OutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.util.Map;
import java.util.List;
import java.util.LinkedList;

import org.xwiki.component.annotation.Component;

import se.kreablo.tpk.office.LetterMaker;
import se.kreablo.tpk.office.LetterMakerException;
import se.kreablo.tpk.office.LetterTemplateSource;

import com.sun.star.frame.XStorable;
import com.sun.star.document.XTypeDetection;
import com.sun.star.container.XNameAccess;
import com.sun.star.text.XTextDocument;
import com.sun.star.text.XText;
import com.sun.star.text.XTextContent;
import com.sun.star.text.XTextRange;
import com.sun.star.text.XParagraphAppend;
import com.sun.star.util.XSearchable;
import com.sun.star.util.XSearchDescriptor;
import com.sun.star.util.XReplaceable;
import com.sun.star.util.XReplaceDescriptor;
import com.sun.star.util.XCloseable;
import com.sun.star.util.CloseVetoException;
import com.sun.star.uno.UnoRuntime;
import com.sun.star.beans.PropertyValue;

import org.apache.pdfbox.multipdf.PDFMergerUtility;
import org.apache.pdfbox.io.MemoryUsageSetting;


@Component
@Named("default")
@Singleton
public class DefaultLetterMaker extends AbstractOfficeComponent implements LetterMaker {

    @Inject
    private LetterTemplateSource letterTemplateSource;

    @Override
    public File getPDFLetters(final List<Map<String, String>> replacements) throws LetterMakerException {

        final PropertyValue propertyValues[] = new PropertyValue[1];
        propertyValues[0] = new PropertyValue();
        propertyValues[0].Name = "Hidden";
        propertyValues[0].Value = Boolean.TRUE;

        final List<File> tempfiles = new LinkedList();

        try {
            for (Map<String, String> rep : replacements) {
                final Object oDoc =
                    getRemoteComponentLoader().loadComponentFromURL(
                        letterTemplateSource.getLetterTemplate(), "_blank", 0, propertyValues);

                final XTextDocument textDocument = UnoRuntime.queryInterface(XTextDocument.class, oDoc);

                searchAndReplace(oDoc, rep);
                
                final File tempfile = File.createTempFile("letter", ".pdf");
                tempfiles.add(tempfile);

                save(oDoc, tempfile);

                final XCloseable closeable = UnoRuntime.queryInterface(XCloseable.class, oDoc);
                if (closeable == null) {
                    textDocument.dispose();
                } else {
                    try {
                        closeable.close(true);
                    } catch (CloseVetoException e0) {
                    }
                }
            }

            return mergePDFs(tempfiles);
        } catch (Exception e) {
            throw new LetterMakerException("Could not load document " +
                                           letterTemplateSource.getLetterTemplate(), e);
        } finally {
            try {
                letterTemplateSource.close();
            } catch (IOException e) {
            }
            for (File tmp  : tempfiles) {
                System.err.println("delete " + tmp);
                tmp.delete();
            }
        }
    }

    private void searchAndReplace(final Object oDoc, Map<String, String> replacements) throws Exception {
        final XReplaceable replaceable = UnoRuntime.queryInterface(XReplaceable.class, oDoc);

        for (Map.Entry<String, String> kv : replacements.entrySet()) {
            searchAndReplace(replaceable, kv.getKey(), kv.getValue());
        }
    }

    private long searchAndReplace(final XReplaceable replaceable, final String key, final String value) {
        final XReplaceDescriptor sdesc = replaceable.createReplaceDescriptor();

        sdesc.setSearchString("<<" + key + ">>");
        sdesc.setReplaceString(value);

        return replaceable.replaceAll(sdesc);
    }

    private void save(final Object oDoc, final File file) throws Exception {
        final XStorable storable = UnoRuntime.queryInterface(XStorable.class, oDoc);

        final PropertyValue [] properties = new PropertyValue[2];
        
        String url = "file://" + file.getAbsolutePath().replace( '\\', '/' );

        properties[0] = new PropertyValue();
        properties[0].Name = "FilterName";
        properties[0].Value = "writer_pdf_Export";
        properties[1] = new PropertyValue();
        properties[1].Name = "Overwrite";
        properties[1].Value = true;

        storable.storeToURL(url, properties);
    }

    private File mergePDFs(List<File> pdfs) throws IOException, FileNotFoundException {
        final PDFMergerUtility merger = new PDFMergerUtility();

        final File dest = File.createTempFile("letters", ".pdf");
        final OutputStream out = new FileOutputStream(dest);

        for (final File pdf : pdfs) {
            merger.addSource(pdf);
        }
        merger.setDestinationStream(out);

        merger.mergeDocuments(MemoryUsageSetting.setupMixed(256000));

        System.err.println("dest: " + dest);

        return dest;
    }

}
