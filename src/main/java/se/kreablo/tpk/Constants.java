/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk;

public class Constants {

    public static final int DEFAULT_PACKAGE = 0;
    
    public static final String ARRAY_DELIM = ";";

    public static final String OFFSET_PARAMETER = "o";

    public static final String LIMIT_PARAMETER = "l";

    public static final String MEASURES_PARAMETER = "m";

    public static final String PACKAGES_PARAMETER = "p";

    public static final String SURGERIES_PARAMETER = "s";

    public static final String SORT_ORDER_PARAMETER = "so";

    public static final String ORDER_BY_PARAMETER = "ob";

    public static final String COUNTY_PARAMETER = "cnt";

    public static final String MUNICIPALITY_PARAMETER = "mun";

    public static final String SUBLOCALITY_PARAMETER = "sl";

    public static final String EVENING_HOURS_PARAMETER = "eh";

    public static final String WEEKEND_HOURS_PARAMETER = "weh";

    public static final String SURGERY_CATEGORY_PARAMETER = "cat";

    public static final String HAVE_PARKING_PARAMETER = "park";

    public static final String PUBLIC_TRANSPORT_PARAMETER = "pt";

    public static final String DENTAL_FEAR_PARAMETER = "df";

    public static final String WARRANTIES_PARAMETER = "warr";

    public static final String REGION_PARAMETER = "r";

    public static final String SPECIAL_PARAMETER = "special";

    public static final String COUNT_PARAMETER = "count";

    public static final String TOPLEVEL_COMMENT = "tpk_toplevel_comment";

    public static final String SURGERY_VIEW_SPACE = "Mottagning";

    public static final String SURGERY_EDIT_SPACE = "MottagningensInformation";

    public static final String VERSION_DOCUMENT = "Version.WebHome";

    public static final String APP_SPACE = "TVPris";

    public static final String CLASS_SPACE = APP_SPACE + ".Classes";

    public static final String SURGERY_CLASS = CLASS_SPACE + ".SurgeryClass";

    public static final String SURGERY_EDITABLE_CLASS = CLASS_SPACE + ".SurgeryEditableClass";

    public static final String PACKAGE_CLASS = CLASS_SPACE + ".PackageClass";

    public static final String MEASURE_CLASS = CLASS_SPACE + ".MeasureClass";

    public static final String VERSION_CLASS = CLASS_SPACE + ".VersionClass";

    public static final String MEASURE_SPACE = "Åtgärder";

    public static final String PACKAGE_SPACE = "Paket";
    
}
