/*
 * This file is generated by jOOQ.
 */
package se.kreablo.tpk.schema.tables.records;


import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record5;
import org.jooq.Row5;
import org.jooq.impl.UpdatableRecordImpl;

import se.kreablo.tpk.schema.tables.TpkItemreference;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.11.5"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class TpkItemreferenceRecord extends UpdatableRecordImpl<TpkItemreferenceRecord> implements Record5<Long, String, String, String, Integer> {

    private static final long serialVersionUID = -589882555;

    /**
     * Setter for <code>tpk_itemreference.xwo_id</code>.
     */
    public void setXwoId(Long value) {
        set(0, value);
    }

    /**
     * Getter for <code>tpk_itemreference.xwo_id</code>.
     */
    public Long getXwoId() {
        return (Long) get(0);
    }

    /**
     * Setter for <code>tpk_itemreference.tpkir_code</code>.
     */
    public void setTpkirCode(String value) {
        set(1, value);
    }

    /**
     * Getter for <code>tpk_itemreference.tpkir_code</code>.
     */
    public String getTpkirCode() {
        return (String) get(1);
    }

    /**
     * Setter for <code>tpk_itemreference.tpkir_type</code>.
     */
    public void setTpkirType(String value) {
        set(2, value);
    }

    /**
     * Getter for <code>tpk_itemreference.tpkir_type</code>.
     */
    public String getTpkirType() {
        return (String) get(2);
    }

    /**
     * Setter for <code>tpk_itemreference.tpkir_infocode</code>.
     */
    public void setTpkirInfocode(String value) {
        set(3, value);
    }

    /**
     * Getter for <code>tpk_itemreference.tpkir_infocode</code>.
     */
    public String getTpkirInfocode() {
        return (String) get(3);
    }

    /**
     * Setter for <code>tpk_itemreference.tpkir_version</code>.
     */
    public void setTpkirVersion(Integer value) {
        set(4, value);
    }

    /**
     * Getter for <code>tpk_itemreference.tpkir_version</code>.
     */
    public Integer getTpkirVersion() {
        return (Integer) get(4);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Record1<Long> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record5 type implementation
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Row5<Long, String, String, String, Integer> fieldsRow() {
        return (Row5) super.fieldsRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Row5<Long, String, String, String, Integer> valuesRow() {
        return (Row5) super.valuesRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Long> field1() {
        return TpkItemreference.TPK_ITEMREFERENCE.XWO_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field2() {
        return TpkItemreference.TPK_ITEMREFERENCE.TPKIR_CODE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field3() {
        return TpkItemreference.TPK_ITEMREFERENCE.TPKIR_TYPE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field4() {
        return TpkItemreference.TPK_ITEMREFERENCE.TPKIR_INFOCODE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field5() {
        return TpkItemreference.TPK_ITEMREFERENCE.TPKIR_VERSION;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long component1() {
        return getXwoId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component2() {
        return getTpkirCode();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component3() {
        return getTpkirType();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component4() {
        return getTpkirInfocode();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer component5() {
        return getTpkirVersion();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long value1() {
        return getXwoId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value2() {
        return getTpkirCode();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value3() {
        return getTpkirType();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value4() {
        return getTpkirInfocode();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value5() {
        return getTpkirVersion();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TpkItemreferenceRecord value1(Long value) {
        setXwoId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TpkItemreferenceRecord value2(String value) {
        setTpkirCode(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TpkItemreferenceRecord value3(String value) {
        setTpkirType(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TpkItemreferenceRecord value4(String value) {
        setTpkirInfocode(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TpkItemreferenceRecord value5(Integer value) {
        setTpkirVersion(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TpkItemreferenceRecord values(Long value1, String value2, String value3, String value4, Integer value5) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        value5(value5);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached TpkItemreferenceRecord
     */
    public TpkItemreferenceRecord() {
        super(TpkItemreference.TPK_ITEMREFERENCE);
    }

    /**
     * Create a detached, initialised TpkItemreferenceRecord
     */
    public TpkItemreferenceRecord(Long xwoId, String tpkirCode, String tpkirType, String tpkirInfocode, Integer tpkirVersion) {
        super(TpkItemreference.TPK_ITEMREFERENCE);

        set(0, xwoId);
        set(1, tpkirCode);
        set(2, tpkirType);
        set(3, tpkirInfocode);
        set(4, tpkirVersion);
    }
}
