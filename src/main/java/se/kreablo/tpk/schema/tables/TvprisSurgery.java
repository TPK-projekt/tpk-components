/*
 * This file is generated by jOOQ.
 */
package se.kreablo.tpk.schema.tables;


import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Index;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;

import se.kreablo.tpk.schema.DefaultSchema;
import se.kreablo.tpk.schema.Indexes;
import se.kreablo.tpk.schema.Keys;
import se.kreablo.tpk.schema.tables.records.TvprisSurgeryRecord;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.11.5"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class TvprisSurgery extends TableImpl<TvprisSurgeryRecord> {

    private static final long serialVersionUID = -366317019;

    /**
     * The reference instance of <code>tvpris_surgery</code>
     */
    public static final TvprisSurgery TVPRIS_SURGERY = new TvprisSurgery();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<TvprisSurgeryRecord> getRecordType() {
        return TvprisSurgeryRecord.class;
    }

    /**
     * The column <code>tvpris_surgery.xwo_id</code>.
     */
    public final TableField<TvprisSurgeryRecord, Long> XWO_ID = createField("xwo_id", org.jooq.impl.SQLDataType.BIGINT.nullable(false), this, "");

    /**
     * The column <code>tvpris_surgery.tvps_surgery_id</code>.
     */
    public final TableField<TvprisSurgeryRecord, Integer> TVPS_SURGERY_ID = createField("tvps_surgery_id", org.jooq.impl.SQLDataType.INTEGER.nullable(false), this, "");

    /**
     * The column <code>tvpris_surgery.tvps_county</code>.
     */
    public final TableField<TvprisSurgeryRecord, String> TVPS_COUNTY = createField("tvps_county", org.jooq.impl.SQLDataType.VARCHAR(255), this, "");

    /**
     * The column <code>tvpris_surgery.tvps_municipality</code>.
     */
    public final TableField<TvprisSurgeryRecord, String> TVPS_MUNICIPALITY = createField("tvps_municipality", org.jooq.impl.SQLDataType.VARCHAR(255), this, "");

    /**
     * The column <code>tvpris_surgery.tvps_sublocality</code>.
     */
    public final TableField<TvprisSurgeryRecord, String> TVPS_SUBLOCALITY = createField("tvps_sublocality", org.jooq.impl.SQLDataType.VARCHAR(255), this, "");

    /**
     * The column <code>tvpris_surgery.tvps_name</code>.
     */
    public final TableField<TvprisSurgeryRecord, String> TVPS_NAME = createField("tvps_name", org.jooq.impl.SQLDataType.CLOB, this, "");

    /**
     * The column <code>tvpris_surgery.tvps_surgery_editable_id</code>.
     */
    public final TableField<TvprisSurgeryRecord, Long> TVPS_SURGERY_EDITABLE_ID = createField("tvps_surgery_editable_id", org.jooq.impl.SQLDataType.BIGINT, this, "");

    /**
     * The column <code>tvpris_surgery.tvps_category</code>.
     */
    public final TableField<TvprisSurgeryRecord, String> TVPS_CATEGORY = createField("tvps_category", org.jooq.impl.SQLDataType.CLOB, this, "");

    /**
     * The column <code>tvpris_surgery.tvps_company</code>.
     */
    public final TableField<TvprisSurgeryRecord, String> TVPS_COMPANY = createField("tvps_company", org.jooq.impl.SQLDataType.VARCHAR(255), this, "");

    /**
     * The column <code>tvpris_surgery.tvps_region</code>.
     */
    public final TableField<TvprisSurgeryRecord, String> TVPS_REGION = createField("tvps_region", org.jooq.impl.SQLDataType.VARCHAR(255), this, "");

    /**
     * Create a <code>tvpris_surgery</code> table reference
     */
    public TvprisSurgery() {
        this(DSL.name("tvpris_surgery"), null);
    }

    /**
     * Create an aliased <code>tvpris_surgery</code> table reference
     */
    public TvprisSurgery(String alias) {
        this(DSL.name(alias), TVPRIS_SURGERY);
    }

    /**
     * Create an aliased <code>tvpris_surgery</code> table reference
     */
    public TvprisSurgery(Name alias) {
        this(alias, TVPRIS_SURGERY);
    }

    private TvprisSurgery(Name alias, Table<TvprisSurgeryRecord> aliased) {
        this(alias, aliased, null);
    }

    private TvprisSurgery(Name alias, Table<TvprisSurgeryRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""));
    }

    public <O extends Record> TvprisSurgery(Table<O> child, ForeignKey<O, TvprisSurgeryRecord> key) {
        super(child, key, TVPRIS_SURGERY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Schema getSchema() {
        return DefaultSchema.DEFAULT_SCHEMA;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Index> getIndexes() {
        return Arrays.<Index>asList(Indexes.TVPRIS_SURGERY_PKEY, Indexes.TVPRIS_SURGERY_TVPS_SURGERY_ID_KEY, Indexes.TVPRIS_TVPS_SURGERY_CATEGORY, Indexes.TVPRIS_TVPS_SURGERY_COMPANY, Indexes.TVPRIS_TVPS_SURGERY_COUNTY, Indexes.TVPRIS_TVPS_SURGERY_EDITABLE_ID, Indexes.TVPRIS_TVPS_SURGERY_ID, Indexes.TVPRIS_TVPS_SURGERY_MUNICIPALITY, Indexes.TVPRIS_TVPS_SURGERY_NAME, Indexes.TVPRIS_TVPS_SURGERY_REGION, Indexes.TVPRIS_TVPS_SURGERY_SUBLOCALITY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UniqueKey<TvprisSurgeryRecord> getPrimaryKey() {
        return Keys.TVPRIS_SURGERY_PKEY;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UniqueKey<TvprisSurgeryRecord>> getKeys() {
        return Arrays.<UniqueKey<TvprisSurgeryRecord>>asList(Keys.TVPRIS_SURGERY_PKEY, Keys.TVPRIS_SURGERY_TVPS_SURGERY_ID_KEY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TvprisSurgery as(String alias) {
        return new TvprisSurgery(DSL.name(alias), this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TvprisSurgery as(Name alias) {
        return new TvprisSurgery(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public TvprisSurgery rename(String name) {
        return new TvprisSurgery(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public TvprisSurgery rename(Name name) {
        return new TvprisSurgery(name, null);
    }
}
