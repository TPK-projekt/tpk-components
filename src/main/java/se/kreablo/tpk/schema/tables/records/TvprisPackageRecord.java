/*
 * This file is generated by jOOQ.
 */
package se.kreablo.tpk.schema.tables.records;


import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record3;
import org.jooq.Row3;
import org.jooq.impl.UpdatableRecordImpl;

import se.kreablo.tpk.schema.tables.TvprisPackage;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.11.5"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class TvprisPackageRecord extends UpdatableRecordImpl<TvprisPackageRecord> implements Record3<Long, String, String> {

    private static final long serialVersionUID = -1896974515;

    /**
     * Setter for <code>tvpris_package.xwo_id</code>.
     */
    public void setXwoId(Long value) {
        set(0, value);
    }

    /**
     * Getter for <code>tvpris_package.xwo_id</code>.
     */
    public Long getXwoId() {
        return (Long) get(0);
    }

    /**
     * Setter for <code>tvpris_package.tvpp_text_code</code>.
     */
    public void setTvppTextCode(String value) {
        set(1, value);
    }

    /**
     * Getter for <code>tvpris_package.tvpp_text_code</code>.
     */
    public String getTvppTextCode() {
        return (String) get(1);
    }

    /**
     * Setter for <code>tvpris_package.tvpp_measures</code>.
     */
    public void setTvppMeasures(String value) {
        set(2, value);
    }

    /**
     * Getter for <code>tvpris_package.tvpp_measures</code>.
     */
    public String getTvppMeasures() {
        return (String) get(2);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Record1<Long> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record3 type implementation
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Row3<Long, String, String> fieldsRow() {
        return (Row3) super.fieldsRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Row3<Long, String, String> valuesRow() {
        return (Row3) super.valuesRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Long> field1() {
        return TvprisPackage.TVPRIS_PACKAGE.XWO_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field2() {
        return TvprisPackage.TVPRIS_PACKAGE.TVPP_TEXT_CODE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field3() {
        return TvprisPackage.TVPRIS_PACKAGE.TVPP_MEASURES;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long component1() {
        return getXwoId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component2() {
        return getTvppTextCode();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component3() {
        return getTvppMeasures();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long value1() {
        return getXwoId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value2() {
        return getTvppTextCode();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value3() {
        return getTvppMeasures();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TvprisPackageRecord value1(Long value) {
        setXwoId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TvprisPackageRecord value2(String value) {
        setTvppTextCode(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TvprisPackageRecord value3(String value) {
        setTvppMeasures(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TvprisPackageRecord values(Long value1, String value2, String value3) {
        value1(value1);
        value2(value2);
        value3(value3);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached TvprisPackageRecord
     */
    public TvprisPackageRecord() {
        super(TvprisPackage.TVPRIS_PACKAGE);
    }

    /**
     * Create a detached, initialised TvprisPackageRecord
     */
    public TvprisPackageRecord(Long xwoId, String tvppTextCode, String tvppMeasures) {
        super(TvprisPackage.TVPRIS_PACKAGE);

        set(0, xwoId);
        set(1, tvppTextCode);
        set(2, tvppMeasures);
    }
}
