/*
 * This file is generated by jOOQ.
 */
package se.kreablo.tpk.schema;


import javax.annotation.Generated;

import org.jooq.ForeignKey;
import org.jooq.Identity;
import org.jooq.UniqueKey;
import org.jooq.impl.Internal;

import se.kreablo.tpk.schema.tables.TpkAreacommentclass;
import se.kreablo.tpk.schema.tables.TpkInfotext;
import se.kreablo.tpk.schema.tables.TpkItemprice;
import se.kreablo.tpk.schema.tables.TpkItemreference;
import se.kreablo.tpk.schema.tables.TpkLargeMunicipalities;
import se.kreablo.tpk.schema.tables.TpkLinkableinfotext;
import se.kreablo.tpk.schema.tables.TpkPackageAreas;
import se.kreablo.tpk.schema.tables.TvprisInternalpricecomment;
import se.kreablo.tpk.schema.tables.TvprisItem;
import se.kreablo.tpk.schema.tables.TvprisItemprice;
import se.kreablo.tpk.schema.tables.TvprisMeasure;
import se.kreablo.tpk.schema.tables.TvprisPackage;
import se.kreablo.tpk.schema.tables.TvprisPublicpricecomment;
import se.kreablo.tpk.schema.tables.TvprisSurgery;
import se.kreablo.tpk.schema.tables.TvprisSurgeryedit;
import se.kreablo.tpk.schema.tables.TvprisVersionclass;
import se.kreablo.tpk.schema.tables.Xwikidoc;
import se.kreablo.tpk.schema.tables.Xwikiobjects;
import se.kreablo.tpk.schema.tables.records.TpkAreacommentclassRecord;
import se.kreablo.tpk.schema.tables.records.TpkInfotextRecord;
import se.kreablo.tpk.schema.tables.records.TpkItempriceRecord;
import se.kreablo.tpk.schema.tables.records.TpkItemreferenceRecord;
import se.kreablo.tpk.schema.tables.records.TpkLargeMunicipalitiesRecord;
import se.kreablo.tpk.schema.tables.records.TpkLinkableinfotextRecord;
import se.kreablo.tpk.schema.tables.records.TpkPackageAreasRecord;
import se.kreablo.tpk.schema.tables.records.TvprisInternalpricecommentRecord;
import se.kreablo.tpk.schema.tables.records.TvprisItemRecord;
import se.kreablo.tpk.schema.tables.records.TvprisItempriceRecord;
import se.kreablo.tpk.schema.tables.records.TvprisMeasureRecord;
import se.kreablo.tpk.schema.tables.records.TvprisPackageRecord;
import se.kreablo.tpk.schema.tables.records.TvprisPublicpricecommentRecord;
import se.kreablo.tpk.schema.tables.records.TvprisSurgeryRecord;
import se.kreablo.tpk.schema.tables.records.TvprisSurgeryeditRecord;
import se.kreablo.tpk.schema.tables.records.TvprisVersionclassRecord;
import se.kreablo.tpk.schema.tables.records.XwikidocRecord;
import se.kreablo.tpk.schema.tables.records.XwikiobjectsRecord;


/**
 * A class modelling foreign key relationships and constraints of tables of 
 * the <code></code> schema.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.11.5"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Keys {

    // -------------------------------------------------------------------------
    // IDENTITY definitions
    // -------------------------------------------------------------------------

    public static final Identity<TpkItempriceRecord, Integer> IDENTITY_TPK_ITEMPRICE = Identities0.IDENTITY_TPK_ITEMPRICE;

    // -------------------------------------------------------------------------
    // UNIQUE and PRIMARY KEY definitions
    // -------------------------------------------------------------------------

    public static final UniqueKey<TpkAreacommentclassRecord> TPK_AREACOMMENTCLASS_PKEY = UniqueKeys0.TPK_AREACOMMENTCLASS_PKEY;
    public static final UniqueKey<TpkInfotextRecord> TPK_INFOTEXT_PKEY = UniqueKeys0.TPK_INFOTEXT_PKEY;
    public static final UniqueKey<TpkItempriceRecord> TPK_ITEMPRICE_PKEY = UniqueKeys0.TPK_ITEMPRICE_PKEY;
    public static final UniqueKey<TpkItempriceRecord> TPK_ITEM_COMPOSITE_UNIQUE = UniqueKeys0.TPK_ITEM_COMPOSITE_UNIQUE;
    public static final UniqueKey<TpkItemreferenceRecord> TPK_ITEMREFERENCE_PKEY = UniqueKeys0.TPK_ITEMREFERENCE_PKEY;
    public static final UniqueKey<TpkLargeMunicipalitiesRecord> TPK_LARGE_MUNICIPALITIES_PKEY = UniqueKeys0.TPK_LARGE_MUNICIPALITIES_PKEY;
    public static final UniqueKey<TpkLinkableinfotextRecord> TPK_LINKABLEINFOTEXT_PKEY = UniqueKeys0.TPK_LINKABLEINFOTEXT_PKEY;
    public static final UniqueKey<TpkPackageAreasRecord> TPK_PACKAGE_AREAS_UNIQUE = UniqueKeys0.TPK_PACKAGE_AREAS_UNIQUE;
    public static final UniqueKey<TvprisInternalpricecommentRecord> TVPRIS_INTERNALPRICECOMMENT_PKEY = UniqueKeys0.TVPRIS_INTERNALPRICECOMMENT_PKEY;
    public static final UniqueKey<TvprisItemRecord> TVPRIS_ITEM_PKEY = UniqueKeys0.TVPRIS_ITEM_PKEY;
    public static final UniqueKey<TvprisItemRecord> TVPRIS_ITEM_COMPOSITE_UNIQUE = UniqueKeys0.TVPRIS_ITEM_COMPOSITE_UNIQUE;
    public static final UniqueKey<TvprisItempriceRecord> TVPRIS_ITEMPRICE_PKEY = UniqueKeys0.TVPRIS_ITEMPRICE_PKEY;
    public static final UniqueKey<TvprisMeasureRecord> TVPRIS_MEASURE_PKEY = UniqueKeys0.TVPRIS_MEASURE_PKEY;
    public static final UniqueKey<TvprisPackageRecord> TVPRIS_PACKAGE_PKEY = UniqueKeys0.TVPRIS_PACKAGE_PKEY;
    public static final UniqueKey<TvprisPublicpricecommentRecord> TVPRIS_PUBLICPRICECOMMENT_PKEY = UniqueKeys0.TVPRIS_PUBLICPRICECOMMENT_PKEY;
    public static final UniqueKey<TvprisSurgeryRecord> TVPRIS_SURGERY_PKEY = UniqueKeys0.TVPRIS_SURGERY_PKEY;
    public static final UniqueKey<TvprisSurgeryRecord> TVPRIS_SURGERY_TVPS_SURGERY_ID_KEY = UniqueKeys0.TVPRIS_SURGERY_TVPS_SURGERY_ID_KEY;
    public static final UniqueKey<TvprisSurgeryeditRecord> TVPRIS_SURGERYEDIT_PKEY = UniqueKeys0.TVPRIS_SURGERYEDIT_PKEY;
    public static final UniqueKey<TvprisVersionclassRecord> TVPRIS_VERSIONCLASS_PKEY = UniqueKeys0.TVPRIS_VERSIONCLASS_PKEY;
    public static final UniqueKey<XwikidocRecord> XWIKIDOC_PKEY = UniqueKeys0.XWIKIDOC_PKEY;
    public static final UniqueKey<XwikiobjectsRecord> XWIKIOBJECTS_PKEY = UniqueKeys0.XWIKIOBJECTS_PKEY;

    // -------------------------------------------------------------------------
    // FOREIGN KEY definitions
    // -------------------------------------------------------------------------

    public static final ForeignKey<TpkPackageAreasRecord, TvprisPackageRecord> TPK_PACKAGE_AREAS__TPK_PACKAGE_AREAS_ID_FKEY = ForeignKeys0.TPK_PACKAGE_AREAS__TPK_PACKAGE_AREAS_ID_FKEY;
    public static final ForeignKey<TvprisMeasureRecord, TvprisItemRecord> TVPRIS_MEASURE__FK85CEDE4DFCF6FCC = ForeignKeys0.TVPRIS_MEASURE__FK85CEDE4DFCF6FCC;
    public static final ForeignKey<TvprisPackageRecord, TvprisItemRecord> TVPRIS_PACKAGE__FK1DC66D35B644F1E4 = ForeignKeys0.TVPRIS_PACKAGE__FK1DC66D35B644F1E4;

    // -------------------------------------------------------------------------
    // [#1459] distribute members to avoid static initialisers > 64kb
    // -------------------------------------------------------------------------

    private static class Identities0 {
        public static Identity<TpkItempriceRecord, Integer> IDENTITY_TPK_ITEMPRICE = Internal.createIdentity(TpkItemprice.TPK_ITEMPRICE, TpkItemprice.TPK_ITEMPRICE.TVPIP_ID);
    }

    private static class UniqueKeys0 {
        public static final UniqueKey<TpkAreacommentclassRecord> TPK_AREACOMMENTCLASS_PKEY = Internal.createUniqueKey(TpkAreacommentclass.TPK_AREACOMMENTCLASS, "tpk_areacommentclass_pkey", TpkAreacommentclass.TPK_AREACOMMENTCLASS.XWO_ID);
        public static final UniqueKey<TpkInfotextRecord> TPK_INFOTEXT_PKEY = Internal.createUniqueKey(TpkInfotext.TPK_INFOTEXT, "tpk_infotext_pkey", TpkInfotext.TPK_INFOTEXT.XWO_ID);
        public static final UniqueKey<TpkItempriceRecord> TPK_ITEMPRICE_PKEY = Internal.createUniqueKey(TpkItemprice.TPK_ITEMPRICE, "tpk_itemprice_pkey", TpkItemprice.TPK_ITEMPRICE.TVPIP_ID);
        public static final UniqueKey<TpkItempriceRecord> TPK_ITEM_COMPOSITE_UNIQUE = Internal.createUniqueKey(TpkItemprice.TPK_ITEMPRICE, "tpk_item_composite_unique", TpkItemprice.TPK_ITEMPRICE.TVPIP_TYPE, TpkItemprice.TPK_ITEMPRICE.TVPIP_VERSION, TpkItemprice.TPK_ITEMPRICE.TVPIP_ITEM_ID, TpkItemprice.TPK_ITEMPRICE.TVPIP_SURGERY_ID);
        public static final UniqueKey<TpkItemreferenceRecord> TPK_ITEMREFERENCE_PKEY = Internal.createUniqueKey(TpkItemreference.TPK_ITEMREFERENCE, "tpk_itemreference_pkey", TpkItemreference.TPK_ITEMREFERENCE.XWO_ID);
        public static final UniqueKey<TpkLargeMunicipalitiesRecord> TPK_LARGE_MUNICIPALITIES_PKEY = Internal.createUniqueKey(TpkLargeMunicipalities.TPK_LARGE_MUNICIPALITIES, "tpk_large_municipalities_pkey", TpkLargeMunicipalities.TPK_LARGE_MUNICIPALITIES.TLM_MUNICIPALITY);
        public static final UniqueKey<TpkLinkableinfotextRecord> TPK_LINKABLEINFOTEXT_PKEY = Internal.createUniqueKey(TpkLinkableinfotext.TPK_LINKABLEINFOTEXT, "tpk_linkableinfotext_pkey", TpkLinkableinfotext.TPK_LINKABLEINFOTEXT.XWO_ID);
        public static final UniqueKey<TpkPackageAreasRecord> TPK_PACKAGE_AREAS_UNIQUE = Internal.createUniqueKey(TpkPackageAreas.TPK_PACKAGE_AREAS, "tpk_package_areas_unique", TpkPackageAreas.TPK_PACKAGE_AREAS.XWO_ID, TpkPackageAreas.TPK_PACKAGE_AREAS.TPA_AREA);
        public static final UniqueKey<TvprisInternalpricecommentRecord> TVPRIS_INTERNALPRICECOMMENT_PKEY = Internal.createUniqueKey(TvprisInternalpricecomment.TVPRIS_INTERNALPRICECOMMENT, "tvpris_internalpricecomment_pkey", TvprisInternalpricecomment.TVPRIS_INTERNALPRICECOMMENT.XWO_ID);
        public static final UniqueKey<TvprisItemRecord> TVPRIS_ITEM_PKEY = Internal.createUniqueKey(TvprisItem.TVPRIS_ITEM, "tvpris_item_pkey", TvprisItem.TVPRIS_ITEM.XWO_ID);
        public static final UniqueKey<TvprisItemRecord> TVPRIS_ITEM_COMPOSITE_UNIQUE = Internal.createUniqueKey(TvprisItem.TVPRIS_ITEM, "tvpris_item_composite_unique", TvprisItem.TVPRIS_ITEM.IS_MEASURE, TvprisItem.TVPRIS_ITEM.TVPI_VERSION, TvprisItem.TVPRIS_ITEM.TVPI_CODE);
        public static final UniqueKey<TvprisItempriceRecord> TVPRIS_ITEMPRICE_PKEY = Internal.createUniqueKey(TvprisItemprice.TVPRIS_ITEMPRICE, "tvpris_itemprice_pkey", TvprisItemprice.TVPRIS_ITEMPRICE.XWO_ID);
        public static final UniqueKey<TvprisMeasureRecord> TVPRIS_MEASURE_PKEY = Internal.createUniqueKey(TvprisMeasure.TVPRIS_MEASURE, "tvpris_measure_pkey", TvprisMeasure.TVPRIS_MEASURE.XWO_ID);
        public static final UniqueKey<TvprisPackageRecord> TVPRIS_PACKAGE_PKEY = Internal.createUniqueKey(TvprisPackage.TVPRIS_PACKAGE, "tvpris_package_pkey", TvprisPackage.TVPRIS_PACKAGE.XWO_ID);
        public static final UniqueKey<TvprisPublicpricecommentRecord> TVPRIS_PUBLICPRICECOMMENT_PKEY = Internal.createUniqueKey(TvprisPublicpricecomment.TVPRIS_PUBLICPRICECOMMENT, "tvpris_publicpricecomment_pkey", TvprisPublicpricecomment.TVPRIS_PUBLICPRICECOMMENT.XWO_ID);
        public static final UniqueKey<TvprisSurgeryRecord> TVPRIS_SURGERY_PKEY = Internal.createUniqueKey(TvprisSurgery.TVPRIS_SURGERY, "tvpris_surgery_pkey", TvprisSurgery.TVPRIS_SURGERY.XWO_ID);
        public static final UniqueKey<TvprisSurgeryRecord> TVPRIS_SURGERY_TVPS_SURGERY_ID_KEY = Internal.createUniqueKey(TvprisSurgery.TVPRIS_SURGERY, "tvpris_surgery_tvps_surgery_id_key", TvprisSurgery.TVPRIS_SURGERY.TVPS_SURGERY_ID);
        public static final UniqueKey<TvprisSurgeryeditRecord> TVPRIS_SURGERYEDIT_PKEY = Internal.createUniqueKey(TvprisSurgeryedit.TVPRIS_SURGERYEDIT, "tvpris_surgeryedit_pkey", TvprisSurgeryedit.TVPRIS_SURGERYEDIT.XWO_ID);
        public static final UniqueKey<TvprisVersionclassRecord> TVPRIS_VERSIONCLASS_PKEY = Internal.createUniqueKey(TvprisVersionclass.TVPRIS_VERSIONCLASS, "tvpris_versionclass_pkey", TvprisVersionclass.TVPRIS_VERSIONCLASS.XWO_ID);
        public static final UniqueKey<XwikidocRecord> XWIKIDOC_PKEY = Internal.createUniqueKey(Xwikidoc.XWIKIDOC, "xwikidoc_pkey", Xwikidoc.XWIKIDOC.XWD_ID);
        public static final UniqueKey<XwikiobjectsRecord> XWIKIOBJECTS_PKEY = Internal.createUniqueKey(Xwikiobjects.XWIKIOBJECTS, "xwikiobjects_pkey", Xwikiobjects.XWIKIOBJECTS.XWO_ID);
    }

    private static class ForeignKeys0 {
        public static final ForeignKey<TpkPackageAreasRecord, TvprisPackageRecord> TPK_PACKAGE_AREAS__TPK_PACKAGE_AREAS_ID_FKEY = Internal.createForeignKey(se.kreablo.tpk.schema.Keys.TVPRIS_PACKAGE_PKEY, TpkPackageAreas.TPK_PACKAGE_AREAS, "tpk_package_areas__tpk_package_areas_id_fkey", TpkPackageAreas.TPK_PACKAGE_AREAS.XWO_ID);
        public static final ForeignKey<TvprisMeasureRecord, TvprisItemRecord> TVPRIS_MEASURE__FK85CEDE4DFCF6FCC = Internal.createForeignKey(se.kreablo.tpk.schema.Keys.TVPRIS_ITEM_PKEY, TvprisMeasure.TVPRIS_MEASURE, "tvpris_measure__fk85cede4dfcf6fcc", TvprisMeasure.TVPRIS_MEASURE.XWO_ID);
        public static final ForeignKey<TvprisPackageRecord, TvprisItemRecord> TVPRIS_PACKAGE__FK1DC66D35B644F1E4 = Internal.createForeignKey(se.kreablo.tpk.schema.Keys.TVPRIS_ITEM_PKEY, TvprisPackage.TVPRIS_PACKAGE, "tvpris_package__fk1dc66d35b644f1e4", TvprisPackage.TVPRIS_PACKAGE.XWO_ID);
    }
}
