/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.rest;

import javax.ws.rs.PUT;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.MediaType;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Provider;

import java.io.Reader;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import org.slf4j.Logger;

import org.xwiki.component.annotation.Component;

import se.kreablo.tpk.objectimport.tpk.PackageSource;
import se.kreablo.tpk.objectimport.tpk.PackageSink;
import se.kreablo.tpk.objectimport.tpk.PackageAreaFiller;
import se.kreablo.tpk.objectimport.tpk.PackageAreaFillerException;
import se.kreablo.tpk.objectimport.RecordConduit;
import se.kreablo.tpk.objectimport.FieldSource;
import se.kreablo.tpk.objectimport.FieldSink;
import se.kreablo.tpk.objectimport.ConduitException;
import se.kreablo.tpk.objectimport.tpk.CSVSourceConfig;

/**
 * Rest service for iporting package objects.
 * @version $Id$
 */
@Component("se.kreablo.tpk.rest.TPKImportPackages")
@Path("/tpk-import-packages")
public class TPKImportPackages extends AbstractImportService
{
    @Inject
    private Logger logger;

    @Inject
    @Named("csv")
    private Provider<PackageSource<CSVSourceConfig>> packageSourceProvider;

    @Inject
    @Named("object")
    private Provider<PackageSink> packageSinkProvider;

    @Inject
    @Named("table")
    private PackageAreaFiller packageAreaFiller;
    
    /**
     * Upload a csv document with package object data.
     * @param content The CSV document content as a reader.
     * @return A json object.
     */
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("text/csv")
    public Response put(Reader content) {

        checkAccess();
        
        PackageSource<CSVSourceConfig> s = packageSourceProvider.get();
        PackageSink                    t = packageSinkProvider.get();

        addCloseable(s);
        addCloseable(t);

        try {
            try {
                s.start(new CSVSourceConfig(content));
            } catch (ConduitException e) {
                throw new RuntimeException("Couldn't start source", e);
            }

            final FieldSource<?> [] sources = { s.getVersionSource(), s.getCodeSource(), s.getTextCodeSource(),
                                                s.getTitleSource(), s.getAreaSource(), s.getDescriptionSource(),
                                                s.getDescriptionMoreSource(), s.getMeasuresSource(), s.getPriceSource() };
            final FieldSink<?>   [] sinks   = { t.getVersionSink(),   t.getCodeSink(),   t.getTextCodeSink(),
                                                t.getTitleSink(),   t.getAreaSink(),   t.getDescriptionSink(),
                                                t.getDescriptionMoreSink(), t.getMeasuresSink(),     t.getPriceSink()   };

            RecordConduit rc = new RecordConduit(sources, sinks);

            List<Exception> exceptions = rc.run();

            try {
                packageAreaFiller.fill();
            } catch (PackageAreaFillerException e) {
                exceptions.add(e);
            }

            Map<String, Object> ret = new HashMap<>();

            return maybeAddExceptionMessages(ret, exceptions);
        } finally {
            try {
                close();
            } catch (IOException e) {
                logger.error("Exception when importing packages.", e);
                throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
            }
        }
    }
    
}
