/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.rest;

import org.xwiki.component.annotation.Component;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import java.util.Map;
import java.util.HashMap;
import java.io.Writer;
import java.io.StringWriter;
import java.io.IOException;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.MediaType;

import org.xwiki.component.annotation.Component;
import org.xwiki.rest.XWikiResource;
import org.xwiki.query.QueryException;

import se.kreablo.tpk.Search;

import org.slf4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 
 */
@Component("se.kreablo.tpk.rest.TPKSearch")
@Path("/tpk-search")
public class TPKSearch extends XWikiResource
{
    @Inject
    Search search;

    @Inject
    Logger logger;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String get(@QueryParam("q")  String query,
		      @QueryParam("limit") Integer limit,
		      @QueryParam("offset") Integer offset) {
	if (query == null) {
	    return "{}";
	}

	final Map<String, Object> jsonItems = new HashMap<String, Object>();

	try {
	    jsonItems.put("search", search.search(query, offset, limit));
	} catch (QueryException e) {
	    throw new RuntimeException(e);
	}
		
	final ObjectMapper objectMapper = new ObjectMapper();
	final Writer w = new StringWriter();
	try {
	    objectMapper.writeValue(w, jsonItems);
	} catch (IOException e) {
	    throw new RuntimeException(e);
	}
	return w.toString();

    }
}

