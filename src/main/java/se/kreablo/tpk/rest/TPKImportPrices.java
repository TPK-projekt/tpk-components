/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.rest;

import javax.ws.rs.PUT;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.Status;

import org.xwiki.component.annotation.Component;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Provider;

import java.io.Reader;
import java.io.IOException;
import org.slf4j.Logger;
import java.util.Map;

import static se.kreablo.tpk.Constants.SPECIAL_PARAMETER;
import se.kreablo.tpk.objectimport.tpk.PriceConduit;
import se.kreablo.tpk.objectimport.tpk.CSVSourceConfig;
import se.kreablo.tpk.objectimport.ConduitException;

/**
 * Rest service for importing price data.
 * @version $Id$
 */
@Component("se.kreablo.tpk.rest.TPKImportPrices")
@Path("/tpk-import-prices")
public class TPKImportPrices extends AbstractImportService
{

    @Inject
    @Named("csvtotable")
    private Provider<PriceConduit<CSVSourceConfig>> priceConduitProvider;

    @Inject
    private Logger logger;

    /**
     * Put method uploads CSV data and adds it to the database under the next version number.
     * @param content The CSV content.
     * @param special If measure prices should be special.
     * @return A json object.
     */
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("text/csv")
    public Object put(Reader content, @DefaultValue("false") @QueryParam(SPECIAL_PARAMETER) Boolean special) {

        checkAccess();

        try {
            final Map<String, Object> ret = priceConduitProvider.get().run(
                new CSVSourceConfig(content), special, c -> { addCloseable(c); });
            return ret;
        } catch (ConduitException e) {
            logger.error("Exception when importing prices.", e);
            throw new WebApplicationException(e, Status.INTERNAL_SERVER_ERROR);
        } catch (WebApplicationException e) {
            logger.trace("WebApplicationException", e);
            throw e;
        } catch (Exception e) {
            logger.error("Exception when importing prices.", e);
            throw new WebApplicationException(e, Status.INTERNAL_SERVER_ERROR);
        } finally {
            try {
                close();
            } catch (Exception e) {
                logger.error("Exception when finishing importing prices.", e);
                throw new WebApplicationException(e, Status.INTERNAL_SERVER_ERROR);
            }
        }
    }
    
}


