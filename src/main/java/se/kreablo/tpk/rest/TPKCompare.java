/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.rest;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.MediaType;

import org.xwiki.component.annotation.Component;
import org.xwiki.rest.XWikiResource;

import se.kreablo.tpk.Compare;
import se.kreablo.tpk.Parameters;
import se.kreablo.tpk.internal.IntegerSetFactory;
import se.kreablo.tpk.internal.StringSetFactory;

import org.slf4j.Logger;

import java.util.Set;
import java.util.HashSet;

import javax.ws.rs.core.StreamingOutput;
import java.io.OutputStream;
import java.io.IOException;

import org.jooq.Record;
import org.jooq.Result;

import se.kreablo.tpk.CompareResult;
import se.kreablo.tpk.RendererException;
import static se.kreablo.tpk.Constants.*;

/**
 * 
 */
@Component("se.kreablo.tpk.rest.TPKCompare")
@Path("/tpk-compare")
public class TPKCompare extends XWikiResource
{

    @Inject
    Compare compare;

    @Inject
    Logger logger;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Object
        get(@QueryParam(OFFSET_PARAMETER)  Integer offset,
            @QueryParam(LIMIT_PARAMETER)   Integer limit,
            @QueryParam(MEASURES_PARAMETER)       IntegerSetFactory measures,
            @QueryParam(PACKAGES_PARAMETER)       IntegerSetFactory packages,
            @QueryParam(SURGERIES_PARAMETER)      IntegerSetFactory surgeries,
            @QueryParam(SORT_ORDER_PARAMETER) String sortOrder,
            @QueryParam(ORDER_BY_PARAMETER) String orderBy,
            @QueryParam(COUNTY_PARAMETER) StringSetFactory county,
            @QueryParam(MUNICIPALITY_PARAMETER) StringSetFactory municipality,
            @QueryParam(SUBLOCALITY_PARAMETER) StringSetFactory sublocality,
            @QueryParam(EVENING_HOURS_PARAMETER) Boolean eveningHours,
            @QueryParam(WEEKEND_HOURS_PARAMETER) Boolean weekendHours,
            @QueryParam(SURGERY_CATEGORY_PARAMETER) String surgeryCategory,
            @QueryParam(HAVE_PARKING_PARAMETER) Boolean haveParking,
            @QueryParam(PUBLIC_TRANSPORT_PARAMETER) Boolean publicTransport,
            @QueryParam(DENTAL_FEAR_PARAMETER) Boolean dentalFear,
            @QueryParam(WARRANTIES_PARAMETER) Boolean warranties,
            @QueryParam(REGION_PARAMETER) String region,
            @QueryParam(SPECIAL_PARAMETER) Boolean special,
            @QueryParam(COUNT_PARAMETER)  Integer count
            )
    {
        Parameters parameters = new Parameters();
        parameters.setRegion(Parameters.Region.fromString(region));
        parameters.setPublicTransport(publicTransport);
        parameters.setDentalFear(dentalFear);
        parameters.setWarranties(warranties);
        parameters.setHaveParking(haveParking);
        parameters.setSurgeryCategory(Parameters.SurgeryCategory.fromString(surgeryCategory));
        parameters.setEveningHours(eveningHours);
        parameters.setWeekendHours(weekendHours);
        if (sublocality != null) {
            parameters.addSublocalities(sublocality.getSet());
        }
        if (county != null) {
            parameters.addCounties(county.getSet());
        }
        if (municipality != null) {
            parameters.addMunicipalities(municipality.getSet());
        }
        parameters.setOffset(offset);
        parameters.setLimit(limit);
        parameters.setSpecial(special);
        parameters.setCount(count);
        if (measures != null) {
            parameters.addMeasures(measures.getSet());
        }
        if (packages != null) {
            parameters.addPackages(packages.getSet());
        }
        if (surgeries != null) {
            parameters.addSurgeries(surgeries.getSet());
        }
        parameters.setSortOrder(Parameters.SortOrder.fromString(sortOrder));
        parameters.setOrderBy(Parameters.OrderBy.fromString(orderBy));
        logger.trace("TPKCompare rest comparing " + parameters);

        final Map<String, Object> ret = new HashMap<String, Object>();
        ret.put("fields", CompareResult.getJSONFields());

        List<List<Object>> records = new ArrayList<List<Object>>();
        ret.put("records", records);
        try {
            for (CompareResult result : compare.compare(parameters)) {
                records.add(result.getJSONRecord());
            }
            ret.put("count", parameters.getCount());
        } catch (RendererException e) {
            logger.error("Renderer exception", e);
        }
        return ret;
    }

    private StreamingOutput jsonOutput(final Result<? extends Record> result) {
        logger.debug("jsonOutput");
        return new StreamingOutput() {
            @Override
            public void write(OutputStream out) throws IOException {
                logger.debug("jsonOutput.write");
                try {
                    result.formatJSON(out);
                } catch (RuntimeException e) {
                    logger.error("Exception in formatJSON.", e);
                    throw e;
                }
                logger.debug("flushing");
                out.flush();
            }
        };
    }

}
