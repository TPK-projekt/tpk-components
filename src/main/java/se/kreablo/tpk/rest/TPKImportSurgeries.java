/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.rest;

import javax.ws.rs.PUT;
import javax.ws.rs.DELETE;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.Response;

import org.xwiki.component.annotation.Component;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Provider;

import java.io.Reader;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import static java.util.Arrays.asList;

import se.kreablo.tpk.objectimport.tpk.SurgerySink;
import se.kreablo.tpk.objectimport.tpk.SurgerySource;
import se.kreablo.tpk.objectimport.tpk.SurgeryDeleterSink;
import se.kreablo.tpk.objectimport.tpk.SurgeryIdSource;
import se.kreablo.tpk.objectimport.tpk.CSVSourceConfig;
import se.kreablo.tpk.objectimport.RecordConduit;
import se.kreablo.tpk.objectimport.FieldConduit;
import se.kreablo.tpk.objectimport.ConduitException;


/**
 * Rest service for importing surgery data.
 * @version $Id$
 */
@Component("se.kreablo.tpk.rest.TPKImportSurgeries")
@Path("/tpk-import-surgeries")
public class TPKImportSurgeries extends AbstractImportService
{

    @Inject
    @Named("object")
    private Provider<SurgerySink> surgerySinkProvider;

    @Inject
    @Named("csv")
    private Provider<SurgerySource<CSVSourceConfig>> surgerySourceProvider;

    @Inject
    @Named("csv")
    private Provider<SurgeryIdSource<CSVSourceConfig>> surgeryIdSourceProvider;

    @Inject
    @Named("document")
    private Provider<SurgeryDeleterSink> surgeryDeleterSinkProvider;

    /**
     * Put method uploads CSV data and adds it to the database under the next version number.
     * @param content The CSV content.
     * @param special If measure prices should be special.
     * @return A json object.
     */
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("text/csv")
    public Response put(Reader content) {
        checkAccess();

        final SurgerySink surgerySink = surgerySinkProvider.get();
        final SurgerySource<CSVSourceConfig> surgerySource = surgerySourceProvider.get();

        try {

            surgerySource.start(new CSVSourceConfig(content));

            RecordConduit record = assembleRecord(surgerySource, surgerySink);

            final Map<String, Object> ret = new HashMap<>();

            List<Exception> exceptions = record.run();

            if (surgerySource.getWarnings().size() > 0) {
                ret.put("warnings", surgerySource.getWarnings());
            }

            return maybeAddExceptionMessages(ret, exceptions);

        } catch (ConduitException e) {
            getLogger().error("Import of surgeries failed", e);
            throw new WebApplicationException(e, Status.INTERNAL_SERVER_ERROR);
        } catch (RuntimeException e) {
            getLogger().error("Exception when uploading surgeries.", e);
            throw new WebApplicationException(e, Status.INTERNAL_SERVER_ERROR);
        } finally {
            for (Object o : asList(surgerySink, surgerySource)) {
                if (o instanceof AutoCloseable) {
                    try {
                        ((AutoCloseable) o).close();
                    } catch (Exception e) {
                        getLogger().error("Exception when closing", e);
                    }
                }
            }
        }
    }

    private RecordConduit assembleRecord(SurgerySource<?> source, SurgerySink sink) {
        final FieldConduit<?> [] fcs = {
            new FieldConduit<Integer>(source.getIdSource(), sink.getIdSink()),
            new FieldConduit<String>(source.getNameSource(), sink.getNameSink()),
            new FieldConduit<String>(source.getCompanySource(), sink.getCompanySink()),
            new FieldConduit<Double>(source.getLatitudeSource(), sink.getLatitudeSink()),
            new FieldConduit<Double>(source.getLongitudeSource(), sink.getLongitudeSink()),
            new FieldConduit<String>(source.getAddressSource(), sink.getAddressSink()),
            new FieldConduit<String>(source.getPostalCodeSource(), sink.getPostalCodeSink()),
            new FieldConduit<String>(source.getCitySource(), sink.getCitySink()),
            new FieldConduit<String>(source.getMunicipalitySource(), sink.getMunicipalitySink()),
            new FieldConduit<String>(source.getCountySource(), sink.getCountySink()),
            new FieldConduit<String>(source.getSublocalitySource(), sink.getSublocalitySink()),
            new FieldConduit<String>(source.getCategorySource(), sink.getCategorySink()),
            new FieldConduit<String>(source.getEmailSource(), sink.getEmailSink()),
            new FieldConduit<String>(source.getWebUrlSource(), sink.getWebUrlSink()),
            new FieldConduit<String>(source.getPhoneSource(), sink.getPhoneSink())
        };

        return new RecordConduit(fcs);
    }

    /**
     * Delete the surgery objects identified by the uploaded CSV document.
     * @param content The CSV content.
     * @return A json object.
     */
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(Reader content) {
        checkAccess();

        final SurgeryDeleterSink surgeryDeleterSink = surgeryDeleterSinkProvider.get();
        final SurgeryIdSource<CSVSourceConfig> surgeryIdSource = surgeryIdSourceProvider.get();

        try {

            surgeryIdSource.start(new CSVSourceConfig(content));

            final RecordConduit record = new RecordConduit(surgeryIdSource.getIdSource(),
                                                           surgeryDeleterSink.getIdSink());

            final List<Exception> exceptions = record.run();

            final Map<String, Object> ret = new HashMap<>();

            return maybeAddExceptionMessages(ret, exceptions);

        } catch (ConduitException e) {
            getLogger().error("Deletion of surgeries failed", e);
            throw new WebApplicationException(e, Status.INTERNAL_SERVER_ERROR);
        } catch (RuntimeException e) {
            getLogger().error("Exception when deleting surgeries", e);
            throw e;
        }

    }
}
