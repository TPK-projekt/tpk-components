/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.rest;

import org.xwiki.component.annotation.Component;
import org.xwiki.rest.XWikiResource;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.core.MediaType;

import javax.inject.Inject;

import se.kreablo.tpk.InfoboxRenderer;
import se.kreablo.tpk.InfoboxRendererException;
import se.kreablo.tpk.RendererException;
import se.kreablo.tpk.Package;

import org.slf4j.Logger;

import static se.kreablo.tpk.Constants.*;

import java.util.Map;
import java.util.HashMap;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.List;
import java.util.ArrayList;

import java.io.Writer;
import java.io.StringWriter;
import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;

@Component("se.kreablo.tpk.rest.TPKInfoboxRenderer")
@Path("/tpk-infobox")
public class TPKInfoboxRenderer extends XWikiResource
{
    @Inject
    private InfoboxRenderer infoboxRenderer;
    
    @Inject
    private Logger logger;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String get(@QueryParam("infoboxes%5B%5D") List<String> infoboxesList,
                      @QueryParam("mareas%5B%5D") List<String> mareasList,
                      @QueryParam("packages%5B%5D") List<Integer> packagesList,
                      @QueryParam("measures%5B%5D") List<Integer> measuresList,
                      @DefaultValue("false") @QueryParam(SPECIAL_PARAMETER) Boolean special)  {

        SortedSet<String> infoboxes = new TreeSet<String>();
        SortedSet<String> mareas = new TreeSet<String>();
        SortedSet<Integer> packages = new TreeSet<Integer>();
        SortedSet<Integer> measures = new TreeSet<Integer>();

        for (String infobox : infoboxesList) {
            if (infobox != null) {
                infoboxes.add(infobox);
            }
        }
        for (String marea : mareasList) {
            if (marea != null) {
                mareas.add(marea);
            }
        }
        for (Integer p : packagesList) {
            if (p != null) {
                packages.add(p);
            }
        }
        for (Integer m : measuresList) {
            if (m !=  null) {
                measures.add(m);
            }
        }

        Map<String, Object> jsonItems = new HashMap<String, Object>();
        List<String> errors = new ArrayList<String>();

        logger.debug("tpk-infobox {} {} {} {} {}", infoboxes, mareas, packages, measures, special);
	
        if (infoboxes.size() > 0) {
            try {
                jsonItems.put("infoboxes", infoboxRenderer.renderInfoboxes(infoboxes));
            } catch (InfoboxRendererException e) {
                logger.warn("Caught exception", e);
                errors.add(e.getMessage());
            } catch (RendererException e) {
                logger.warn("Caught exception", e);
                errors.add(e.getMessage());
            }

        }
        if (mareas.size() > 0) {
            try {
                jsonItems.put("mareas", infoboxRenderer.renderMAreaInfoboxes(mareas));
            } catch (InfoboxRendererException e) {
                logger.warn("Caught exception", e);
                errors.add(e.getMessage());
            } catch (RendererException e) {
                logger.warn("Caught exception", e);
                errors.add(e.getMessage());
            }

        }
        if (packages.size() > 0) {
            try {
                final Map<Integer, Package> ps = infoboxRenderer.renderPackageInfoboxes(packages);
                for (Package p : ps.values()) {
                    for (Integer m : p.getPackageMeasureCodes()) {
                        measures.add(m);
                    }
                }
                jsonItems.put("packages", ps);
            } catch (InfoboxRendererException e) {
                logger.warn("Caught exception", e);
                errors.add(e.getMessage());
            } catch (RendererException e) {
                logger.warn("Caught exception", e);
                errors.add(e.getMessage());
            }
        }
        if (measures.size() > 0) {
            try {
                jsonItems.put("measures", infoboxRenderer.renderMeasureInfoboxes(measures, special));
            } catch (InfoboxRendererException e) {
                logger.warn("Caught exception", e);
                errors.add(e.getMessage());
            } catch (RendererException e) {
                logger.warn("Caught exception", e);
                errors.add(e.getMessage());
            }

        }
        if (errors.size() > 0) {
            jsonItems.put("errors", errors);
        }

        ObjectMapper objectMapper = new ObjectMapper();
        Writer w = new StringWriter();
        try {
            objectMapper.writeValue(w, jsonItems);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return w.toString();
    }
}
