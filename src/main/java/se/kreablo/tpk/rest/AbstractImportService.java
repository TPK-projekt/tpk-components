/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.rest;

import org.xwiki.rest.XWikiResource;
import org.xwiki.security.authorization.AuthorizationManager;
import org.xwiki.security.authorization.Right;
import org.xwiki.model.reference.DocumentReference;
import org.xwiki.model.reference.WikiReference;

import com.xpn.xwiki.XWikiContext;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.MediaType;
import javax.inject.Inject;
import javax.inject.Provider;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.io.Closeable;
import java.io.IOException;

import org.slf4j.Logger;

import se.kreablo.tpk.objectimport.ConduitException;

/**
 * Super class for import rest components.
 * @version $Id$
 */
public abstract class AbstractImportService extends XWikiResource implements Closeable
{

    @Inject
    private AuthorizationManager authorizationManager;

    @Inject
    private Provider<XWikiContext> xcontextProvider;

    @Inject
    private Logger logger;

    private final List<Closeable> closeables = new ArrayList<>();

    /**
     * Check access and throw an exception if permission is denied.
     * @throws WebApplicationException if the access is denied.
     */
    protected void checkAccess() throws WebApplicationException {
        final DocumentReference currentUser = xcontextProvider.get().getUserReference();

        if (!this.authorizationManager.hasAccess(
                Right.ADMIN,
                currentUser,
                new WikiReference(xcontextProvider.get().getWikiId()))
            ) {
            throw new WebApplicationException(Status.UNAUTHORIZED);
        }
    }

    /**
     * Add an object that may be closeable which should be closed at
     * the end of the request.
     * @param o The object.
     */
    protected void addCloseable(Object o) {
        if (o instanceof Closeable) {
            closeables.add((Closeable) o);
        }
    }

    /**
     * @param ret Return value to be serialized to json.
     * @param exceptions List of exceptions.
     */
    protected Response maybeAddExceptionMessages(Map<String, Object> ret, List<Exception> exceptions) {
        Status status = null;
        if (exceptions.size() > 0) {
            List<String> msgs = new ArrayList<>();
            for (Exception e : exceptions) {
                msgs.add(e.toString());
                if (e instanceof ConduitException) {
                    status = Status.BAD_REQUEST;
                }
            }
            ret.put("exceptions", msgs);
        }
        if (status != null) {
            return Response.status(status).entity(ret).build();
        }
        return Response.ok(ret, MediaType.APPLICATION_JSON).build();
    }

    /** @return the logger. */
    protected Logger getLogger() {
        return logger;
    }

    @Override
    public void close() throws IOException {
        IOException caught = null;
        for (Closeable c : closeables) {
            try {
                c.close();
            } catch (IOException e) {
                if (caught != null) {
                    caught = e;
                }
            }
        }
        if (caught != null) {
            throw caught;
        }
    }
}
