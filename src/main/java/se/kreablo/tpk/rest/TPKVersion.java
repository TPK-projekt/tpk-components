/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.rest;

import org.xwiki.component.annotation.Component;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.inject.Provider;

import javax.ws.rs.PUT;
import javax.ws.rs.GET;
import javax.ws.rs.DELETE;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.MediaType;

import java.util.Map;
import java.util.HashMap;

import se.kreablo.tpk.objectimport.tpk.VersionService;

/**
 * Rest service for version number.
 * @version $Id$
 */
@Component("se.kreablo.tpk.rest.TPKVersion")
@Path("/tpk-version")
@Singleton
public class TPKVersion extends AbstractImportService {

    @Inject
    @Named("table")
    private Provider<VersionService> versionServiceProvider;

    /**
     * Get the current version of the data.
     * @return json object with the property version.
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Object get() {
        getLogger().trace("GET tpk-version");
        try {

            int version = versionServiceProvider.get().getVersion();
        
            final Map<String, Object> ret = new HashMap<>();

            ret.put("version", version);
            return ret;
        } catch (RuntimeException e) {
            getLogger().error("Exception when getting version.", e);
            throw e;
        }
    }

    /**
     * Set version number.
     * @param version The new version.
     * @return A json object.
     */
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{version}")
    public Object put(@PathParam("version") Integer version) {

        checkAccess();

        if (version == null) {
            throw new WebApplicationException(Status.BAD_REQUEST);
        }

        try {
            versionServiceProvider.get().setVersion(version);
        
            final Map<String, Object> ret = new HashMap<>();

            return ret;
        } catch (RuntimeException e) {
            getLogger().error("Exception when updating version to {}", version, e);
            throw e;
        }
    }

    /**
     * Delete all objects for a particular version.
     * @param version The version to delete.
     * @return A json object.
     */
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{version}")
    public Object delete(@PathParam("version") Integer version) {
        checkAccess();

        if (version == null) {
            throw new WebApplicationException(Status.BAD_REQUEST);
        }

        final int curVersion = versionServiceProvider.get().getVersion();

        if (version == curVersion) {
            throw new WebApplicationException(Status.CONFLICT);
        }

        try {
            versionServiceProvider.get().deleteVersion(version);

            final Map<String, Object> ret = new HashMap<>();

            return ret;
        } catch (RuntimeException e) {
            getLogger().error("Exception when deleting version {}", version, e);
            throw e;
        }
    }
    
}
                                       
