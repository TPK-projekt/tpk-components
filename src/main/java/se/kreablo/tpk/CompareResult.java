/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk;

import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import static se.kreablo.tpk.Constants.*;

public class CompareResult {

    private final Integer sid;

    private final String surgeryName;

    private final String surgeryAddress;

    private final String surgeryDescription;

    private String renderedSurgeryDescription;
    
    private final Double longitude;

    private final Double latitude;

    private final Integer totalPrice;
    
    private final Integer[] referencePrice;

    private final Integer[] items;

    private final Integer[] refItems;

    private final Boolean[] isMeasure;
    
    private final String[] comments;

    private final Boolean[] activeInArea;

    private final String[] areas;

    private final Integer count;

    private final TextRenderer textRenderer;

    private String zipcode;

    private String city;

    public CompareResult(TextRenderer textRenderer,
                         Integer sid, String surgeryName, String surgeryAddress,
                         String zipcode, String city, String surgeryDescription, Double longitude, Double latitude,
                         Integer totalPrice, Integer[] referencePrice, Integer[] items, Integer refItems[], Boolean[] isMeasure,
                         String[] comments, Boolean[] activeInArea, String[] areas, Integer count) {
        this.sid = sid;
        this.surgeryName = surgeryName;
        this.surgeryAddress = surgeryAddress;
        this.zipcode = zipcode;
        this.city = city;
        this.surgeryDescription = surgeryDescription;
        this.longitude = longitude;
        this.latitude = latitude;
        this.totalPrice = totalPrice;
        this.referencePrice = referencePrice;
        this.items = items;
        this.refItems = refItems;
        this.isMeasure = isMeasure;
        this.comments = comments;
        this.activeInArea = activeInArea;
        this.areas = areas;
        this.textRenderer = textRenderer;
        this.count = count;
    }

    public Integer getSid() {
        return this.sid;
    }

    public String getSurgeryName() {
        return this.surgeryName;
    }

    public String getSurgeryAddress() {
        return this.surgeryAddress;
    }

    public String getSurgeryDescription() {
        return this.surgeryDescription;
    }

    public String getRenderedSurgeryDescription() throws RendererException {
        if (renderedSurgeryDescription ==  null) {
            renderedSurgeryDescription = textRenderer.renderText(surgeryDescription, SURGERY_VIEW_SPACE + "." + getSid() + ".WebHome");
        }
        return renderedSurgeryDescription;
    }

    public Double getLongitude() {
        return this.longitude;
    }

    public Double getLatitude() {
        return this.latitude;
    }

    public Integer getTotalPrice() {
        return this.totalPrice;
    }

    public Integer[] getReferencePrice() {
        return this.referencePrice;
    }

    public Integer[] getItems() {
        return this.items;
    }

    public Integer[] getRefItems() {
        return this.refItems;
    }

    public Boolean[] getIsMeasure() {
        return this.isMeasure;
    }
    
    public String[] getComments() {
        return this.comments;
    }

    public Boolean[] getActiveInArea() {
        return this.activeInArea;
    }
    
    public String[] getAreas() {
        return this.areas;
    }

    public Integer getCount() {
        return this.count;
    }

    public String getZipcode() {
        return this.zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getCity() {
        return this.city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public static List<Map<String, String>> getJSONFields() {
        return new ArrayList() {
            {
                a("s_id", "INTEGER");
                a("s_name", "CLOB");
                a("s_address", "CLOB");
                a("s_zipcode", "CLOB");
                a("s_city", "CLOB");
                a("s_description", "CLOB");
                a("lon", "DOUBLE");
                a("lat", "DOUBLE");
                a("price_total", "INTEGER");
                a("ref_price", "BIGINT");
                a("ref_items", "INTEGER");
                a("items", "INTEGER");
                a("is_m", "BOOLEAN");
                a("comments", "CLOB");
                a("area_active", "BOOLEAN");
                a("areas", "CLOB");
                a("n", "INTEGER");
            }

            private void a(String name, String type) {
                final Map<String, String> m = new HashMap<String, String>();
                m.put("name", name);
                m.put("type", type);
                add(m);
            }
        };
    }

    public List<Object> getJSONRecord() throws RendererException {
        return new ArrayList() {
            {
                add(getSid());
                add(getSurgeryName());
                add(getSurgeryAddress());
                add(getZipcode());
                add(getCity());
                add(getRenderedSurgeryDescription());
                add(getLongitude());
                add(getLatitude());
                add(getTotalPrice());
                add(getReferencePrice());
                add(getRefItems());
                add(getItems());
                add(getIsMeasure());
                add(getComments());
                add(getActiveInArea());
                add(getAreas());
                add(getCount());
            }
        };
    }

}
