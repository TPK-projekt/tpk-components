/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk;

import java.util.Set;
import java.util.HashSet;
import java.net.URLEncoder;
import java.io.UnsupportedEncodingException;

import org.apache.commons.lang3.StringUtils;

import static se.kreablo.tpk.Constants.*;

public class Parameters {

    private Integer offset;

    private Integer limit;

    private Integer count;

    private boolean special;

    private OrderBy orderBy;

    private SortOrder sortOrder = SortOrder.DEFAULT;

    private Set<String> counties = new HashSet<String>();

    private Set<String> municipalities = new HashSet<String>();

    private Set<String> sublocalities = new HashSet<String>();

    private final Set<Integer> packages = new HashSet<Integer>();

    private final Set<Integer> measures = new HashSet<Integer>();

    private final Set<Integer> surgeries = new HashSet<Integer>();

    private SurgeryCategory surgeryCategory;

    private Boolean haveParking;

    private Boolean publicTransport;

    private Boolean dentalFear;

    private Boolean warranties;

    private Boolean weekendHours;

    private Boolean eveningHours;

    private Region region;

    public Parameters() {
    }

    public Boolean getEveningHours() {
        return this.eveningHours;
    }

    public void setEveningHours(Boolean eveningHours) {
        this.eveningHours = eveningHours;
    }

    public Boolean getWeekendHours() {
        return this.weekendHours;
    }

    public void setWeekendHours(Boolean weekendHours) {
        this.weekendHours = weekendHours;
    }

    public SurgeryCategory getSurgeryCategory() {
        return this.surgeryCategory;
    }

    public void setSurgeryCategory(SurgeryCategory surgeryCategory) {
        this.surgeryCategory = surgeryCategory;
    }

    public Boolean getHaveParking() {
        return this.haveParking;
    }

    public void setHaveParking(Boolean haveParking) {
        this.haveParking = haveParking;
    }

    public Boolean getPublicTransport() {
        return this.publicTransport;
    }

    public void setPublicTransport(Boolean publicTransport) {
        this.publicTransport = publicTransport;
    }

    public Boolean getDentalFear() {
        return this.dentalFear;
    }

    public void setDentalFear(Boolean dentalFear) {
        this.dentalFear = dentalFear;
    }

    public Boolean getWarranties() {
        return this.warranties;
    }

    public void setWarranties(Boolean warranties) {
        this.warranties = warranties;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getOffset() {
        return this.offset;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getLimit() {
        return this.limit;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getCount() {
        return this.count;
    }

    public void addMeasure(Integer measure) {
        this.measures.add(measure);
    }

    public void addMeasures(Set<Integer> measures) {
        this.measures.addAll(measures);
    }

    public Set<Integer> getMeasures() {
        return this.measures;
    }

    public void addPackage(Integer pkg) {
        this.packages.add(pkg);
    }

    public void addPackages(Set<Integer> pkgs) {
        this.packages.addAll(pkgs);
    }

    public Set<Integer> getPackages() {
        return this.packages;
    }

    public void addSurgery(Integer surgery) {
        this.surgeries.add(surgery);
    }

    public void addSurgeries(Set<Integer> surgeries) {
        this.surgeries.addAll(surgeries);
    }

    public Set<Integer> getSurgeries() {
        return this.surgeries;
    }

    public void setSpecial(Boolean special) {
        if (special == null) {
            this.special = false;
        } else {
            this.special = special;
        }
    }

    public boolean isSpecial() {
        return special;
    }

    public void setOrderBy(OrderBy orderBy) {
        this.orderBy = orderBy;
    }
    
    public OrderBy getOrderBy() {
        return orderBy;
    }

    public void setSortOrder(SortOrder sortOrder) {
        this.sortOrder = sortOrder;
    }
    
    public SortOrder getSortOrder() {
        return sortOrder;
    }

    public void addCounties(Set<String> counties) {
        this.counties.addAll(counties);
    }

    public Set<String> getCounties() {
        return this.counties;
    }

    public void addMunicipalities(Set<String> municipalities) {
        this.municipalities.addAll(municipalities);
    }

    public Set<String> getMunicipalities() {
        return this.municipalities;
    }

    public void addSublocalities(Set<String> sublocalities) {
        this.sublocalities.addAll(sublocalities);
    }

    public Set<String> getSublocalities() {
        return this.sublocalities;
    }

    public Region getRegion() {
        return this.region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public static OrderBy orderByFromString(String s) {
        if (s == null || s.equals("price")) {
            return OrderBy.PRICE;
        }
        if (s.equals("s_name")) {
            return  OrderBy.SURGERY_NAME;
        }
        throw new IllegalArgumentException("Not a OrderBy value: " + s);
    }
    
    public static enum OrderBy {
        PRICE("price"),
        SURGERY_NAME("s_name");
	
        private final String name;

        private OrderBy(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return name;
        }
	
        public static OrderBy fromString(String s) {
            return orderByFromString(s);
        }
    }

    public static SortOrder sortOrderFromString(String s) {
        if (s == null || s.equals("asc")) {
            return SortOrder.ASC;
        }
        if (s.equals("desc")) {
            return  SortOrder.DESC;
        }
        if (s.equals("default")) {
            return  SortOrder.DEFAULT;
        }
        throw new IllegalArgumentException("Not a SortOrder value: " + s);
    }

    public static enum SortOrder {
        ASC("asc"),
        DESC("desc"),
        DEFAULT("default");

        private final String name;

        private SortOrder(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return name;
        }
	
        public static SortOrder fromString(String s) {
            return sortOrderFromString(s);
        }
    }

    public static SurgeryCategory surgeryTypeFromString(String s) {
        if (s == null) {
            return null;
        }
        if ("privat".equalsIgnoreCase(s)) {
            return SurgeryCategory.PRIVATE;
        }
        if ("folktandvård".equalsIgnoreCase(s) || "landsting".equalsIgnoreCase(s)) {
            return SurgeryCategory.PUBLIC;
        }
        throw new IllegalArgumentException("Not a SurgeryCategory value: " + s);
    }
    
    public static enum SurgeryCategory {
        PRIVATE("privat"),
        PUBLIC("folktandvård");

        private final String name;

        private SurgeryCategory(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return name;
        }

        public static SurgeryCategory fromString(String s) {
            return surgeryTypeFromString(s);
        }
    }

    public static Region regionFromString(final String s) {
        if (s == null) {
            return null;
        }
        if (Region.COUNTRY.toString().equalsIgnoreCase(s)) {
            return Region.COUNTRY;
        }
        if (Region.COUNTY.toString().equalsIgnoreCase(s)) {
            return Region.COUNTY;
        }
        if (Region.SURROUNDING_COUNTIES.toString().equalsIgnoreCase(s)) {
            return Region.SURROUNDING_COUNTIES;
        }
        if (Region.MUNICIPALITY.toString().equalsIgnoreCase(s)) {
            return Region.MUNICIPALITY;
        }
        if (Region.SUBLOCALITY.toString().equalsIgnoreCase(s)) {
            return Region.SUBLOCALITY;
        }
        throw new IllegalArgumentException("Not a region value: " + s);
    }
    
    public static enum Region {
        COUNTRY("landet"),
        COUNTY("länet"),
        SURROUNDING_COUNTIES("angränsande län"),
        MUNICIPALITY("kommun"),
        SUBLOCALITY("stadsdel");

        private final String name;
	
        private Region(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return this.name;
        }

        public static Region fromString(String s) {
            return regionFromString(s);
        }
    }

    @Override
    public String toString() {
        return
            "{offset: " + offset + " " +
            "limit: " + limit + " " +
            "count: " + count + " " +
            "special: " + special + " " +
            "orderBy: " + orderBy + " " +
            "sortOrder: " + sortOrder + " " +
            "county: " + counties + " " +
            "municipality: " + municipalities + " " +
            "sublocality: " + sublocalities + " " +
            "region: " + region + " " +
            "packages: " + packages + " " +
            "measures: " +  measures + " " +
            "surgeries: " + surgeries +
            "cagegory: "  + surgeryCategory + " " +
            "haveParking: " + haveParking + " " +
            "publicTransport: " + publicTransport + " " +
            "dentalFear: " + dentalFear + " " +
            "warranties: " + warranties + " " + 
            "weekendHours: " + weekendHours + " " +
            "eveningHours: " + eveningHours + "}";
    }

    private static class QueryStringBuilder {
        private boolean first = true;

        private StringBuilder qs = new StringBuilder();

        public QueryStringBuilder a(String name, Object value) {
            if (value == null) {
                return this;
            }
            if (first) {
                first = false;
            } else {
                qs.append("&");
            }
            qs.append(name).append('=');
            try {
                if (value instanceof Iterable) {
                    boolean f = true;
                    for (Object v : (Iterable) value) {
                        if (f) {
                            f = false;
                        } else {
                            qs.append(';');
                        }
                        qs.append(URLEncoder.encode(v.toString(), "UTF-8"));
                    }
                } else {
                    qs.append(URLEncoder.encode(value.toString(), "UTF-8"));
                }
            } catch (UnsupportedEncodingException e) {
                throw new RuntimeException("No utf-8 support!", e);
            }
            return this;
        }

        @Override
        public String toString() {
            return qs.toString();
        }

    }

    public String queryString() {
        return new QueryStringBuilder()
            .a(OFFSET_PARAMETER, offset)
            .a(LIMIT_PARAMETER, limit)
            .a(MEASURES_PARAMETER, measures)
            .a(PACKAGES_PARAMETER, packages)
            .a(SURGERIES_PARAMETER, surgeries)
            .a(SORT_ORDER_PARAMETER, sortOrder)
            .a(ORDER_BY_PARAMETER, orderBy)
            .a(COUNTY_PARAMETER, StringUtils.join(counties, ARRAY_DELIM))
            .a(MUNICIPALITY_PARAMETER, StringUtils.join(municipalities, ARRAY_DELIM))
            .a(SUBLOCALITY_PARAMETER, StringUtils.join(sublocalities, ARRAY_DELIM))
            .a(REGION_PARAMETER, region)
            .a(EVENING_HOURS_PARAMETER, eveningHours)
            .a(WEEKEND_HOURS_PARAMETER, weekendHours)
            .a(SURGERY_CATEGORY_PARAMETER, surgeryCategory)
            .a(HAVE_PARKING_PARAMETER, haveParking)
            .a(PUBLIC_TRANSPORT_PARAMETER, publicTransport)
            .a(DENTAL_FEAR_PARAMETER, dentalFear)
            .a(WARRANTIES_PARAMETER, warranties)
            .toString();
    }
}

