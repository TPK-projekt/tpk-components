/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.script;

import org.xwiki.component.annotation.Component;
import org.xwiki.script.service.ScriptService;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.util.SortedSet;

import se.kreablo.tpk.InfoboxRenderer;
import se.kreablo.tpk.InfoboxRendererException;
import se.kreablo.tpk.RendererException;

import org.slf4j.Logger;

@Component
@Named("infobox")
@Singleton
public class InfoboxRendererService implements ScriptService {

    @Inject
    private InfoboxRenderer infoboxRenderer;

    @Inject
    private Logger logger;

    public InfoboxRendererService() {
    }

    public Object renderInfoboxes(SortedSet<String> infoboxes) {
	try {
	    return infoboxRenderer.renderInfoboxes(infoboxes);
	} catch (InfoboxRendererException e) {
	    return null;
	} catch (RendererException e) {
	    return null;
	}
    }

    Object renderMAreaInfoboxes(SortedSet<String> mareas) {
	try {
	    return infoboxRenderer.renderMAreaInfoboxes(mareas);
	} catch (InfoboxRendererException e) {
	    return null;
	} catch (RendererException e) {
	    return null;
	}
    }
	

    Object renderPackageInfoboxes(SortedSet<Integer> packages) {
	try {
	    return infoboxRenderer.renderPackageInfoboxes(packages);
	} catch (InfoboxRendererException e) {
	    return null;
	} catch (RendererException e) {
	    return null;
	}
    }


    Object renderMeasureInfoboxes(SortedSet<Integer> measures, boolean special) {
	try {
	    return infoboxRenderer.renderMeasureInfoboxes(measures, special);
	} catch (InfoboxRendererException e) {
	    return null;
	} catch (RendererException e) {
	    return null;
	}
    }
    
}
