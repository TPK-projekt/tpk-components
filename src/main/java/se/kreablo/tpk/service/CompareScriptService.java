/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.script;

import org.xwiki.component.annotation.Component;
import org.xwiki.script.service.ScriptService;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import org.slf4j.Logger;

import se.kreablo.tpk.Compare;
import se.kreablo.tpk.Parameters;
import se.kreablo.tpk.CompareResult;
import se.kreablo.tpk.objectimport.tpk.VersionService;
import static se.kreablo.tpk.Constants.*;

@Component
@Named("tpkCompare")
@Singleton
public class CompareScriptService implements ScriptService {

    @Inject
    private Compare compare;

    @Inject
    private Logger logger;

    @Inject
    @Named("table")
    private VersionService versionService;

    public CompareScriptService() {
    }

    public Parameters newParameters() {
	return new Parameters();
    }

    public Iterable<CompareResult> compare(Parameters parameters) {
	return compare.compare(parameters);

    }

    public String getARRAY_DELIM() {
	return ARRAY_DELIM;
    }

    public String getOFFSET_PARAMETER() {
	return OFFSET_PARAMETER;
    }

    public String getLIMIT_PARAMETER() {
	return LIMIT_PARAMETER;
    }

    public String getMEASURES_PARAMETER() {
	return MEASURES_PARAMETER;
    }

    public String getPACKAGES_PARAMETER() {
	return PACKAGES_PARAMETER;
    }

    public String getSURGERIES_PARAMETER() {
	return SURGERIES_PARAMETER;
    }

    public String getSORT_ORDER_PARAMETER() {
	return SORT_ORDER_PARAMETER;
    }

    public String getORDER_BY_PARAMETER() {
	return ORDER_BY_PARAMETER;
    }

    public String getCOUNTY_PARAMETER() {
	return COUNTY_PARAMETER;
    }

    public String getMUNICIPALITY_PARAMETER() {
	return MUNICIPALITY_PARAMETER;
    }

    public String getSUBLOCALITY_PARAMETER() {
        return SUBLOCALITY_PARAMETER;
    }

    public String getEVENING_HOURS_PARAMETER() {
	return EVENING_HOURS_PARAMETER;
    }

    public String getWEEKEND_HOURS_PARAMETER() {
	return WEEKEND_HOURS_PARAMETER;
    }

    public String getSURGERY_CATEGORY_PARAMETER() {
	return SURGERY_CATEGORY_PARAMETER;
    }

    public String getHAVE_PARKING_PARAMETER() {
	return HAVE_PARKING_PARAMETER;
    }

    public String getPUBLIC_TRANSPORT_PARAMETER() {
	return PUBLIC_TRANSPORT_PARAMETER;
    }

    public String getDENTAL_FEAR_PARAMETER() {
	return DENTAL_FEAR_PARAMETER;
    }

    public String getWARRANTIES_PARAMETER() {
	return WARRANTIES_PARAMETER;
    }

    public String getREGION_PARAMETER() {
	return REGION_PARAMETER;
    }

    public String getSPECIAL_PARAMETER() {
	return SPECIAL_PARAMETER;
    }

    public String getTOPLEVEL_COMMENT() {
	return TOPLEVEL_COMMENT;
    }

    public String getSURGERY_VIEW_SPACE() {
	return SURGERY_VIEW_SPACE;
    }

    public String getSURGERY_EDIT_SPACE() {
	return SURGERY_EDIT_SPACE;
    }

    public int getDEFAULT_PACKAGE() {
	return DEFAULT_PACKAGE;
    }

    public String getVERSION_DOCUMENT() {
        return VERSION_DOCUMENT;
    }

    public int getVersion() {
        return versionService.getVersion();
    }
}
