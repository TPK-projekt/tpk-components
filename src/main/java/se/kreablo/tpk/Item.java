/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk;

import java.util.List;

public abstract class Item {

    private Integer code;
    
    private String title;

    private String description;

    private String descriptionMore;

    private List<Infobox> infoboxes;

    public Item(Integer code, String title, String description, String descriptionMore, List<Infobox> infoboxes) {
	this.code = code;
	this.title = title;
	this.description = description;
	this.descriptionMore = descriptionMore;
	this.infoboxes = infoboxes;
    }

    public Integer getCode() {
	return this.code;
    }

    public void setCode(Integer code) {
	this.code = code;
    }
	
    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public String getDescriptionMore() {
        return this.descriptionMore;
    }

    public void setDescriptionMore(String descriptionMore) {
        this.descriptionMore = descriptionMore;
    }

    public List<Infobox> getInfoboxes() {
	return infoboxes;
    }

    public void setInfoboxes(List<Infobox> infoboxes) {
	this.infoboxes = infoboxes;
    }
}
