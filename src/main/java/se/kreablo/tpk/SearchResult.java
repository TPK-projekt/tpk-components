/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk;

import java.util.List;
import java.util.ArrayList;

public class SearchResult {

    private long numFound = 0;

    private final List<Item> items = new ArrayList<Item>();

    public SearchResult() {
    }

    public void addNumFound(long numFound) {
	this.numFound += numFound;
    }

    public long getNumFound() {
	return numFound;
    }

    public List<Item> getItems() {
	return this.items;
    }

    public void addItem(Item item) {
	items.add(item);
    }

    public static class Item {
	private final String id;

	private final String description;

	private final String url;

	public Item(String id, String description, String url) {
	    this.id = id;
	    this.description = description;
	    this.url = url;
	}

	public String getId() {
	    return this.id;
	}

	public String getDescription() {
	    return this.description;
	}

	public String getURL() {
	    return this.url;
	}
    }
}
