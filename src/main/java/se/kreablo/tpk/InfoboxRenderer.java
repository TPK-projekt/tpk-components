/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk;

import org.xwiki.component.annotation.Role;

import java.util.Map;
import java.util.List;
import java.util.SortedSet;

@Role
public interface InfoboxRenderer {

    Object renderInfoboxes(SortedSet<String> infoboxes) throws InfoboxRendererException, RendererException;

    Map<String, List<Infobox>>  renderMAreaInfoboxes(SortedSet<String> mareas) throws InfoboxRendererException, RendererException;

    Map<Integer, Package> renderPackageInfoboxes(SortedSet<Integer> packages) throws InfoboxRendererException, RendererException;

    Map<Integer, Measure> renderMeasureInfoboxes(SortedSet<Integer> measures, Boolean special) throws InfoboxRendererException, RendererException;
    
}
