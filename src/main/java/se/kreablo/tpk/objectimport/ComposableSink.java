/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.objectimport;

/**
 * Supetype for sink composition.
 * @param <T> The type of the sink.
 * @param <S> The type of the downstream sink.
 * @version $Id$
 */
public abstract class ComposableSink<T, S> implements FieldSink<T>
{

    private final FieldSink<S> after;

    /** @param after The downstream sink */
    public ComposableSink(FieldSink<S> after) {
        this.after = after;
    }

    @Override
    public final void startCycle() throws ConduitException {
        start();
        after.startCycle();
    }
                             
    @Override
    public final void endCycle() throws ConduitException {
        end();
        after.endCycle();
    }

    @Override
    public final void cancelCycle() {
        cancel();
        after.cancelCycle();
    }
    
    @Override
    public void yield(T v) throws ConduitException {
        after.yield(f(v));
    }

    /**
     * Function to transform value.
     * @param v The input value.
     * @throws ConduitException on failure.
     */
    protected abstract S f(T v) throws ConduitException;

    /**
     * On start cycle.
     * @throws ConduitException on failure.
     */
    protected void start() throws ConduitException {
    }

    /**
     * On end cycle.
     * @throws ConduitException on failure.
     */
    protected void end() throws ConduitException {
    }

    /**
     * On cancel cycle.
     * @throws ConduitException on failure.
     */
    protected void cancel() {
    }
    
}
