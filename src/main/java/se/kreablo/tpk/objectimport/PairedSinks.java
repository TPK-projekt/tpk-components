/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.objectimport;

import org.apache.commons.lang3.tuple.Pair;

/**
 * A pair of sinks that consumes values from a pair of values.
 * @param <L> Type of left value.
 * @param <R> Type of right value.
 * @version $Id$
 */
public class PairedSinks<L, R> implements FieldSink<Pair<L, R>>
{

    private final FieldSink<L> left;
    
    private final FieldSink<R> right;

    /**
     * @param left The left sink.
     * @param right The right sink.
     */
    public PairedSinks(FieldSink<L> left, FieldSink<R> right) {
        this.left = left;
        this.right = right;
    }

    @Override
    public void yield(Pair<L, R> v) throws ConduitException {
        left.yield(v.getLeft());
        right.yield(v.getRight());
    }

    @Override
    public void startCycle() throws ConduitException {
        left.startCycle();
        right.startCycle();
    }

    @Override
    public void endCycle() throws ConduitException {
        left.endCycle();
        right.endCycle();
    }

    @Override
    public void cancelCycle() {
        left.cancelCycle();
        right.cancelCycle();
    }
}
