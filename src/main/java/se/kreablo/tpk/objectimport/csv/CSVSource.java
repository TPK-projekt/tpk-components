/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.objectimport.csv;

import se.kreablo.tpk.objectimport.CycleItem;
import se.kreablo.tpk.objectimport.FieldSource;
import se.kreablo.tpk.objectimport.CycleNumber;
import se.kreablo.tpk.objectimport.ConduitException;
import se.kreablo.tpk.objectimport.SharedCycleItem;
import se.kreablo.tpk.objectimport.EndConduitException;
import se.kreablo.tpk.objectimport.EndWithErrorConduitException;
import se.kreablo.tpk.objectimport.tpk.NextVersionSource;

import org.apache.commons.csv.CSVRecord;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;

import java.nio.charset.Charset;
import java.io.InputStream;
import java.io.Reader;
import java.io.InputStreamReader;
import java.io.Closeable;
import java.io.IOException;
import java.util.Locale;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.text.NumberFormat;
import javax.inject.Inject;

/**
 * Supertype for producing sources reading CSV data.
 * @version $Id$
 */
public class CSVSource implements CycleItem, Closeable
{

    private final CycleNumber cycleNumber = new CycleNumber();

    private Reader input;

    private Locale locale;

    private Charset charset;

    private CSVFormat format;

    private CSVRecord currentRecord;

    private CSVParser parser;

    private Iterator<CSVRecord> iterator;

    private final Locale seLocale = Locale.forLanguageTag("sv");

    private NumberFormat numberFormat =
        NumberFormat.getInstance(seLocale);

    @Inject
    private NextVersionSource nextVersionSource;
    
    /**
     * Constructor for CSVSource.
     */
    public CSVSource() {
        cycleNumber.addListener(this);
        this.format = CSVFormat.DEFAULT.withHeader();
        this.locale = seLocale;
        this.charset = Charset.defaultCharset();
    }

    /**
     * @param input The CSV document input as input stream.
     * @param charset The charset of the document.
     */
    public void setInputStream(InputStream input, Charset charset) {
        this.input = new InputStreamReader(input, charset);
    }

    /** @param input The CSV document input as input stream.  The platform default charset will be used. */
    public void setInputStream(InputStream input) {
        this.input = new InputStreamReader(input, this.charset);
    }

    /** @param input The CSV document input as a reader.  */
    public void setInput(Reader input) {
        this.input = input;
    }

    /** @param locale THe locale which may affect parsing numbers. */
    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    /** @param charset The charset of the CSV document */
    public void setCharset(Charset charset) {
        this.charset = charset;
    }

    /** @param format The csv format. */
    public void setFormat(CSVFormat format) {
        this.format = format;
    }

    /** @return a source that reads data from a column in a csv document. */
    protected FieldSource<String> getColumnSource(int column) {
        return new ColumnSource(column);
    }

    private CSVParser getCSVParser() throws ConduitException {
        if (parser == null) {
            try {
                parser = CSVParser.parse(input, format);
            } catch (IOException e) {
                throw new ConduitException(e);
            }
        }
        return parser;
    }

    private Iterator<CSVRecord> getIterator() throws ConduitException {
        if (iterator == null) {
            CSVParser p = getCSVParser();
            iterator = p.iterator();
        }
        return iterator;
    }

    /**
     * @return The header labels.
     * @throws ConduitException if the csv source data cannot be read.
     */
    protected List<String> getLabels() throws ConduitException {
        CSVParser p = getCSVParser();
        List<String> labels = new ArrayList<>();
        for (String s : p.getHeaderMap().keySet()) {
            labels.add(s);
        }
        return labels;
    }

    @Override
    public void startCycle() throws ConduitException {

        Iterator<CSVRecord> it = getIterator();

        if (it.hasNext()) {
            currentRecord = it.next();
        } else {
            throw new EndConduitException();
        }
    }

    @Override
    public void close() throws IOException {
        if (parser != null) {
            parser.close();
            parser = null;
        }
    }

    /** @return The number format in use. */
    protected NumberFormat getNumberFormat() {
        return numberFormat;
    }

    /** @return The next version source. */
    protected NextVersionSource getNextVersionSource() {
        return nextVersionSource;
    }

    /** @return The cycle number object */
    protected CycleNumber getCycleNumber() {
        return cycleNumber;
    }
    
    private class ColumnSource extends SharedCycleItem implements FieldSource<String>
    {
        private final int column;

        ColumnSource(int column) {
            super(cycleNumber);
            this.column = column;
        }

        public String await() throws ConduitException {
            try {
                return currentRecord.get(column);
            } catch (IndexOutOfBoundsException e) {
                throw new EndWithErrorConduitException("Expecting at least " + column + " columns in CSV file.", e);
            }
        }
    }
}
