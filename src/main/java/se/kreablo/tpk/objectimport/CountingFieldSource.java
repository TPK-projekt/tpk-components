package se.kreablo.tpk.objectimport;

/**
 * An integer source that change its value with an additive increment
 * on each cycle.
 * @version $Id$
 */
public class CountingFieldSource implements FieldSource<Integer>
{

    private int value;

    private int increment;

    /**
     * @param startValue The value to start at.
     * @param increment The increment value.
     */
    public CountingFieldSource(int startValue, int increment) {
        this.value = startValue;
        this.increment = increment;
    }

    /**
     * A counting field source with increment 1.
     * @param startValue The value to start at.
     */
    public CountingFieldSource(int startValue) {
        this(startValue, 1);
    }
    
    /**
     * A counting field source starting at 1 with increment 1.
     */
    public CountingFieldSource() {
        this(1, 1);
    }
    
    @Override
    public Integer await() {
        return value;
    }

    @Override
    public void endCycle() {
        value += increment;
    }

    @Override
    public void cancelCycle() {
        value += increment;
    }
    
}
