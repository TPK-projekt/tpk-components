/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.objectimport;

/**
 * Source composition.
 * @param <S> type of the upstream source.
 * @param <T> type of the composed source.
 * @version $Id$
 */
public abstract class ComposableSource<S, T> implements FieldSource<T>
{

    private final FieldSource<S> before;

    /** @param before the upstream source */
    public ComposableSource(FieldSource<S> before) {
        this.before = before;
    }

    @Override
    public final T await() throws ConduitException {
        S value = before.await();
        return f(value);
    }

    /**
     * Function to transform value.
     * @param v The input value.
     * @throws ConduitException on failure.
     */
    protected abstract T f(S v) throws ConduitException;

    @Override
    public final void startCycle() throws ConduitException {
        before.startCycle();
        start();
    }
                                                 
    @Override
    public final void endCycle() throws ConduitException {
        before.endCycle();
        end();
    }

    @Override
    public final void cancelCycle() {
        before.cancelCycle();
        cancel();
    }
    
    /**
     * On start cycle.
     * @throws ConduitException on failure.
     */
    protected void start() throws ConduitException {
    }

    /**
     * On end cycle.
     * @throws ConduitException on failure.
     */
    protected void end() throws ConduitException {
    }
    
    /**
     * On cancel cycle.
     * @throws ConduitException on failure.
     */
    protected void cancel() {
    }
    

}
