/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.objectimport.database;

import java.util.Map;
import java.util.HashMap;
import java.util.function.Function;
import java.util.List;
import java.util.ArrayList;
import javax.inject.Inject;

import se.kreablo.tpk.objectimport.CycleItem;
import se.kreablo.tpk.objectimport.FieldSink;
import se.kreablo.tpk.objectimport.CycleNumber;
import se.kreablo.tpk.objectimport.SharedCycleItem;
import se.kreablo.tpk.objectimport.ConduitException;
import se.kreablo.tpk.internal.AbstractTPKService;
import se.kreablo.util.Either;

import org.jooq.Field;
import org.jooq.Table;
import org.jooq.Record;
import org.jooq.DSLContext;
import org.jooq.InsertSetStep;
import org.jooq.InsertSetMoreStep;
import org.jooq.exception.DataAccessException;

/**
 * Terminates a bundle of conduit sinks into database table insertions.
 * @param <T> Table datatype.
 * @param <R> Table record datatype.
 * @version $Id$
 */
public class TableSink<T extends Table, R extends Record>
    extends AbstractTPKService
    implements CycleItem
{

    protected final T table;

    private final CycleNumber cycleNumber = new CycleNumber();

    private int batchSize = 1000;

    private int numRecords;

    private final Map<Field<?>, FieldSink<?>> fieldSinks = new HashMap<>();

    private DSLContext ctx;

    private Either<InsertSetStep<R>,  InsertSetMoreStep<R>> cur;

    private final List<Runnable> delayedInserts = new ArrayList<>();

    private boolean doInsert;


    /**
     * @param table Construct for the given table.
     */
    public TableSink(T table) {
        cycleNumber.addListener(this);
        this.table = table;
    }

    @Override
    public void startCycle() throws ConduitException {
        doInsert = true;
        delayedInserts.clear();
    }

    @Override
    public void endCycle() throws ConduitException {
        if (doInsert) {
            if (cur == null) {
                cur = Either.left(getContext(true).insertInto(table));
                numRecords++;
            }
            cur.accept(
                r -> { },
                m -> {
                    cur = Either.left(m.newRecord());
                    numRecords++;
                });
            getLogger().trace("Running {} delayed inserts", delayedInserts.size());
            for (Runnable r : delayedInserts) {
                r.run();
            }
            if (numRecords >= batchSize) {
                commit();
            }
        }
    }

    private void commit() throws ConduitException {
        getLogger().trace("committing {} records", numRecords);
        try {
            if (cur == null) {
                getLogger().warn("Commit without insertion");
                return;
            }
                
            cur.accept(
                r -> { getLogger().trace("going left"); },
                m -> {
                    try {
                        int n = m.execute();
                        getLogger().trace("Inserting {} records", n);
                        getLogger().trace(m.toString());
                    } finally {
                        m.close();
                    }
                });
        } catch (DataAccessException e) {
            getLogger().error("Exception in commit.", e);
            throw new ConduitException(e);
        } finally {
            cur = null;
            numRecords = 0;
            closeContext();
        }
    }

    @Override
    public void close() {
        try {
            commit();
        } catch (ConduitException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Wrap column insertion to a database field in a conduit sink.
     * @param field The database field which must be taken from the table.
     * @param guardNull If the insert should be blocked on a null value in this column.
     * @param <V> The type of the database field content.
     * @return The sink.
     */
    public <V> FieldSink<V> getColumnSink(Field<V> field, boolean guardNull) {

        checkAlreadyConnected(field);

        ColumnSink<V> columnSink = new ColumnSink<V>(field, guardNull);
        
        fieldSinks.put(field, columnSink);

        return columnSink;
    }

    /**
     * Wrap column insertion to a database field in a conduit sink.
     * @param field The database field which must be taken from the table.
     * @param guardNull If the insert should be blocked on a null value in this column.
     * @param <V> The type of the database field content.
     * @return The sink.
     */
    public <V> FieldSink<V> getColumnSink(Field<V> field) {
        return getColumnSink(field, false);
    }

    private <T> void checkAlreadyConnected(Field<T> field) {
        if (fieldSinks.get(field) != null) {
            throw new RuntimeException("Column '" + field.getName()
                                       + "' in table '" + table.getName()
                                       + "' already connected!");
        }
    }
    
    /**
     * Generate a column insertion sink that can be inserted to
     * eiteher directly (right value) or via a subquery (left value).
     * @param genQuery Function that produce a query from the left value.
     * @param field The field to insert to.
     * @param <L> The type of the value to input to the subquery.
     * @param <R> The type of the ultimate database field.
     * @return The sink.
     */
    public <L, R> FieldSink<Either<L, R>>
        getColumnSink(Function <L, Field<R>> genQuery, Field<R> field) {

        checkAlreadyConnected(field);

        FieldSink<Either<L, R>> sink = new EitherFieldSink<L, R>(genQuery, field);

        fieldSinks.put(field, sink);

        return sink;
    }

    private <V> void set(Field<V> field, V value) {
        cur.accept(
            s -> { cur = Either.right(s.set(field, value)); },
            m -> { cur = Either.right(m.set(field, value)); });
    }

    private <V> void set(Field<V> field, Field<V> valueField) {
        cur.accept(
            s -> { cur = Either.right(s.set(field, valueField)); },
            m -> { cur = Either.right(m.set(field, valueField)); });
    }
    
    private class ColumnSink<V> extends SharedCycleItem
        implements FieldSink<V>
    {

        private final Field<V> field;

        private boolean guardNull;

        ColumnSink(Field<V> field, boolean guardNull) {
            super(cycleNumber);
            this.field = field;
            this.guardNull = guardNull;
        }

        ColumnSink(Field<V> field) {
            this(field, false);
        }

        @Override
        public void yield(V value) {
            getLogger().trace("Yielding value {} for {}", value, field.getName());
            if (guardNull && value == null) {
                getLogger().debug("Guard null blocking.");
                doInsert = false;
            } else {
                delayedInserts.add(() -> { set(field, value); });
            }
        }

    }

    private class EitherFieldSink<L, R> extends SharedCycleItem
        implements FieldSink<Either<L, R>>
    {

        private final Field<R> field;

        private final Function<L, Field<R>> genQuery;

        private boolean guardNull;
        
        EitherFieldSink(Function<L, Field<R>> genQuery, Field<R> field, boolean guardNull) {
            super(cycleNumber);
            this.field = field;
            this.genQuery = genQuery;
            this.guardNull = guardNull;
        }

        EitherFieldSink(Function<L, Field<R>> genQuery, Field<R> field) {
            this(genQuery, field, false);
        }

        @Override
        public void yield(Either<L, R> value) {
            getLogger().trace("Yielding value {} for {} ", value, field.getName());
            if (guardNull && value == null) {
                doInsert = false;
                return;
            }
            value.accept(
                v -> { delayedInserts.add(() -> { set(field, genQuery.apply(v)); }); },
                v -> { delayedInserts.add(() -> { set(field, v); }); });
        }
    }

}
