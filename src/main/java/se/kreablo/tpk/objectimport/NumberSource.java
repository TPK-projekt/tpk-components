/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.objectimport;

import java.text.NumberFormat;
import java.text.ParseException;

/**
 * Composable source for converting a string into a number.
 *
 * {@code null} is produced if the string fails to parse as a number.
 * @version $Id$
 */
public class NumberSource extends ComposableSource<String, Number>
{

    private final NumberFormat numberFormat;

    /**
     * @param source The upstream source.
     * @param numberFormat The number format used for parsing the string.
     */
    public NumberSource(FieldSource<String> source, NumberFormat numberFormat) {
        super(source);
        this.numberFormat = numberFormat;
    }

    @Override
    public Number f(String s) {
        try {
            return numberFormat.parse(s);
        } catch (ParseException e) {
            return null;
        }
    }
}
