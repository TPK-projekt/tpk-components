/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.objectimport;

/**
 * An item that is operated in cycles defined by calls to methods {@code startCycle} and {@code endCycle}.
 * @version $Id$
 */
public interface CycleItem
{

    /**
     * The start of the cycle.
     * @throws ConduitException on failure.
     */
    default void startCycle() throws ConduitException {
    }
                             

    /**
     * The end of the cycle.
     * @throws ConduitException on failure.
     */
    default void endCycle() throws ConduitException {
    }

    /**
     * Cancel cycle.
     */
    default void cancelCycle() {
    }
}
