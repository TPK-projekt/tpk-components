/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.objectimport.object;

import java.util.Map;
import java.util.HashMap;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Provider;

import org.xwiki.model.reference.DocumentReference;
import org.xwiki.model.reference.DocumentReferenceResolver;
import org.xwiki.model.reference.ObjectPropertyReference;
import org.xwiki.model.reference.ObjectReference;
import org.xwiki.model.reference.EntityReferenceSerializer;
import org.xwiki.bridge.DocumentAccessBridge;
import org.xwiki.bridge.DocumentModelBridge;

import com.xpn.xwiki.objects.BaseObjectReference;
import com.xpn.xwiki.doc.XWikiDocument;
import com.xpn.xwiki.XWikiContext;
import com.xpn.xwiki.objects.BaseObject;

import se.kreablo.tpk.objectimport.CycleItem;
import se.kreablo.tpk.objectimport.FieldSink;
import se.kreablo.tpk.objectimport.CycleNumber;
import se.kreablo.tpk.objectimport.SharedCycleItem;
import se.kreablo.tpk.objectimport.ConduitException;

import org.slf4j.Logger;

/**
 * A sink bundle for inserting values into object properties.
 * @version $Id$
 */
public class ObjectSink implements CycleItem
{

    private Map<DocumentReference, DocumentModelBridge> docs = new HashMap<>();

    private final CycleNumber cycleNumber = new CycleNumber();

    @Inject
    @Named("current")
    private DocumentReferenceResolver documentReferenceResolver;

    @Inject
    private Provider<XWikiContext> xcontext;

    @Inject
    @Named("compactwiki")
    private EntityReferenceSerializer<String> entityReferenceSerializer;

    @Inject
    private DocumentAccessBridge documentAccessBridge;

    @Inject
    private Logger logger;

    /** Constructor. */
    public ObjectSink() {
        cycleNumber.addListener(this);
    }

    @Override
    public void endCycle() throws ConduitException {
        for (DocumentModelBridge doc : docs.values()) {
            try {
                logger.trace("Saving document {}", doc.getFullName());
                xcontext.get().getWiki()
                    .saveDocument((XWikiDocument) doc, "Object import", false, xcontext.get());
            } catch (Exception e) {
                logger.error("Exception when saving document.", e);
                throw new ConduitException(e);
            }
       }
        docs.clear();
    }

    /**
     * Get a document which may be cached for the duration of this cycle.
     * @param docRef The document reference.
     * @return the document.
     * @throws ConduitException on failure.
     */
    protected DocumentModelBridge getDoc(DocumentReference docRef) throws ConduitException {
        DocumentModelBridge doc = docs.get(docRef);
        if (doc == null) {
            try {
                doc = documentAccessBridge.getDocument(docRef);
                docs.put(docRef, doc);
            } catch (Exception e) {
                throw new ConduitException(e);
            }
        }

        return doc;
    }

    /** @return the document access bridge. */
    protected DocumentAccessBridge getDocumentAccessBridge() {
        return documentAccessBridge;
    }

    /** @return document referenc resolver. */
    protected DocumentReferenceResolver getDocumentReferenceResolver() {
        return documentReferenceResolver;
    }

    /**
     * Create a sink that inserts values into an object property specified by a naming scheme.
     * @param type the type of the property.
     * @param namingScheme the naming scheme for identifying the property.
     * @return the sink.
     * @param <T> The type of the property.
     */
    public <T> FieldSink<T> getPropertySink(Class<T> type, NamingScheme namingScheme) {
        return new PropertySink<T>(namingScheme);
    }

    private class PropertySink<T> extends SharedCycleItem implements FieldSink<T>
    {

        private final NamingScheme namingScheme;

        private T value;

        PropertySink(NamingScheme namingScheme) {
            super(cycleNumber);
            this.namingScheme = namingScheme;
        }

        @Override
        public void yield(T value) {
            try {
                logger.trace("Yielding {} on {}", value, namingScheme.getReference());
            } catch (Exception e) {
            }
            this.value = value;
        }

        @Override
        protected void end() throws ConduitException {
            ObjectPropertyReference ref = namingScheme.getReference();
            BaseObjectReference oref = new BaseObjectReference(ref.getParent());
            DocumentReference dref = oref.getDocumentReference();

            try {
                final DocumentModelBridge mdoc = getDoc(dref);

                XWikiDocument xdoc = (XWikiDocument) mdoc;

                BaseObject xobj = xdoc.getXObject(
                    oref.getXClassReference(), oref.getObjectNumber(), true, xcontext.get());
                xobj.set(ref.getName(), this.value, xcontext.get());
            } catch (Exception e) {
                throw new ConduitException(e);
            }
        }
    }

    /**
     * Create an object property reference.
     * @param docRef The document containing the object.
     * @param classRef The class ofthe object.
     * @param objectnr The object number within the document.
     * @param fieldName The property name.
     * @return The object property reference.
     */
    protected ObjectPropertyReference getPropertyReference(
        DocumentReference docRef, DocumentReference classRef, int objectnr, String fieldName) {
        ObjectReference objRef =
            new ObjectReference(entityReferenceSerializer.serialize(classRef) + "[" + objectnr + "]", docRef);

        return new ObjectPropertyReference(fieldName, objRef);
    }

    /** @return xwiki context. */
    protected XWikiContext getXContext() {
        return xcontext.get();
    }

    /** @return the logger. */
    protected Logger getLogger() {
        return logger;
    }

    /** @return the XWiki object id of the corresponding object */
    protected Long getObjectId(ObjectPropertyReference propRef) throws ConduitException {
        try {
            final BaseObjectReference oref = new BaseObjectReference(propRef.getParent());
            final DocumentReference dref = oref.getDocumentReference();
            final DocumentModelBridge mdoc = getDoc(dref);
            final XWikiDocument xdoc = (XWikiDocument) mdoc;
            final BaseObject xobj = xdoc.getXObject(
                oref.getXClassReference(), oref.getObjectNumber(), true, xcontext.get());
            return xobj.getId();
        } catch (Exception e) {
            logger.error("Exception when fetching object id.", e);
            throw new ConduitException(e);
        }
    }

    /**
     * A naming scheme is used for globally identifying a specific property.
     */
    public interface NamingScheme
    {

        /** @return the object property reference. */
        ObjectPropertyReference getReference();
    }

}
