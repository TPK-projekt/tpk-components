/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.objectimport;

/**
 * A cycle item that is depending on a cycle number that may be shared
 * with other cycle items.
 * @version $Id$
 */
public class SharedCycleItem implements CycleItem
{

    private final CycleNumber cycleNumber;

    private int n = -1;

    /** @param cycleNumber the shared cycle number instance. */
    protected SharedCycleItem(CycleNumber cycleNumber) {
        this.cycleNumber = cycleNumber;
        cycleNumber.incNumDependent();
    }

    @Override
    public final void startCycle() throws ConduitException {
        cycleNumber.startCycle();
        int n0 = cycleNumber.getCycleNumber();
        n = n0;
        start();
    }
                             
    @Override
    public final void endCycle() throws ConduitException {
        if (cycleNumber.getCycleNumber() != n) {
            throw new InvalidStateException(this.getClass().getName()
                                            + " cyclenumber: " + cycleNumber.getCycleNumber()
                                            + " n: " + n);
        }
        end();
        cycleNumber.report();
    }

    @Override
    public final void cancelCycle() {
        cancel();
        cycleNumber.reportCancel();
    }

    /**
     * On cycle start.
     * @throws Conduit exception on failure.
     */
    protected void start() throws ConduitException {
    }

    /**
     * On cycle end.
     * @throws Conduit exception on failure.
     */
    protected void end() throws ConduitException {
    }


    /**
     * On cycle cancel.
     */
    protected void cancel() {
    }
}
