/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.objectimport;

import java.util.List;
import java.util.ArrayList;

/**
 * Counter for cycle number.  Dependent cycle items must report in
 * their respective endCycle.  endCycle will be called on listeners
 * when all dependent cycle items have reported.
 * @version $Id$
 */
public class CycleNumber implements CycleItem
{

    private int n;

    private int reported;

    private int numDependent;

    private boolean started;

    private List<CycleItem> listeners = new ArrayList<>();

    @Override
    public void startCycle() throws ConduitException {
        if (!started) {
            started = true;
            for (CycleItem i : listeners) {
                i.startCycle();
            }
        }
    }

    @Override
    public void endCycle() throws ConduitException {
        reported = 0;
        for (CycleItem i : listeners) {
            i.endCycle();
        }
        n++;
        started = false;
    }

    @Override
    public void cancelCycle() {
        reported = 0;
        for (CycleItem i : listeners) {
            i.cancelCycle();
        }
        n++;
        started = false;
    }
    
    /** @return the current cycle number */
    public int getCycleNumber() {
        return n;
    }

    /**
     * Increase the number of dependent items.
     */
    public void incNumDependent() {
        numDependent++;
    }

    /**
     * Dependent items must call report in their endCycle method and reportCancel in ther cancelCycle method.
     * @throws ConduitException on failure.
     */
    public void report() throws ConduitException {
        reported++;
        if (reported == numDependent) {
            endCycle();
        }
    }


    /**
     * Dependent items must call report in their endCycle method and reportCancel in ther cancelCycle method.
     */
    public void reportCancel() {
        reported++;
        if (reported == numDependent) {
            cancelCycle();
        }
    }

    /**
     * Add a listener that will be driven by this cycle number.
     * @param item The listener.
     */
    public void addListener(CycleItem item) {
        listeners.add(item);
    }

}
    
