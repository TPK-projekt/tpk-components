/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.objectimport;

import java.util.List;
import java.util.ArrayList;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;

/**
 * A record conduit bundles a set of FieldConduits and runs them toghether.
 * @version $Id$
 */
public class RecordConduit implements CycleItem
{

    private final FieldConduit<?> [] fieldConduits;

    private final int exceptionLimit = 100;


    /**
     * @param fieldConduits The list of conduits.
     */
    public RecordConduit(FieldConduit<?>... fieldConduits) {
        this.fieldConduits = fieldConduits;
    }
    
    /**
     * @param fieldConduits The list of conduits.
     */
    public RecordConduit(List<FieldConduit<?>> fieldConduits) {
        this(fieldConduits.toArray(new FieldConduit[0]));
    }

    /**
     * Create FieldConduits to connect the sources and sinks. 
     * @param sources the sources
     * @param sinks the sinks
     */
    public RecordConduit(List<FieldSource<?>> sources, List<FieldSink<?>> sinks) {
        List<FieldConduit<?>> fcs = new ArrayList<>();

        if (sources.size() != sinks.size()) {
            throw new IllegalArgumentException("Different number of sinks and sources "
                                               + sources.size() + " != " + sinks.size());
        }

        for (int i = 0; i < sources.size(); i++) {
            fcs.add(new FieldConduit(sources.get(i), sinks.get(i)));
        }

        this.fieldConduits = fcs.toArray(new FieldConduit[0]);
    }

    /**
     * Create FieldConduits to connect the sources and sinks. 
     * @param sources the sources
     * @param sinks the sinks
     */
    public RecordConduit(FieldSource<?> [] sources, FieldSink<?> [] sinks) {
        this(asList(sources), asList(sinks));
    }

    /**
     * Create a singleton FieldConduit to connect one source with one sink.
     * @param source The source
     * @param sink The sink
     */
    public <T> RecordConduit(FieldSource<T> source, FieldSink<T> sink) {
        this(singletonList(source), singletonList(sink));
    }
    

    @Override
    public void startCycle() throws ConduitException {
        for (FieldConduit<?> fc : fieldConduits) {
            fc.startCycle();
        }
    }

    private void transferValues() throws ConduitException {
        for (FieldConduit<?> fc : fieldConduits) {
            fc.transferValue();
        }
    }

    @Override
    public void endCycle() throws ConduitException {
        for (FieldConduit<?> fc : fieldConduits) {
            fc.endCycle();
        }
    }

    @Override
    public void cancelCycle() {
        for (FieldConduit<?> fc : fieldConduits) {
            fc.cancelCycle();
        }
    }

    /**
     * Run the conduits in the record.
     *
     * The execution tolerates that some cycles fail and produce an exception up to a limit.
     * 
     * @return a list of exceptions.
     */
    public List<Exception> run() {
        ArrayList<Exception> exceptions = new ArrayList<>();
        while (true) {
            if (exceptions.size() > exceptionLimit) {
                throw new RuntimeException("Too many exceptions.", exceptions.get(exceptions.size() - 1));
            }
            try {
                tick();
            } catch (EndWithErrorConduitException e) {
                exceptions.add(e);
                break;
            } catch (EndConduitException e) {
                break;
            } catch (StopCycleException e) {
                cancelCycle();
            } catch (Exception e) {
                cancelCycle();
                exceptions.add(e);
            }
        }
        return exceptions;
    }

    /**
     * Advance the items in the record with one cycle.
     * @throws ConduitException on failure.
     */
    public void tick() throws ConduitException {
        startCycle();
        transferValues();
        endCycle();
    }
}
