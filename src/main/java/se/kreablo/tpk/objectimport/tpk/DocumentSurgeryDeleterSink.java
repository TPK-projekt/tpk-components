/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.objectimport.tpk;

import static org.xwiki.component.descriptor.ComponentInstantiationStrategy.PER_LOOKUP;
import org.xwiki.component.annotation.InstantiationStrategy;
import org.xwiki.component.annotation.Component;
import org.xwiki.model.reference.DocumentReference;
import org.xwiki.model.reference.SpaceReference;
import org.xwiki.component.phase.Initializable;

import se.kreablo.tpk.objectimport.object.ObjectSink;
import se.kreablo.tpk.objectimport.FieldSink;
import se.kreablo.tpk.objectimport.ConduitException;

import com.xpn.xwiki.doc.XWikiDocument;

import static se.kreablo.tpk.Constants.SURGERY_VIEW_SPACE;
import static se.kreablo.tpk.Constants.SURGERY_EDIT_SPACE;

import javax.inject.Named;

import java.util.Set;
import java.util.HashSet;

@Component
@Named("document")
@InstantiationStrategy(PER_LOOKUP)
public class DocumentSurgeryDeleterSink extends ObjectSink implements SurgeryDeleterSink, Initializable {

    private static final String DEFAULT_PAGE = ".WebHome";

    private SpaceReference surgeryRoSpace;
    
    private SpaceReference surgeryEditSpace;

    @Override
    public void initialize() {
        surgeryRoSpace = getDocumentReferenceResolver()
            .resolve(SURGERY_VIEW_SPACE + DEFAULT_PAGE).getLastSpaceReference();
        surgeryEditSpace = getDocumentReferenceResolver()
            .resolve(SURGERY_EDIT_SPACE + DEFAULT_PAGE).getLastSpaceReference();
    }
    
    @Override
    public FieldSink<Integer> getIdSink() {
        return new FieldSink<Integer>() {
            @Override
            public void yield(Integer id) throws ConduitException {
                Set<DocumentReference> toDelete = new HashSet<>();
                
                final SpaceReference editSpaceRef = new SpaceReference(id.toString(), surgeryEditSpace);
                final SpaceReference roSpaceRef = new SpaceReference(id.toString(), surgeryRoSpace);

                DocumentReference editRef = new DocumentReference("WebHome", editSpaceRef);
                delete(editRef);
                DocumentReference roRef = new DocumentReference("WebHome", roSpaceRef);
                delete(roRef);
            }
        };
    }

    private void delete(DocumentReference docRef) throws ConduitException {
        try {
            XWikiDocument doc = (XWikiDocument) getDocumentAccessBridge().getDocumentInstance(docRef);

            for (DocumentReference childRef : doc.getChildrenReferences(getXContext())) {
                delete(childRef);
            }
            getLogger().trace("Deleting document {}", docRef);
            getXContext().getWiki().deleteAllDocuments(doc, getXContext());
        } catch (Exception e) {
            getLogger().error("Exception when deleting document", e);
            throw new ConduitException("Exception when deleting document", e);
        }
    }

            
}
