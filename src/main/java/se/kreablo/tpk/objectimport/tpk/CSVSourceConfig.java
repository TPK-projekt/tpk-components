package se.kreablo.tpk.objectimport.tpk;

import java.io.Reader;

/**
 * Configuration for CSV sources.
 * @version $Id$
 */
public class CSVSourceConfig
{

    private final Reader input;

    /** @param input The CSV document as a Reader. */
    public CSVSourceConfig(Reader input) {
        this.input = input;
    }

    /** @return The CSV documen as a Reader. */
    public Reader getInput() {
        return input;
    }

}
