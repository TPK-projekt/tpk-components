/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.objectimport.tpk;

import org.xwiki.component.annotation.Component;
import org.xwiki.component.phase.Initializable;
import org.xwiki.bridge.DocumentAccessBridge;
import org.xwiki.model.reference.DocumentReference;
import org.xwiki.model.reference.DocumentReferenceResolver;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import static se.kreablo.tpk.Constants.VERSION_DOCUMENT;
import static se.kreablo.tpk.Constants.VERSION_CLASS;

/**
 * Source for producing the next version number.
 * @version $Id$
 */
@Component
@Singleton
public class DocumentNextVersionSource implements NextVersionSource, Initializable
{

    private DocumentReference versionDocRef;

    private DocumentReference versionClassRef;

    @Inject
    @Named("current")
    private DocumentReferenceResolver documentReferenceResolver;

    @Inject
    private DocumentAccessBridge documentAccessBridge;
    
    @Override
    public void initialize() {
        versionDocRef = documentReferenceResolver.resolve(VERSION_DOCUMENT);
        versionClassRef = documentReferenceResolver.resolve(VERSION_CLASS);
    }

    /** @return The next version number. */
    protected int getNextVersion() {
        return (Integer) documentAccessBridge.getProperty(versionDocRef, versionClassRef, "version") + 1;
    }

    @Override
    public Integer await() {
        return getNextVersion();
    }
        
}

