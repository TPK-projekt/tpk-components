/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.objectimport.tpk;

import static org.xwiki.component.descriptor.ComponentInstantiationStrategy.PER_LOOKUP;
import org.xwiki.component.annotation.InstantiationStrategy;
import org.xwiki.component.annotation.Component;

import se.kreablo.tpk.objectimport.FieldSource;
import se.kreablo.tpk.objectimport.FieldSink;
import se.kreablo.tpk.objectimport.FieldConduit;
import se.kreablo.tpk.objectimport.ConduitException;
import se.kreablo.tpk.objectimport.StopCycleException;
import se.kreablo.tpk.objectimport.EndConduitException;
import se.kreablo.tpk.objectimport.RecordConduit;

import org.apache.commons.lang3.tuple.Pair;
import se.kreablo.util.Either;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.function.Consumer;
import javax.inject.Named;
import javax.inject.Inject;
import org.slf4j.Logger;

/**
 * Conduit bundle for transfering price data from a CSV document to a database table.
 * @version $Id$
 */
@Component
@Named("csvtotable")
@InstantiationStrategy(PER_LOOKUP)
public class CSVToTablePriceConduit implements PriceConduit<CSVSourceConfig>
{

    @Inject
    @Named("csv")
    private PriceSource<CSVSourceConfig> priceSource;

    @Inject
    @Named("table")
    private PriceSink priceSink;

    @Inject
    private Logger logger;

    @Override
    public Map<String, Object> run(CSVSourceConfig config, boolean special, Consumer<Object> registerCloseable)
        throws ConduitException {

        registerCloseable.accept(priceSource);
        registerCloseable.accept(priceSink);

        List<Pair<FieldSource<Either<String, Pair<Boolean, Integer>>>, FieldSource<Integer>>> priceSources;

        priceSource.start(config);
        priceSources = priceSource.getLabeledPriceSources(false);

        final List<FieldSource<?>> allSources = new ArrayList<>();

        final FieldSource<Integer> versionSource = priceSource.getVersionSource();

        allSources.add(versionSource);

        final List<RecordConduit> conduits = new ArrayList<>(priceSources.size());

        final FieldSink<Either<String, Pair<Boolean, Integer>>> itemIdSink = priceSink.getItemIdSink();

        final FieldSink<Integer> versionSink = priceSink.getVersionSink();

        final FieldSink<Integer> pSink = priceSink.getPriceSink();

        final FieldSink<Integer> surgeryIdSink = priceSink.getSurgeryIdSink();

        final FieldSource<Integer> surgeryIdSource = priceSource.getSurgeryIdSource();

        allSources.add(surgeryIdSource);
        
        for (Pair<FieldSource<Either<String, Pair<Boolean, Integer>>>, FieldSource<Integer>> ps : priceSources) {

            final FieldConduit<Integer> surgeryIdConduit =
                new FieldConduit<>(false, surgeryIdSource, true, surgeryIdSink);

            final FieldConduit<Integer> versionConduit =
                new FieldConduit<>(false, versionSource, true, versionSink);

            final FieldConduit<Either<String, Pair<Boolean, Integer>>> itemIdConduit =
                new FieldConduit<>(false, ps.getLeft(), true, itemIdSink);

            allSources.add(ps.getLeft());
            allSources.add(ps.getRight());

            final FieldConduit<Integer> priceConduit = new FieldConduit<>(false, ps.getRight(), true, pSink);

            conduits.add(new RecordConduit(surgeryIdConduit, versionConduit, itemIdConduit, priceConduit));
        }

        return doIt(allSources, conduits);

    }

    private Map<String, Object> doIt(List<FieldSource<?>> allSources, List<RecordConduit> conduits)
        throws ConduitException {
        Map<String, Object> ret = new HashMap<>();
        
        final List<Exception> exceptions = new ArrayList<>();

        final int exceptionLimit = 100;

        while (true) {
            if (exceptions.size() > exceptionLimit) {
                throw new RuntimeException("Too many exceptions.", exceptions.get(exceptions.size() - 1));
            }
            try {
                for (FieldSource<?> s : allSources) {
                    s.startCycle();
                }
                for (RecordConduit r : conduits) {
                    r.tick();
                }
                for (FieldSource<?> s : allSources) {
                    s.endCycle();
                }
            } catch (StopCycleException e) {
            } catch (EndConduitException e) {
                break;
            } catch (Exception e) {
                logger.trace("Caught exception", e);
                exceptions.add(e);
                for (RecordConduit r : conduits) {
                    r.cancelCycle();
                }
                for (FieldSource<?> s : allSources) {
                    s.cancelCycle();
                }
            }
        }

        if (exceptions.size() > 0) {
            List<String> msgs = new ArrayList<>();
            for (Exception e : exceptions) {
                msgs.add(e.toString());
            }
            ret.put("exceptions", msgs);
        }

        return ret;
    }
}
