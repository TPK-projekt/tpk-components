/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.objectimport.tpk;

import org.xwiki.component.annotation.Role;
import se.kreablo.tpk.objectimport.FieldSource;
import se.kreablo.tpk.objectimport.ConduitException;
import se.kreablo.tpk.objectimport.CycleItem;

import org.apache.commons.lang3.tuple.Pair;
import java.util.List;
import se.kreablo.util.Either;

/**
 * Bundle of sources for price data.
 * @param <C> Configuration type.
 * @version $Id$
 */
@Role
public interface PriceSource<C> extends CycleItem
{

    /** @return source for surgery id field. */
    FieldSource<Integer> getSurgeryIdSource();

    /** @return source for version field. */
    FieldSource<Integer> getVersionSource();

    /**
     * @param isSpecial If measure prices are special.
     * @return sink for item id field.  The item can either be
     * a packaged identified by a string, or a measure identified by
     * an integer and a flag indiciting if it is a special item.
     * @throws ConduitException on failure.
     */
    List<Pair<FieldSource<Either<String, Pair<Boolean, Integer>>>,
        FieldSource<Integer>>> getLabeledPriceSources(boolean isSpecial) throws ConduitException;
    
    /**
     * Configure sources.
     * @param config The configuration.
     * @throws ConduitException on failure.
     */
    void start(C config) throws ConduitException;

}
