/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.objectimport.tpk;

/**
 * The enumeration of supported item price types.
 * @version $Id$
 */
public enum ItemPriceType
{

    /** Price type for packages. */
    PACKAGE(0),
    /** Price type for measures. */
    MEASURE(1),
    /** Price type for special measures. */
    MEASURE_SPECIAL(2);

    private final int code;

    /** @param code The integer code for the type. */
    ItemPriceType(int code) {
        this.code = code;
    }

    /** @return The integer code for the type. */
    public int getCode() {
        return this.code;
    }
}
