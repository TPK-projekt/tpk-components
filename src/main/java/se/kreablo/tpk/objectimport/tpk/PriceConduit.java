/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.objectimport.tpk;

import org.xwiki.component.annotation.Role;
import java.util.Map;
import se.kreablo.tpk.objectimport.ConduitException;
import java.util.function.Consumer;

/**
 * Setup and run a bundle of conduits for price data.
 * @param <C> The configuration data type.
 * @version $Id$
 */
@Role
public interface PriceConduit<C>
{

    /**
     * @param config The configuration.
     * @param special If measure prices are special.
     * @param registerCloseable Consumer to register closeable objects.
     * @return The resulting object to serialize as JSON.
     * @throws ConduitException on failure.
     */
    Map<String, Object> run(C config, boolean special, Consumer<Object> registerCloseable) throws ConduitException;
    
}
