/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.objectimport.tpk;

import static org.xwiki.component.descriptor.ComponentInstantiationStrategy.PER_LOOKUP;
import org.xwiki.component.annotation.InstantiationStrategy;
import org.xwiki.component.annotation.Component;

import se.kreablo.tpk.internal.AbstractTPKService;
import se.kreablo.tpk.internal.AbstractTPKService;
import static se.kreablo.tpk.schema.Tables.*;

import javax.inject.Named;

import org.jooq.exception.DataAccessException;
import org.jooq.SelectWhereStep;
import org.jooq.Record2;
import org.jooq.Query;
import org.jooq.DSLContext;
import org.jooq.impl.DSL;

/**
 * Insert areas into the package area table for next version.
 * @version $Id$
 */
@Component
@Named("table")
@InstantiationStrategy(PER_LOOKUP)
public class TablePackageAreaFiller extends AbstractTPKService implements PackageAreaFiller {

    @Override
    public void fill() throws PackageAreaFillerException {

        SelectWhereStep<Record2<Long, String>> selectAreas = getContext(true)
            .select(DSL.field("t.xwo_id", Long.class), TVPRIS_ITEM.TVPI_AREA)
            .from(
                "(SELECT xwo_id, CAST(unnest(regexp_matches(tvpp_measures, '[[:<:]][[:digit:]]{3}[[:>:]]', 'g')) AS INT) AS measure, tvpi_version FROM tvpris_package JOIN tvpris_item USING (xwo_id)) AS t")
            .join(TVPRIS_ITEM).on(DSL.condition(TVPRIS_ITEM.IS_MEASURE).and(DSL.field("t.measure", Integer.class).equal(TVPRIS_ITEM.TVPI_CODE)
                                                             .and(DSL.field("t.tvpi_version", Integer.class).equal(TVPRIS_ITEM.TVPI_VERSION))));


        final Query insert = getContext().insertInto(TPK_PACKAGE_AREAS, TPK_PACKAGE_AREAS.XWO_ID, TPK_PACKAGE_AREAS.TPA_AREA)
            .select(selectAreas
                    .where(versionQuery(create()).asField().add(1)
                           .eq(DSL.field("t.tvpi_version", Integer.class))))
            .onConflictDoNothing();
        try {
            insert.execute();
        } catch (DataAccessException e) {
            throw new PackageAreaFillerException(e);
        } finally {
            insert.close();
            closeContext();
        }
        
    }
}

