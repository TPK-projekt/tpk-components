/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.objectimport.tpk;

import org.xwiki.component.annotation.Role;
import se.kreablo.tpk.objectimport.FieldSink;
import se.kreablo.tpk.objectimport.CycleItem;

/**
 * Sink bundle for package objects.
 * @version $Id$
 */
@Role
public interface PackageSink extends CycleItem
{

    /** @return sink for code field. */
    FieldSink<Integer> getCodeSink();

    /** @return sink for version field. */
    FieldSink<Integer> getVersionSink();

    /** @return sink for text code field. */
    FieldSink<String> getTextCodeSink();

    /** @return sink for title field. */
    FieldSink<String> getTitleSink();

    /** @return sink for area field. */
    FieldSink<String> getAreaSink();

    /** @return sink for description field */
    FieldSink<String> getDescriptionSink();

    /** @return sink for description_more field */
    FieldSink<String> getDescriptionMoreSink();

    /** @return sink for measures field */
    FieldSink<String> getMeasuresSink();

    /** @return sink for price field */
    FieldSink<Integer> getPriceSink();
    
}
