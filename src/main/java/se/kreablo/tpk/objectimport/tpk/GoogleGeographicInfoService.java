/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.objectimport.tpk;

import static org.xwiki.component.descriptor.ComponentInstantiationStrategy.PER_LOOKUP;
import org.xwiki.component.annotation.InstantiationStrategy;
import org.xwiki.component.annotation.Component;
import se.kreablo.tpk.script.ConfigurationService;
import org.xwiki.script.service.ScriptService;

import javax.inject.Named;
import javax.inject.Inject;

import org.json.JSONObject;
import org.json.JSONArray;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.HttpEntity;
import java.net.URI;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.List;
import java.util.ArrayList;
import java.util.function.Predicate;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.net.URISyntaxException;
import org.slf4j.Logger;

/**
 * Read version number from database and manipulate version number.
 * @version $Id$
 */
@Component
@Named("google")
@InstantiationStrategy(PER_LOOKUP)
public class GoogleGeographicInfoService implements GeographicInfoService
{

    @Inject
    @Named("tpkConfig")
    private ScriptService tpkConfig;

    @Inject
    private Logger logger;

    private final Pattern postalNumberPattern =
        Pattern.compile("^\\s*[1-9](?:(?:\\s*)[0-9]){4}\\s*$", Pattern.MULTILINE);


    @Override
    public GeographicInfo getFromAddress(String address, String postalCode, String city) {

        try {
            return getCoordinates(address, postalCode, city, new ArrayList<String>(), true);
        } catch (URISyntaxException e) {
            logger.error("Invalid syntax in uri for geoservice", e);
            throw new RuntimeException(e);
        } catch (IOException e) {
            logger.error("IO exception when resolving geographic information.", e);
            throw new RuntimeException(e);
        }
    }

    private GeographicInfo getCoordinates(String address, String postalCode, String city, List<String> warnings, boolean usePostalCode) throws URISyntaxException, IOException {

        final String pnr = postalCode.replaceAll("\\s", "");
        final String components = "country:SE" + (pnr == null || !usePostalCode ? "" : "|postal_code:" + pnr);

        URI uri = new URIBuilder()
            .setScheme("https")
            .setHost("maps.googleapis.com")
            .setPath("/maps/api/geocode/json")
            .addParameter("address", address + "," + city)
            .addParameter("components", components)
            .addParameter("key", getConfig().getGoogleMapsKey())
            .build();

        final String json = getHttpContents(uri, warnings);

        final JSONObject jsonObj = new JSONObject(json);

        JSONArray results = jsonObj.getJSONArray("results");

        if (results.length() > 0) {
            if (results.length() > 1) {
                warnings.add("There where more than one location matching address '" + address + "," + city  + "'");
                logger.warn("There where more than one location matching address '{},{}'", address, city);
            }
            return geocode(results, warnings);
        }
        if (usePostalCode && pnr != null)  {
            /* Fallback to ignoring postall number */
            return getCoordinates(address, postalCode, city, warnings, false);
        }
        warnings.add("No location matched address '" + address + "," + city + "'");
        logger.warn("No location matched address '{},{}'", address, city);
        return null;
    }

    private GeographicInfo geocode(JSONArray results, List<String> warnings) throws URISyntaxException, IOException {
        JSONObject result0 = results.getJSONObject(0);
        if (result0 == null) {
            return null;
        }

        JSONObject location = result0.getJSONObject("geometry").getJSONObject("location");

        final GeographicInfo ret = new GeographicInfo();
        ret.setLatitude(location.getDouble("lat"));
        ret.setLongitude(location.getDouble("lng"));
        URI uri1 = new URIBuilder()
            .setScheme("https")
            .setHost("maps.googleapis.com")
            .setPath("/maps/api/geocode/json")
            .addParameter("latlng", "" + ret.getLatitude() + "," + ret.getLongitude())
            .addParameter("language", "sv")
            .addParameter("key", getConfig().getGoogleMapsKey())
            .build();

        String json1 = getHttpContents(uri1, warnings);

        JSONObject jsonObj1 = new JSONObject(json1);
        JSONArray results1 = jsonObj1.getJSONArray("results");

        for (int j = 0; j < results1.length(); j++) {
            JSONObject result1 = results1.getJSONObject(j);
            JSONArray addressComponents = result1.getJSONArray("address_components");
            for (int i = 0; i < addressComponents.length(); i++) {
                final JSONObject ac = addressComponents.getJSONObject(i);
                switch (addressComponentType(ac)) {
                case OTHER:
                    break;
                case COUNTY:
                    ret.setCounty(ac.getString("long_name"));
                    break;
                case MUNICIPALITY:
                    ret.setMunicipality(ac.getString("long_name"));
                    if (ret.getMunicipality() == "Malung" || ret.getMunicipality() == "Sälen") {
                        ret.setMunicipality("Malung-Sälen");
                    }
                    break;
                case SUBLOCALITY:
                    if (ret.getSublocality() != null) {
                        break;
                    }
                case NEIGHBORHOOD:
                    ret.setSublocality(ac.getString("long_name"));
                    break;
                default:
                    break;
                }
            }
        }
        return ret;
    }

    private String getHttpContents(URI uri, List<String> warnings) throws IOException {
        CloseableHttpClient httpclient = HttpClients.createDefault();

        HttpGet httpget = new HttpGet(uri);
        CloseableHttpResponse response = httpclient.execute(httpget);

        String content = "";

        try {
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                InputStream instream = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(instream));
                try {
                    String line = reader.readLine();
                    while (line != null) {
                        content += line;
                        line = reader.readLine();
                    }
                } finally {
                    reader.close();
                }
            }
        } catch (IOException e) {
            logger.warn("Caught exception when making http request to {}", uri,  e);
            warnings.add("Caught exception when making http request to " + uri + ": " +  e.getMessage());
            return null;
        } finally {
            response.close();
        }
        return content;
    }

    private AddressComponentType addressComponentType(JSONObject ac) {
        JSONArray types = ac.getJSONArray("types");
        for (int i = 0; i < types.length(); i++) {
            final String type = types.getString(i);
            final AddressComponentType t = AddressComponentType.fromString(type);
            if (t != AddressComponentType.OTHER) {
                return t;
            }
        }
        return AddressComponentType.OTHER;
    }

    private enum AddressComponentType {
        COUNTY       (type -> { return "administrative_area_level_1".equals(type); } ),
        MUNICIPALITY (type -> { return "administrative_area_level_2".equals(type); } ),
        NEIGHBORHOOD (type -> { return "neighborhood".equals(type); } ),
        SUBLOCALITY  (type -> { return "sublocality_level_1".equals(type); } ),
        OTHER        (type -> { return true ; } );

        private final Predicate<String> predicate;

        AddressComponentType(Predicate<String> p) {
            this.predicate = p;
        }

        boolean test(String s) {
            return predicate.test(s);
        }

        static AddressComponentType fromString(String s) {
            AddressComponentType [] candidates = { COUNTY, MUNICIPALITY, NEIGHBORHOOD, SUBLOCALITY, OTHER };
            for (AddressComponentType c : candidates) {
                if (c.test(s)) {
                    return c;
                }
            }
            return OTHER;
        }
    }

    private String extractPostalNumber(String address)
    {
        Matcher m = postalNumberPattern.matcher(address);
        if (m.find()) {
            String pnr = m.group();
            return pnr.replaceAll("\\s", "");
        }
        return null;
    }

    private String cleanAddress(String address)
    {
        Matcher m = postalNumberPattern.matcher(address);
        final String a0 = m.replaceFirst("");
        return a0.replaceAll("[\\n\\r]+", " ");
    }

    private ConfigurationService getConfig() {
        return (ConfigurationService) tpkConfig;
    }

}

