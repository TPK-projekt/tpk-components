/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.objectimport.tpk;

import static org.xwiki.component.descriptor.ComponentInstantiationStrategy.PER_LOOKUP;
import org.xwiki.component.annotation.InstantiationStrategy;
import org.xwiki.component.annotation.Component;
import org.xwiki.model.reference.ObjectPropertyReference;
import org.xwiki.model.reference.DocumentReference;
import org.xwiki.model.reference.SpaceReference;
import org.xwiki.component.phase.Initializable;

import org.apache.commons.lang3.tuple.Pair;

import se.kreablo.tpk.objectimport.object.ObjectSink;
import se.kreablo.tpk.objectimport.FieldSink;
import se.kreablo.tpk.objectimport.ComposableSink;
import se.kreablo.tpk.objectimport.PairedSinks;
import se.kreablo.tpk.objectimport.ConduitException;

import static se.kreablo.tpk.Constants.SURGERY_CLASS;
import static se.kreablo.tpk.Constants.SURGERY_EDITABLE_CLASS;
import static se.kreablo.tpk.Constants.SURGERY_VIEW_SPACE;
import static se.kreablo.tpk.Constants.SURGERY_EDIT_SPACE;


import javax.inject.Named;

/**
 * Sink for creating surgery objects and inserting surgery data.
 * @version $Id$
 */
@Component
@Named("object")
@InstantiationStrategy(PER_LOOKUP)
public class ObjectSurgerySink extends ObjectSink implements SurgerySink, Initializable
{

    private static final String DEFAULT_PAGE = ".WebHome";

    private Integer currentId;

    private DocumentReference surgeryRoClass;

    private DocumentReference surgeryEditClass;

    private SpaceReference surgeryRoSpace;

    private SpaceReference surgeryEditSpace;

    @Override
    public void initialize() {
        surgeryRoClass = getDocumentReferenceResolver().resolve(SURGERY_CLASS);
        surgeryEditClass = getDocumentReferenceResolver().resolve(SURGERY_EDITABLE_CLASS);
        surgeryRoSpace = getDocumentReferenceResolver()
            .resolve(SURGERY_VIEW_SPACE + DEFAULT_PAGE).getLastSpaceReference();
        surgeryEditSpace = getDocumentReferenceResolver()
            .resolve(SURGERY_EDIT_SPACE + DEFAULT_PAGE).getLastSpaceReference();
    }

    @Override
    public FieldSink<String> getNameSink() {
        return getPropertySink(String.class, new SurgeryNamingScheme("name", false));
    }

    @Override
    public FieldSink<String> getCompanySink() {
        return getPropertySink(String.class, new SurgeryNamingScheme("company", false));
    }
    
    @Override
    public FieldSink<Double> getLatitudeSink() {
        return getPropertySink(Double.class, new SurgeryNamingScheme("latitude", true));
    }

    @Override
    public FieldSink<Double> getLongitudeSink() {
        return getPropertySink(Double.class, new SurgeryNamingScheme("longitude", true));
    }

    @Override
    public FieldSink<String> getAddressSink() {
        return getPropertySink(String.class, new SurgeryNamingScheme("address", true));
    }

    @Override
    public FieldSink<String> getPostalCodeSink() {
        return getPropertySink(String.class, new SurgeryNamingScheme("zipcode", true));
    }

    @Override
    public FieldSink<String> getCitySink() {
        return getPropertySink(String.class, new SurgeryNamingScheme("city", true));
    }

    @Override
    public FieldSink<String> getMunicipalitySink() {
        return getPropertySink(String.class, new SurgeryNamingScheme("municipality", false));
    }

    @Override
    public FieldSink<String> getCountySink() {
        return getPropertySink(String.class, new SurgeryNamingScheme("county", false));
    }

    @Override
    public FieldSink<String> getSublocalitySink() {
        return getPropertySink(String.class, new SurgeryNamingScheme("sublocality", false));
    }


    @Override
    public FieldSink<String> getEmailSink() {
        return getPropertySink(String.class, new SurgeryNamingScheme("email", true));
    }

    @Override
    public FieldSink<String> getWebUrlSink() {
        return getPropertySink(String.class, new SurgeryNamingScheme("web_url", true));
    }

    @Override
    public FieldSink<String> getPhoneSink() {
        return getPropertySink(String.class, new SurgeryNamingScheme("phone", true));
    }

    @Override
    public FieldSink<Integer> getIdSink() {

        final NamingScheme idNamingScheme = new SurgeryNamingScheme("surgery_id", false);
        final NamingScheme idEditableNamingScheme = new SurgeryNamingScheme("surgery_editable_id", false);
        final NamingScheme addressNamingScheme = new SurgeryNamingScheme("address", true);

        FieldSink<Integer> id =
            getPropertySink(Integer.class, idNamingScheme);

        FieldSink<Long> editableId =
            getPropertySink(Long.class, idEditableNamingScheme);

        final PairedSinks<Integer, Long> idPairedSink = new PairedSinks<Integer, Long>(id, editableId);

        return new ComposableSink<Integer, Pair<Integer, Long>>(idPairedSink) {
            @Override
            protected Pair<Integer, Long> f(Integer v) throws ConduitException {
                currentId = v;
                return Pair.of(v, getObjectId(addressNamingScheme.getReference()));
            }
        };
    }

    @Override
    public FieldSink<String> getCategorySink() {
        return getPropertySink(String.class, new SurgeryNamingScheme("category", false));
    }

    private class SurgeryNamingScheme implements NamingScheme
    {

        private final String fieldName;

        private final boolean editable;        

        SurgeryNamingScheme(String fieldName, boolean editable) {
            this.fieldName = fieldName;
            this.editable = editable;
        }

        @Override
        public ObjectPropertyReference getReference() {
            SpaceReference spaceRef =
                new SpaceReference(currentId.toString(), editable ? surgeryEditSpace : surgeryRoSpace);
            DocumentReference classRef = editable ? surgeryEditClass : surgeryRoClass;
            DocumentReference docRef = new DocumentReference("WebHome", spaceRef);
            return getPropertyReference(docRef, classRef, 0, fieldName);
        }

        @Override
        public String toString() {
            return getReference().toString();
        }
            
    }
}
