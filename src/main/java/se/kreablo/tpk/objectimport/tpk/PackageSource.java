/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.objectimport.tpk;

import org.xwiki.component.annotation.Role;
import se.kreablo.tpk.objectimport.FieldSource;
import se.kreablo.tpk.objectimport.ConduitException;
import se.kreablo.tpk.objectimport.CycleItem;

/**
 * Source bundle for package objects.
 * @param <C> Configuration type.
 * @version $Id$
 */
@Role
public interface PackageSource<C> extends CycleItem
{

    /** @return source for code field. */
    FieldSource<Integer> getCodeSource();

    /** @return source for version field. */
    FieldSource<Integer> getVersionSource();

    /** @return source for text code field. */
    FieldSource<String> getTextCodeSource();

    /** @return source for title field. */
    FieldSource<String> getTitleSource();

    /** @return source for area field. */
    FieldSource<String> getAreaSource();

    /** @return source for description field. */
    FieldSource<String> getDescriptionSource();

    /** @return source for description_more field. */
    FieldSource<String> getDescriptionMoreSource();

    /** @return source for measures field. */
    FieldSource<String> getMeasuresSource();

    /** @return source for price field. */
    FieldSource<Integer> getPriceSource();
    
    /**
     * Configure sources.
     * @param config The configuration.
     * @throws ConduitException on failure.
     */
    void start(C config) throws ConduitException;
}
