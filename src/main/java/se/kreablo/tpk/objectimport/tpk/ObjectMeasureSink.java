package se.kreablo.tpk.objectimport.tpk;

import static org.xwiki.component.descriptor.ComponentInstantiationStrategy.PER_LOOKUP;
import org.xwiki.component.annotation.InstantiationStrategy;
import org.xwiki.component.annotation.Component;
import org.xwiki.component.phase.Initializable;
import org.xwiki.model.reference.ObjectPropertyReference;
import org.xwiki.model.reference.DocumentReference;
import org.xwiki.model.reference.SpaceReference;

import se.kreablo.tpk.objectimport.object.ObjectSink;
import se.kreablo.tpk.objectimport.FieldSink;
import se.kreablo.tpk.objectimport.ComposableSink;
import static se.kreablo.tpk.Constants.MEASURE_CLASS;
import static se.kreablo.tpk.Constants.MEASURE_SPACE;

import javax.inject.Named;

/**
 * Sink for creating measure objects and inserting measure object data.
 * @version $Id$
 */
@Component
@Named("object")
@InstantiationStrategy(PER_LOOKUP)
public class ObjectMeasureSink extends ObjectSink implements MeasureSink, Initializable
{

    private Integer currentCode;

    private Integer currentVersion;

    private SpaceReference spaceRef;

    private DocumentReference classRef;

    @Override
    public void initialize() {
        spaceRef = getDocumentReferenceResolver()
            .resolve(MEASURE_SPACE + ".WebHome").getLastSpaceReference();
        classRef = getDocumentReferenceResolver().resolve(MEASURE_CLASS);
    }

    private NamingScheme n(String name) {
        return new MeasureNamingScheme(name);
    }

    @Override
    public FieldSink<Integer> getCodeSink() {
        FieldSink<Integer> codeSink = getPropertySink(Integer.class, n("code"));

        return new ComposableSink<Integer, Integer>(codeSink) {
            @Override
            public Integer f(Integer v) {
                currentCode = v;
                return v;
            }
        };
    }

    @Override
    public FieldSink<Integer> getVersionSink() {
        FieldSink<Integer> versionSink = getPropertySink(Integer.class, n("version"));
        return new ComposableSink<Integer, Integer>(versionSink) {
            @Override
            public Integer f(Integer v) {
                currentVersion = v;
                return v;
            }
        };
    }

    @Override
    public FieldSink<String> getTitleSink() {
        return getPropertySink(String.class, n("title"));
    }

    @Override
    public FieldSink<Integer> getPriceSink() {
        return getPropertySink(Integer.class, n("price"));
    }

    @Override
    public FieldSink<Integer> getPriceSpecialSink() {
        return getPropertySink(Integer.class, n("price_special"));
    }

    @Override
    public FieldSink<String> getAreaSink() {
        return getPropertySink(String.class, n("area"));
    }

    @Override
    public FieldSink<String> getAreaSpecialSink() {
        return getPropertySink(String.class, n("area_special"));
    }

    private class MeasureNamingScheme implements NamingScheme
    {

        private final String fieldName;

        MeasureNamingScheme(String fieldName) {
            this.fieldName = fieldName;
        }

        @Override
        public ObjectPropertyReference getReference() {
            SpaceReference space = new SpaceReference(currentVersion.toString(), spaceRef);
            DocumentReference docRef = new DocumentReference(currentCode.toString(), space);

            return getPropertyReference(docRef, classRef, 0, fieldName);
        }

    }
}
