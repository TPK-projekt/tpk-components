/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.objectimport.tpk;

import static org.xwiki.component.descriptor.ComponentInstantiationStrategy.PER_LOOKUP;
import org.xwiki.component.annotation.InstantiationStrategy;
import org.xwiki.component.annotation.Component;

import se.kreablo.tpk.objectimport.FieldSource;
import se.kreablo.tpk.objectimport.CountingFieldSource;
import se.kreablo.tpk.objectimport.NumberSource;
import se.kreablo.tpk.objectimport.NonNullSource;
import se.kreablo.tpk.objectimport.IntegerSource;
import se.kreablo.tpk.objectimport.csv.CSVSource;

import javax.inject.Named;

/**
 * Source for extracting package objects from a CSV document.
 * @version $Id$
 */
@Component
@Named("csv")
@InstantiationStrategy(PER_LOOKUP)
public class CSVPackageSource extends CSVSource implements PackageSource<CSVSourceConfig>
{

    @Override
    public FieldSource<Integer> getCodeSource() {
        return new CountingFieldSource();
    }

    @Override
    public FieldSource<Integer> getVersionSource() {
        return getNextVersionSource();
    }

    @Override
    public FieldSource<String> getTextCodeSource() {
        return new NonNullSource(getColumnSource(2));
    }

    @Override
    public FieldSource<String> getTitleSource() {
        return getColumnSource(3);
    }

    @Override
    public FieldSource<String> getAreaSource() {
        return getColumnSource(1);
    }

    @Override
    public FieldSource<String> getDescriptionSource() {
        return getColumnSource(4);
    }

    @Override
    public FieldSource<String> getDescriptionMoreSource() {
        return getColumnSource(5);
    }

    @Override
    public FieldSource<String> getMeasuresSource() {
        return getColumnSource(6);
    }

    @Override
    public FieldSource<Integer> getPriceSource() {
        return new NonNullSource(new IntegerSource(new NumberSource(getColumnSource(7), getNumberFormat())));
    }


    @Override
    public void start(CSVSourceConfig config) {
        setInput(config.getInput());
    }

}
