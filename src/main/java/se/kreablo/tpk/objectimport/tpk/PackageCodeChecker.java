/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.objectimport.tpk;

import se.kreablo.tpk.internal.AbstractTPKService;

import static se.kreablo.tpk.schema.Tables.TVPRIS_ITEM;
import static se.kreablo.tpk.schema.Tables.TVPRIS_PACKAGE;

import org.jooq.exception.DataAccessException;
import org.jooq.DSLContext;
import org.jooq.Table;
import org.jooq.Record1;

import javax.inject.Named;
import javax.inject.Singleton;

import org.xwiki.component.annotation.Component;

/**
 * Validate that package code is a valid package code for next version.
 * @version $Id$
 */
@Component
@Named("package")
@Singleton
public class PackageCodeChecker extends AbstractTPKService implements CodeChecker<String>
{

    @Override
    public boolean check(String mcode) {

        try {
            final DSLContext ctx = getContext();

            final Table<Record1<Integer>> versionTable = versionQuery(ctx).asTable();

            return ctx.fetchExists(
                ctx.select(TVPRIS_ITEM.TVPI_CODE)
                .from(versionTable)                   
                .crossJoin(TVPRIS_ITEM).join(TVPRIS_PACKAGE).on(TVPRIS_PACKAGE.XWO_ID.eq(TVPRIS_ITEM.XWO_ID))
                .where(TVPRIS_PACKAGE.TVPP_TEXT_CODE.eq(mcode).and(TVPRIS_ITEM.TVPI_VERSION.add(-1)
                                                                   .eq(versionTable.field(0, Integer.class)))));
        } catch (DataAccessException e) {
        } finally {
            close();
        }
        return false;
    }
}
