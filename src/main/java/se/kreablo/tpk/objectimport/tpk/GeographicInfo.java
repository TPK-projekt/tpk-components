/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.objectimport.tpk;

import java.util.List;
import java.util.ArrayList;

/**
 * POJO for geographic info.
 * @version $Id$
 */
public class GeographicInfo {

    private Double latitude;

    private Double longitude;

    private String municipality;

    private String county;

    private String sublocality;

    private transient List<String> warnings = new ArrayList<>();

    /** @return the latitude */
    public Double getLatitude() {
        return this.latitude;
    }

    /** @param latitude */
    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    /** @return the longitude */
    public Double getLongitude() {
        return this.longitude;
    }

    /** @param longitude */
    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    /** @return the municipality */
    public String getMunicipality() {
        return this.municipality;
    }

    /** @param municipality */
    public void setMunicipality(String municipality) {
        this.municipality = municipality;
    }

    /** @return the county */
    public String getCounty() {
        return this.county;
    }

    /** @param county */
    public void setCounty(String county) {
        this.county = county;
    }

    /** @return the sublocality */
    public String getSublocality() {
        return this.sublocality;
    }

    /** @param sublocality */
    public void setSublocality(String sublocality) {
        this.sublocality = sublocality;
    }

    public void addWarning(String warning) {
        warnings.add(warning);
    }
    
    public List<String> getWarnings() {
        return warnings;
    }
        
}

