/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.objectimport.tpk;

import org.xwiki.component.annotation.Role;

import se.kreablo.tpk.objectimport.FieldSource;
import se.kreablo.tpk.objectimport.CycleItem;
import se.kreablo.tpk.objectimport.ConduitException;

import java.util.List;

/**
 * Source bundle for surgery data.
 * @param <C> Configuration type.
 * @version $Id$
 */
@Role
public interface SurgerySource<C> extends CycleItem
{
    
    /** @return a source for the id field */
    FieldSource<Integer> getIdSource();

    /** @return a source for the name field */
    FieldSource<String> getNameSource();

    /** @return a source for the company field */
    FieldSource<String> getCompanySource();

    /** @return a source for the latitude field */
    FieldSource<Double> getLatitudeSource();

    /** @return a source for the longitude field */
    FieldSource<Double> getLongitudeSource();

    /** @return a source for the address field */
    FieldSource<String> getAddressSource();

    /** @return a source for the postal code field */
    FieldSource<String> getPostalCodeSource();

    /** @return a source for the city field */
    FieldSource<String> getCitySource();

    /** @return a source for the municipality field */
    FieldSource<String> getMunicipalitySource();

    /** @return a source for the county field */
    FieldSource<String> getCountySource();

    /** @return a source for the sublocality field */
    FieldSource<String> getSublocalitySource();

    /** @return a source for the category field */
    FieldSource<String> getCategorySource();

    /** @return a source for the email field */
    FieldSource<String> getEmailSource();

    /** @return a source for the web_url field */
    FieldSource<String> getWebUrlSource();

    /** @return a source for the phone field */
    FieldSource<String> getPhoneSource();

    /**
     * Configure sources.
     * @param config The configuration.
     * @throws ConduitException on failure.
     */
    void start(C config) throws ConduitException;

    /**
     * The source may return a list of warnings.
     * @return list of warnings.
     */
    List<? extends Object> getWarnings();
}
