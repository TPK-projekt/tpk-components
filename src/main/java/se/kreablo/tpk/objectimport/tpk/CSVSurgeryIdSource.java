/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.objectimport.tpk;

import static org.xwiki.component.descriptor.ComponentInstantiationStrategy.PER_LOOKUP;
import org.xwiki.component.annotation.InstantiationStrategy;
import org.xwiki.component.annotation.Component;

import se.kreablo.tpk.objectimport.FieldSource;
import se.kreablo.tpk.objectimport.csv.CSVSource;
import se.kreablo.tpk.objectimport.NumberSource;
import se.kreablo.tpk.objectimport.NonNullSource;
import se.kreablo.tpk.objectimport.IntegerSource;
import javax.inject.Named;

/**
 * Source for extracting surgery identities from a CSV document.
 * @version $Id$
 */
@Component
@Named("csv")
@InstantiationStrategy(PER_LOOKUP)
public class CSVSurgeryIdSource extends CSVSource implements SurgeryIdSource<CSVSourceConfig>
{
    @Override
    public FieldSource<Integer> getIdSource() {
        return new NonNullSource(
            new IntegerSource(
                new NumberSource(getColumnSource(0), getNumberFormat())));

    }

    @Override
    public void start(CSVSourceConfig config) {
        setInput(config.getInput());
    }
}
