/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.objectimport.tpk;

import static org.xwiki.component.descriptor.ComponentInstantiationStrategy.PER_LOOKUP;
import org.xwiki.component.annotation.InstantiationStrategy;
import org.xwiki.component.annotation.Component;

import javax.inject.Named;
import javax.inject.Inject;

import se.kreablo.tpk.objectimport.FieldSource;
import se.kreablo.tpk.objectimport.ConduitException;
import se.kreablo.tpk.objectimport.NumberSource;
import se.kreablo.tpk.objectimport.NonNullSource;
import se.kreablo.tpk.objectimport.IntegerSource;
import se.kreablo.tpk.objectimport.ConstantFieldSource;
import se.kreablo.tpk.objectimport.csv.CSVSource;

import org.apache.commons.lang3.tuple.Pair;
import java.util.List;
import java.util.ArrayList;
import se.kreablo.util.Either;

/**
 * Source bundle for extracting prices from a CSV document.
 * @version $Id$
 */
@Component
@Named("csv")
@InstantiationStrategy(PER_LOOKUP)
public class CSVPriceSource extends CSVSource implements PriceSource<CSVSourceConfig>
{

    @Inject
    @Named("package")
    private CodeChecker<String> packageCodeChecker;

    @Inject
    @Named("measure")
    private CodeChecker<Integer> measureCodeChecker;

    @Override
    public FieldSource<Integer> getSurgeryIdSource() {
        return new NonNullSource(new IntegerSource(new NumberSource(getColumnSource(0), getNumberFormat())));
    }

    @Override
    public FieldSource<Integer> getVersionSource() {
        return getNextVersionSource();
    }

    @Override
    public List<Pair<FieldSource<Either<String, Pair<Boolean, Integer>>>, FieldSource<Integer>>>
        getLabeledPriceSources(final boolean isSpecial) throws ConduitException {
        final List<String> labels = getLabels();
        List<Pair<FieldSource<Either<String, Pair<Boolean, Integer>>>, FieldSource<Integer>>> result =
            new ArrayList<>();
        for (int i = 1; i < labels.size(); i++) {
            Either<String, Integer> item = checkLabel(labels.get(i));
            if (item != null) {
                final FieldSource<Integer> priceSource =
                    new IntegerSource(new NumberSource(getColumnSource(i), getNumberFormat()));
                result.add(
                    item.apply(
                        s -> { return Pair.of(new ConstantFieldSource(Either.left(s)),                      priceSource); },
                        p -> { return Pair.of(new ConstantFieldSource(Either.right(Pair.of(isSpecial, p))), priceSource); }
                               ));
            }
        }
        return result;
    }

    @Override
    public void start(CSVSourceConfig config) {
        setInput(config.getInput());
    }

    private Either<String, Integer> checkLabel(String label) {
        Integer measureCode = checkMeasureCode(label);
        if (measureCode != null) {
            return Either.right(measureCode);
        }
        String packageCode = checkPackageCode(label);
        if (packageCode != null) {
            return Either.left(packageCode);
        }
        return null;
    }

    private Integer checkMeasureCode(String label) {
        try {
            Integer code = Integer.parseInt(label);
            if (measureCodeChecker.check(code)) {
                return code;
            }
        } catch (NumberFormatException e) {
        }

        return null;
    }

    private String checkPackageCode(String label) {
        return packageCodeChecker.check(label) ? label : null;
    }
}
