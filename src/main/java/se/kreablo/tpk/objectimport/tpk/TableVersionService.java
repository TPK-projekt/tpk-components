/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.objectimport.tpk;

import static org.xwiki.component.descriptor.ComponentInstantiationStrategy.PER_LOOKUP;
import org.xwiki.component.annotation.InstantiationStrategy;
import org.xwiki.component.annotation.Component;

import se.kreablo.tpk.internal.AbstractTPKService;

import javax.inject.Named;
import javax.inject.Inject;

import org.jooq.SelectQuery;
import org.jooq.Record1;
import org.jooq.DSLContext;

import static se.kreablo.tpk.schema.Tables.TPK_ITEMPRICE;
import static se.kreablo.tpk.schema.Tables.TVPRIS_VERSIONCLASS;

/**
 * Read version number from database and manipulate version number.
 * @version $Id$
 */
@Component
@Named("table")
@InstantiationStrategy(PER_LOOKUP)
public class TableVersionService extends AbstractTPKService implements VersionService
{

    @Inject
    @Named("document")
    private VersionDeleter documentVersionDeleter;
    
    @Override
    public int getVersion() {

        try {
            SelectQuery<Record1<Integer>> query = versionQuery(getContext());
            Record1<Integer> record = query.fetchOne();

            return record.value1();
        } catch (RuntimeException e) {
            getLogger().error("Exception when querying version", e);
            throw e;
        } finally {
            close();
        }
    }

    @Override
    public void setVersion(int version) {
        try {
            final DSLContext ctx = getContext(true);
            int n = ctx.update(TVPRIS_VERSIONCLASS).set(TVPRIS_VERSIONCLASS.TPKV_VERSION, version).execute();
            getLogger().trace("Updated version to {}, num updated {}", version, n);
        } finally {
            close();
        }
    }

    @Override
    public void deleteVersion(int version) {
        if (getVersion() == version) {
            throw new RuntimeException("Refuse to delete current version.");
        }
        try {
            final DSLContext ctx = getContext(true);
            long lversion = (long) version;
            int n = ctx.deleteFrom(TPK_ITEMPRICE).where(TPK_ITEMPRICE.TVPIP_VERSION.eq(lversion)).execute();
            getLogger().trace("Deleted {} item prices from version {}", n, version);
        } finally {
            close();
        }
        documentVersionDeleter.delete(version);
    }

}
