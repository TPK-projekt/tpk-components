/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.objectimport.tpk;

import org.xwiki.component.annotation.Component;
import org.xwiki.bridge.DocumentAccessBridge;
import org.xwiki.bridge.DocumentModelBridge;
import org.xwiki.model.reference.DocumentReference;
import org.xwiki.model.reference.DocumentReferenceResolver;
import org.xwiki.query.QueryManager;
import org.xwiki.query.Query;
import org.xwiki.query.QueryException;

import com.xpn.xwiki.XWikiContext;
import com.xpn.xwiki.doc.XWikiDocument;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.inject.Provider;
import org.slf4j.Logger;

import static se.kreablo.tpk.Constants.MEASURE_CLASS;
import static se.kreablo.tpk.Constants.PACKAGE_CLASS;

/**
 * Component for deleting documents corresponding to a particlular version.
 * @version $Id$
 */
@Component
@Singleton
@Named("document")
public class DocumentVersionDeleter implements VersionDeleter
{

    @Inject
    private QueryManager queryManager;

    @Inject
    private DocumentAccessBridge documentAccessBridge;

    @Inject
    @Named("current")
    private DocumentReferenceResolver documentReferenceResolver;

    @Inject
    private Logger logger;

    @Inject
    Provider<XWikiContext> xcontextProvider;

    private Query getQueryForClass(String className, int version) throws QueryException {
        Query q = queryManager.createQuery("from doc.object(" + className
                                           + ") as o where o.version = :version", Query.XWQL);
        return q.bindValue("version", (Integer) version);
    }
    
    @Override
    public void delete(final int version) {
        try {
            final Query qm = getQueryForClass(MEASURE_CLASS, version);
            final Query qp = getQueryForClass(PACKAGE_CLASS, version);

            final Query [] qs = { qm, qp };
            for (Query q : qs) {
                for (Object docNameO : q.execute()) {
                    try {
                        String docName = null;
                        if (docNameO instanceof String) {
                            docName = (String) docNameO;
                        }
                        final DocumentReference docRef = documentReferenceResolver.resolve(docName);

                        DocumentModelBridge doc = documentAccessBridge.getDocumentInstance(docRef);

                        logger.trace("Deleting document {}", docRef);

                        xcontextProvider.get().getWiki().deleteDocument((XWikiDocument) doc,
                                                                        xcontextProvider.get());
                        
                    } catch (Exception e) {
                        logger.error("Exception when deleting document.", e);
                    }
                }
            }
        } catch (QueryException e) {
            throw new RuntimeException(e);
        }
        
    }
}
