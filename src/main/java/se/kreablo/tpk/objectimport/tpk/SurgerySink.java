/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.objectimport.tpk;

import org.xwiki.component.annotation.Role;

import se.kreablo.tpk.objectimport.FieldSink;

/**
 * Sink bundle for surgery data.
 * @version $Id$
 */
@Role
public interface SurgerySink
{
    /** @return a sink for the id field */
    FieldSink<Integer> getIdSink();

    /** @return a sink for the name field */
    FieldSink<String> getNameSink();

    /** @return a sink for the company field */
    FieldSink<String> getCompanySink();

    /** @return a sink for the latitude field */
    FieldSink<Double> getLatitudeSink();

    /** @return a sink for the longitude field */
    FieldSink<Double> getLongitudeSink();

    /** @return a sink for the address field */
    FieldSink<String> getAddressSink();

    /** @return a sink for the postal code field */
    FieldSink<String> getPostalCodeSink();

    /** @return a sink for the city field */
    FieldSink<String> getCitySink();

    /** @return a sink for the municipality field */
    FieldSink<String> getMunicipalitySink();

    /** @return a sink for the county field */
    FieldSink<String> getCountySink();

    /** @return a sink for the sublocality field */
    FieldSink<String> getSublocalitySink();

    /** @return a sink for the category field */
    FieldSink<String> getCategorySink();

    /** @return a sink for the email field */
    FieldSink<String> getEmailSink();

    /** @return a sink for the web_url field */
    FieldSink<String> getWebUrlSink();

    /** @return a sink for the phone field */
    FieldSink<String> getPhoneSink();
    
}
