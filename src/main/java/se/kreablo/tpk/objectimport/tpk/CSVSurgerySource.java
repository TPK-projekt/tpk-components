/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.objectimport.tpk;

import static org.xwiki.component.descriptor.ComponentInstantiationStrategy.PER_LOOKUP;
import org.xwiki.component.annotation.InstantiationStrategy;
import org.xwiki.component.annotation.Component;

import se.kreablo.tpk.objectimport.FieldSource;
import se.kreablo.tpk.objectimport.SpyingSource;
import se.kreablo.tpk.objectimport.SharedCycleItem;
import se.kreablo.tpk.objectimport.PairedSources;
import se.kreablo.tpk.objectimport.ComposableSource;
import se.kreablo.tpk.objectimport.NumberSource;
import se.kreablo.tpk.objectimport.NonNullSource;
import se.kreablo.tpk.objectimport.ConduitException;
import se.kreablo.tpk.objectimport.IntegerSource;
import se.kreablo.tpk.objectimport.csv.CSVSource;

import org.apache.commons.lang3.tuple.Pair;

import javax.inject.Named;
import javax.inject.Inject;
import java.util.List;
import java.util.ArrayList;

/**
 * Source for extracting surgery objects from a CSV document.
 * @version $Id$
 */
@Component
@Named("csv")
@InstantiationStrategy(PER_LOOKUP)
public class CSVSurgerySource extends CSVSource implements SurgerySource<CSVSourceConfig>
{

    /**
     * The code for denoting a publically owned surgery.
     */
    public static final String CATEGORY_PUBLIC = "Landsting";

    /**
     * The code for denoting a privately owned surgery.
     */
    public static final String CATEGORY_PRIVATE = "Privat";

    private FieldSource<String> addressSource;

    private FieldSource<String> postalCodeSource;

    private FieldSource<String> citySource;

    private FieldSource<String> emailSource;
    
    private FieldSource<String> webUrlSource;
    
    private FieldSource<String> phoneSource;

    private FieldSource<GeographicInfo> geographicInfoSource;

    private List<WarningMsg> warnings = new ArrayList<WarningMsg>();

    private Integer currentId;

    private String currentName;

    @Inject
    @Named("google")
    private GeographicInfoService geographicInfoService;
    
    @Override
    public FieldSource<String> getNameSource() {
        return new ComposableSource<String, String>(getColumnSource(4)) {
            @Override
            public String f(String s) {
                currentName = s;
                return s;
            }
        };
    }

    @Override
    public FieldSource<String> getCompanySource() {
        return getColumnSource(3);
    }

    @Override
    public FieldSource<Double> getLatitudeSource() {
        return new ComposableSource<GeographicInfo, Double>(
            new SpyingSource(getGeographicInfoSource())) {
            @Override
            public Double f(GeographicInfo geo) {
                if (geo == null) {
                    return null;
                }
                return geo.getLatitude();
            }
        };
    }

    @Override
    public FieldSource<Double> getLongitudeSource() {
        return new ComposableSource<GeographicInfo, Double>(
            new SpyingSource(getGeographicInfoSource())) {
            @Override
            public Double f(GeographicInfo geo) {
                if (geo == null) {
                    return null;
                }
                return geo.getLongitude();
            }
        };
    }

    @Override
    public FieldSource<String> getAddressSource() {
        if (addressSource == null) {
            addressSource = getColumnSource(5);
        }
        return addressSource;
    }

    @Override
    public FieldSource<String> getPostalCodeSource() {
        if (postalCodeSource == null) {
            postalCodeSource = getColumnSource(6);
        }
        return postalCodeSource;
    }

    @Override
    public FieldSource<String> getCitySource() {
        if (citySource == null) {
            citySource = getColumnSource(7);
        }
        return citySource;
    }

    @Override
    public FieldSource<String> getEmailSource() {
        if (emailSource == null) {
            emailSource = getColumnSource(8);
        }
        return emailSource;
    }


    @Override
    public FieldSource<String> getWebUrlSource() {
        if (webUrlSource == null) {
            webUrlSource = getColumnSource(9);
        }
        return webUrlSource;
    }

    @Override
    public FieldSource<String> getPhoneSource() {
        if (phoneSource == null) {
            phoneSource = getColumnSource(10);
        }
        return phoneSource;
    }

    private FieldSource<GeographicInfo> getGeographicInfoSource() {
        if (geographicInfoSource == null) {
            geographicInfoSource = createGeographicInfoSource();
        }
        return geographicInfoSource;
    }

    private FieldSource<GeographicInfo> createGeographicInfoSource() {
        if (geographicInfoSource != null) {
            return geographicInfoSource;
        }
        
        final FieldSource<String> spyingAddressSource = new SpyingSource(getAddressSource());
        final FieldSource<String> spyingCitySource = new SpyingSource(getCitySource());
        final FieldSource<String> spyingPostalCodeSource = new SpyingSource(getPostalCodeSource());

        final FieldSource<Pair<Pair<String, String>, String>> bundledSources =
            new PairedSources<Pair<String, String>, String>(
                new PairedSources<String, String>(spyingAddressSource, spyingCitySource), spyingPostalCodeSource);

        geographicInfoSource = new ComposableSource<Pair<Pair<String, String>, String>, GeographicInfo>(bundledSources) {

                private GeographicInfo geographicInfo;

                @Override
                public GeographicInfo f(Pair<Pair<String, String>, String> bundle) throws ConduitException {
                    if (geographicInfo != null) {
                        return geographicInfo;
                    }
                    final String address = bundle.getLeft().getLeft();
                    final String city = bundle.getLeft().getRight();
                    final String postalCode = bundle.getRight();

                    geographicInfo = geographicInfoService.getFromAddress(address, postalCode, city);

                    return geographicInfo;
                }

                @Override
                public void start() {
                    geographicInfo = null;
                }
            };

        getCycleNumber().addListener(geographicInfoSource);

        return geographicInfoSource;
    }

    @Override
    public FieldSource<String> getMunicipalitySource() {
        return new ComposableSource<GeographicInfo, String>(
            new SpyingSource(getGeographicInfoSource())) {
            @Override
            public String f(GeographicInfo geo) {
                if (geo == null) {
                    return null;
                }
                return geo.getMunicipality();
            }

            @Override
            public void end() throws ConduitException {
                if (geographicInfoSource != null && geographicInfoSource.await() != null) {
                    for (String wmsg : geographicInfoSource.await().getWarnings()) {
                        warnings.add(new WarningMsg(currentId, currentName, wmsg));
                    }
                }
            }
        };
    }

    private FieldSource<String> getOriginalCountySource() {
        return getColumnSource(2);
    }

    @Override
    public FieldSource<String> getCountySource() {
        final FieldSource<String> countySource =
            new ComposableSource<GeographicInfo, String>(
                new SpyingSource(getGeographicInfoSource())) {
                @Override
                public String f(GeographicInfo geo) {
                    if (geo == null) {
                        return null;
                    }
                    return geo.getCounty();
                }
            };
        final FieldSource<Pair<String, String>> paired =
            new PairedSources<String, String>(countySource, getOriginalCountySource());

        return new ComposableSource<Pair<String, String>, String>(paired) {
            @Override
            public String f(Pair<String, String> pair) {
                if (pair.getLeft() != null) {
                    return pair.getLeft();
                }
                return pair.getRight();
            }
        };
    }

    @Override
    public FieldSource<String> getSublocalitySource() {
        return new ComposableSource<GeographicInfo, String>(
            new SpyingSource(getGeographicInfoSource())) {
            @Override
            public String f(GeographicInfo geo) {
                if (geo == null) {
                    return null;
                }
                return geo.getSublocality();
            }
        };
    }

    @Override
    public FieldSource<Integer> getIdSource() {
        return new ComposableSource<Integer, Integer>(
            new NonNullSource(new IntegerSource(new NumberSource(getColumnSource(0), getNumberFormat())))) {
            @Override
            public Integer f(Integer id) {
                currentId = id;
                return id;
            }
        };
    }

    @Override
    public FieldSource<String> getCategorySource() {
        return new ComposableSource<String, String>(getColumnSource(1)) {
            @Override
            public String f(String s) {
                if (CATEGORY_PRIVATE.equalsIgnoreCase(s)) {
                    return CATEGORY_PRIVATE;
                }
                return CATEGORY_PUBLIC;
            }
        };
    }

    @Override
    public void start(CSVSourceConfig config) {
        setInput(config.getInput());
    }

    @Override
    public List<? extends Object> getWarnings() {
        return warnings;
    }

    private class WarningMsg {

        private final int surgeryId;

        private final String surgeryName;

        private final String msg;

        public WarningMsg(int surgeryId, String surgeryName, String msg) {
            this.surgeryId = surgeryId;
            this.surgeryName = surgeryName;
            this.msg = msg;
        }

        public int getSurgeryId() {
            return surgeryId;
        }

        public String getName() {
            return surgeryName;
        }

        public String getMessage() {
            return msg;
        }
    
    }

}
