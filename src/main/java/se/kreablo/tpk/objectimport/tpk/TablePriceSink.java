/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.objectimport.tpk;

import java.util.Map;
import java.util.HashMap;
import javax.inject.Named;

import static org.xwiki.component.descriptor.ComponentInstantiationStrategy.PER_LOOKUP;
import org.xwiki.component.annotation.InstantiationStrategy;
import org.xwiki.component.annotation.Component;

import se.kreablo.tpk.objectimport.database.TableSink;
import se.kreablo.tpk.objectimport.FieldSink;
import se.kreablo.tpk.objectimport.ComposableSink;
import se.kreablo.tpk.objectimport.PairedSinks;
import se.kreablo.util.Either;

import org.apache.commons.lang3.tuple.Pair;

import se.kreablo.tpk.schema.tables.TpkItemprice;
import se.kreablo.tpk.schema.tables.records.TpkItempriceRecord;
import static se.kreablo.tpk.schema.Tables.TVPRIS_ITEM;
import static se.kreablo.tpk.schema.Tables.TVPRIS_PACKAGE;
import static se.kreablo.tpk.schema.Tables.TPK_ITEMPRICE;

import org.jooq.Table;
import org.jooq.Record1;

/**
 * Generate sinks for inserting price data into database table.
 * @version $Id$
 */
@Component
@Named("table")
@InstantiationStrategy(PER_LOOKUP)
public class TablePriceSink extends TableSink<TpkItemprice, TpkItempriceRecord> implements PriceSink
{

    private final Map<String, FieldSink<Integer>> sinks = new HashMap<>();

    /**
     * Constructor.
     */
    public TablePriceSink() {
        super(TPK_ITEMPRICE);
    }

    @Override
    public FieldSink<Integer> getSurgeryIdSink() {
        return getColumnSink(table.TVPIP_SURGERY_ID);
    }

    @Override
    public FieldSink<Integer> getVersionSink() {
        return new ComposableSink<Integer, Long>(getColumnSink(table.TVPIP_VERSION)) {
            @Override
            public Long f(Integer v) {
                return v.longValue();
            }
        };
    }

    @Override
    public FieldSink<Integer> getPriceSink() {
        return getColumnSink(table.TVPIP_PRICE, true);
    }

    @Override
    public FieldSink<Either<String, Pair<Boolean, Integer>>> getItemIdSink() {

        final FieldSink<Pair<Boolean, Integer>> typeSink =
            new PairedSinks<Boolean, Integer>(
                getColumnSink(table.TVPIP_IS_MEASURE),
                getColumnSink(table.TVPIP_TYPE));

        final Table<Record1<Integer>> versionTable = versionQuery(getContext(true)).asTable();
            
        final FieldSink<Either<String, Integer>> idSink =
            getColumnSink(
                v -> {
                    return getContext(true).select(TVPRIS_ITEM.TVPI_CODE)
                    .from(versionTable)
                    .crossJoin(TVPRIS_ITEM).join(TVPRIS_PACKAGE)
                    .on(TVPRIS_PACKAGE.XWO_ID.equal(TVPRIS_ITEM.XWO_ID))
                    .where(TVPRIS_PACKAGE.TVPP_TEXT_CODE.eq(v)
                           .and(TVPRIS_ITEM.TVPI_VERSION.add(-1)
                                .eq(versionTable.field(0, Integer.class)))).asField();
                }, table.TVPIP_ITEM_ID);

        final FieldSink<Pair<Pair<Boolean, Integer>, Either<String, Integer>>> sinkBundle =
            new PairedSinks<>(typeSink, idSink);

        return new ComposableSink<Either<String, Pair<Boolean, Integer>>,
            Pair<Pair<Boolean, Integer>, Either<String, Integer>>>(sinkBundle) {
            @Override
            public Pair<Pair<Boolean, Integer>, Either<String, Integer>>
                f(Either<String, Pair<Boolean, Integer>> bundle) {
                return bundle.apply(
                    pid -> {
                        return Pair.of(Pair.of(false, ItemPriceType.PACKAGE.getCode()), Either.left(pid));
                    },
                    pair -> {
                        return Pair.of(
                            Pair.of(
                                true,
                                pair.getLeft()
                                ? ItemPriceType.MEASURE_SPECIAL.getCode()
                                : ItemPriceType.MEASURE.getCode()),
                            Either.right(pair.getRight()));
                    });
            }
                
        };
    }

}
