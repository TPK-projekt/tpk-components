package se.kreablo.tpk.objectimport.tpk;

import static org.xwiki.component.descriptor.ComponentInstantiationStrategy.PER_LOOKUP;
import org.xwiki.component.annotation.InstantiationStrategy;
import org.xwiki.component.annotation.Component;
import org.xwiki.component.phase.Initializable;
import org.xwiki.model.reference.ObjectPropertyReference;
import org.xwiki.model.reference.DocumentReference;
import org.xwiki.model.reference.SpaceReference;

import se.kreablo.tpk.objectimport.object.ObjectSink;
import se.kreablo.tpk.objectimport.FieldSink;
import se.kreablo.tpk.objectimport.ComposableSink;
import static se.kreablo.tpk.Constants.PACKAGE_SPACE;
import static se.kreablo.tpk.Constants.PACKAGE_CLASS;

import javax.inject.Named;

/**
 * Sink for creating package objects and inserting package object data.
 * @version $Id$
 */
@Component
@Named("object")
@InstantiationStrategy(PER_LOOKUP)
public class ObjectPackageSink extends ObjectSink implements PackageSink, Initializable
{

    private Integer currentCode;

    private Integer currentVersion;

    private String currentTextCode;

    private SpaceReference spaceRef;

    private DocumentReference classRef;

    @Override
    public void initialize() {
        spaceRef = getDocumentReferenceResolver().resolve(PACKAGE_SPACE + ".WebHome").getLastSpaceReference();
        classRef = getDocumentReferenceResolver().resolve(PACKAGE_CLASS);
    }

    private NamingScheme n(String name) {
        return new PackageNamingScheme(name);
    }

    @Override
    public FieldSink<Integer> getCodeSink() {
        FieldSink<Integer> codeSink = getPropertySink(Integer.class, n("code"));

        return new ComposableSink<Integer, Integer>(codeSink) {
            @Override
            public Integer f(Integer v) {
                currentCode = v;
                return v;
            }
        };
    }

    @Override
    public FieldSink<String> getTextCodeSink() {
        FieldSink<String> codeSink = getPropertySink(String.class, n("text_code"));

        return new ComposableSink<String, String>(codeSink) {
            @Override
            public String f(String v) {
                currentTextCode = v;
                return v;
            }
        };
    }

    @Override
    public FieldSink<Integer> getVersionSink() {
        FieldSink<Integer> versionSink = getPropertySink(Integer.class, n("version"));
        return new ComposableSink<Integer, Integer>(versionSink) {
            @Override
            public Integer f(Integer v) {
                currentVersion = v;
                return v;
            }
        };
    }

    @Override
    public FieldSink<String> getTitleSink() {
        return getPropertySink(String.class, n("title"));
    }

    @Override
    public FieldSink<Integer> getPriceSink() {
        return getPropertySink(Integer.class, n("price"));
    }

    @Override
    public FieldSink<String> getAreaSink() {
        return getPropertySink(String.class, n("area"));
    }

    @Override
    public FieldSink<String> getDescriptionSink() {
        return getPropertySink(String.class, n("description"));
    }

    @Override
    public FieldSink<String> getDescriptionMoreSink() {
        return getPropertySink(String.class, n("description_more"));
    }

    @Override
    public FieldSink<String> getMeasuresSink() {
        return getPropertySink(String.class, n("measures"));
    }
    
    private class PackageNamingScheme implements NamingScheme
    {

        private final String fieldName;

        PackageNamingScheme(String fieldName) {
            this.fieldName = fieldName;
        }

        @Override
        public ObjectPropertyReference getReference() {
            SpaceReference space = new SpaceReference(currentVersion.toString(), spaceRef);
            DocumentReference docRef = new DocumentReference(currentTextCode.toString(), space);

            return getPropertyReference(docRef, classRef, 0, fieldName);
        }

    }
}
