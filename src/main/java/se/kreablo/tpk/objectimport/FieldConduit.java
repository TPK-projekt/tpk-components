/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.objectimport;

import java.util.Collection;

/**
 * A conduit ties together a sink and a source and may also drive
 * their cycles.  Several sinks can be connected to the same source.
 * @param <T> The source and sink item types.
 * @version $Id$
 */
public class FieldConduit<T> implements CycleItem
{

    private final FieldSource<T> source;

    private final FieldSink<T> [] sinks;

    private final boolean driveSource;

    private final boolean driveSink;

    /**
     * @param source The source part.
     * @param sinks The sinks.
     */
    public FieldConduit(FieldSource<T> source, FieldSink<T>... sinks) {
        this(true, source, true, sinks);
    }

    /**
     * @param source The source part.
     * @param sinks The sinks as list.
     */
    public FieldConduit(FieldSource<T> source, Collection<FieldSink<T>> sinks) {
        this(source, sinks.toArray(new FieldSink[0]));
    }

    /**
     * @param driveSource Indicate wether this conduit should drive
     *                    its source to synchronize the source's cycles to the conduit's.
     * @param source The source part.
     * @param driveSink Indicate wether this conduit should drive its
     *                           sinks to synchronize the sinks' cycles to the conduit's.
     * @param sinks The sinks as list.
     */
    public FieldConduit(boolean driveSource, FieldSource<T> source, boolean driveSink, FieldSink<T>... sinks) {
        this.source = source;
        this.sinks = sinks;
        this.driveSource = driveSource;
        this.driveSink = driveSink;
    }
     
    @Override
    public void startCycle() throws ConduitException {
        if (driveSink) {
            for (FieldSink<T> sink : sinks) {
                sink.startCycle();
            }
        }
        if (driveSource) {
            source.startCycle();
        }
    }

    /**
     * Transfer the values from the source to the sinks.
     * @throws ConduitException on failure.
     */
    public void transferValue() throws ConduitException  {
        for (FieldSink<T> sink : sinks) {
            sink.yield(source.await());
        }
    }

    @Override
    public void endCycle() throws ConduitException  {
        if (driveSource) {
            source.endCycle();
        }
        if (driveSink) {
            for (FieldSink<T> sink : sinks) {
                sink.endCycle();
            }
        }
    }

    @Override
    public void cancelCycle() {
        if (driveSource) {
            source.cancelCycle();
        }
        if (driveSink) {
            for (FieldSink<T> sink : sinks) {
                sink.cancelCycle();
            }
        }
    }
    
}
