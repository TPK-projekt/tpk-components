/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.filters;

import javax.servlet.Filter;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CachingHeadersFilter implements Filter {

    private static final String CACHE_CONTROL = "Cache-control";
    
    private static final String MUST_REVALIDATE = "must-revalidate";

    private static final String MAX_AGE = "max-age";

    private static final String S_MAXAGE = "s-maxage";

    private String cacheControl = "public";

    private final Logger logger = LoggerFactory.getLogger(CachingHeadersFilter.class);

    @Override
    public void init(FilterConfig config) throws ServletException {

        logger.debug("Initiating filter {} with config {}", this.getClass().getName(), config);

        if (config.getInitParameter(MUST_REVALIDATE) != null) {
            if (isTrue(config.getInitParameter(MUST_REVALIDATE))) {
                cacheControl += ", " + MUST_REVALIDATE;
            }
        }

        if (config.getInitParameter(MAX_AGE) != null) {
            final String s = config.getInitParameter(MAX_AGE);
            long ma;
            try {
                ma = Long.parseLong(s);
            } catch (NumberFormatException e) {
                throw new ServletException("Invalid max-age parameter value.", e);
            }
            cacheControl += ", " + MAX_AGE + "=" + ma;
        }

        if (config.getInitParameter(S_MAXAGE) != null) {
            final String s = config.getInitParameter(S_MAXAGE);
            long ma;
            try {
                ma = Long.parseLong(s);
            } catch (NumberFormatException e) {
                throw new ServletException("Invalid s-maxage parameter value.", e);
            }
            cacheControl += ", " + S_MAXAGE + "=" + ma;
        }
    }

    private boolean isTrue(final String s) {
        return s != null && ("true".equalsIgnoreCase(s) || "yes".equalsIgnoreCase(s));
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
        throws IOException, ServletException {
        ServletResponse rsp = response;
        if (response instanceof HttpServletResponse) {
            rsp =  new HttpServletResponseWrapper((HttpServletResponse) response) {

                    @Override
                    public void setHeader(String name, String value) {
                        if (!name.equalsIgnoreCase("Expires") && !name.equalsIgnoreCase(CACHE_CONTROL) && !name.equalsIgnoreCase("Pragma")) {
                            super.setHeader(name, value);
                        } else {
                            logger.debug("Ignoring setHeader('{}', '{}')", name, value);
                        }
                    }

                    @Override
                    public void addCookie(Cookie cookie) {
                        logger.debug("Ignoring addCookie('{} - {}')", cookie.getName(), cookie.getValue());
                    }
                };

            logger.debug("setHeader('{}', '{}'", CACHE_CONTROL, cacheControl);
            ((HttpServletResponse) response).setHeader(CACHE_CONTROL, cacheControl);
        } else {
            logger.debug("Not an HttpServletResponse ({})", response.getClass().getName());
        }

        chain.doFilter(request, rsp);
    }

    @Override
    public void destroy() {
    }

}
