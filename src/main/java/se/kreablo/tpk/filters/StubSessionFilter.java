/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.filters;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.ServletException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionContext;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.HashMap;
import java.util.Collections;
import java.util.Enumeration;
import java.util.UUID;

public class StubSessionFilter implements Filter {

    private final Logger logger = LoggerFactory.getLogger(StubSessionFilter.class);

    @Override
    public void init(FilterConfig config) throws ServletException {
        logger.trace("init " + config);
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        logger.trace("StubSessionFilter.doFilter");

        if (request instanceof HttpServletRequest) {
            chain.doFilter(new HttpServletRequestWrapper((HttpServletRequest) request) {

                    HttpSession session = null;

                    @Override
                    public HttpSession getSession(boolean create) {

                        logger.trace("StubSessionFilter getSession {}", create);
                        
                        if (session != null) {
                            return session;
                        }

                        if (create) {
                            session = new DummySession(request.getServletContext());
                        }

                        return session;
                    }

                    @Override
                    public HttpSession getSession() {
                        return getSession(true);
                    }
                }, response);
        } else {
            chain.doFilter(request, response);
        }

    }

    @Override
    public void destroy() {
    }

    private static class DummySession implements HttpSession {

        private Map<String, Object> objects = new HashMap<String, Object>();

        private final ServletContext context;

        private final String id = UUID.randomUUID().toString();

        DummySession(ServletContext context) {
            this.context = context;
        }

        @Override
        public boolean isNew() {
            return true;
        }

        @Override
        public void invalidate() {
            objects.clear();
        }

        @Override
        public void removeValue(String name) {
            removeAttribute(name);
        }

        @Override
        public void removeAttribute(String name) {
            objects.remove(name);
        }

        @Override
        public void putValue(String name, Object object) {
            setAttribute(name, object);
        }

        @Override
        public void setAttribute(String name, Object object) {
            objects.put(name, object);
        }

        @Override
        public Object getValue(String name) {
            return getAttribute(name);
        }

        @Override
        public Object getAttribute(String name) {
            return objects.get(name);
        }
        
        @Override
        public String[] getValueNames() {
            return objects.keySet().toArray(new String[0]);
        }

        @Override
        public Enumeration<String> getAttributeNames() {
            return Collections.enumeration(objects.keySet());
        }

        @Override
        public HttpSessionContext getSessionContext() {
            return null;
        }

        @Override
        public int getMaxInactiveInterval() {
            return 0;
        }

        @Override
        public void setMaxInactiveInterval(int interval) {
        }

        @Override
        public ServletContext getServletContext() {
            return context;
        }

        @Override
        public long getLastAccessedTime() {
            return 0;
        }

        @Override
        public String getId() {
            return id;
        }

        @Override
        public long getCreationTime() {
            return 0;
        }
    }
}
