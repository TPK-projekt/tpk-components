/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.util;

import java.util.function.Consumer;
import java.util.function.Function;

/**
 * Implementation of Either sum datatype.
 * @param <L> The datatype of the left value.
 * @param <R> The datatype of the right value.
 * @version $Id$
 */
public final class Either<L, R>
{

    private final L left;

    private final R right;

    private final boolean isLeft;

    private Either(L left, R right, boolean isLeft) {
        this.left = left;
        this.right = right;
        this.isLeft = isLeft;
    }

    /**
     * Construct left value.
     * @param left The left value.
     * @param <L> The datatype of the left value.
     * @param <R> The datatype of the right value.
     * @return An either left value.
     */
    public static <L, R> Either<L, R> left(L left) {
        return new Either<L, R>(left, null, true);
    }

    /**
     * Construct right value.
     * @param right The right value.
     * @param <L> The datatype of the left value.
     * @param <R> The datatype of the right value.
     * @return An either right value.
     */
    public static <L, R> Either<L, R> right(R right) {
        return new Either<L, R>(null, right, false);
    }

    
    /**
     * Apply a consumer on either the right or the left value.
     * @param consumeLeft Consumer for left value.
     * @param consumeRight Consumer for right value.
     */
    public void accept(Consumer<L> consumeLeft, Consumer<R> consumeRight) {
        if (isLeft) {
            consumeLeft.accept(left);
        } else {
            consumeRight.accept(right);
        }
    }

    /**
     * Apply a function on either the right or the left value and return its result.
     * @param processLeft Function for left value.
     * @param processRight Function for right value.
     * @param <T> The return type of the function.
     * @return The result of whichever of the functions where called.
     */
    public <T> T apply(Function<L, T> processLeft, Function<R, T> processRight) {
        return isLeft ? processLeft.apply(left) : processRight.apply(right);
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof Either) {
            Either oe = (Either) other;
            if (oe.isLeft && isLeft) {
                return this.left.equals(oe.left);
            }
            if (!oe.isLeft && !isLeft) {
                return this.right.equals(oe.right);
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        if (isLeft) {
            return left.hashCode();
        }
        return right.hashCode();
    }

    @Override
    public String toString() {
        if (isLeft) {
            return "Left[" + left + "]";
        }
        return "Right[" +  right + "]";
    }
}

    
