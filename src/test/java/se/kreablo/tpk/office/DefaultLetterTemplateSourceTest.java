/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.office;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.Rule;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

import org.xwiki.test.mockito.MockitoComponentMockingRule;
import org.xwiki.model.internal.reference.DefaultStringAttachmentReferenceResolver;
import org.xwiki.model.internal.reference.DefaultStringEntityReferenceResolver;
import org.xwiki.model.reference.AttachmentReferenceResolver;
import org.xwiki.model.reference.AttachmentReference;
import org.xwiki.model.reference.DocumentReference;
import org.xwiki.model.internal.reference.DefaultSymbolScheme;
import org.xwiki.bridge.DocumentAccessBridge;
import org.xwiki.configuration.ConfigurationSource;

import java.io.InputStream;
import java.io.IOException;
import java.io.File;
import java.io.FileInputStream;
import java.net.URI;

import se.kreablo.tpk.office.internal.DefaultLetterTemplateSource;

public class DefaultLetterTemplateSourceTest {

    @Rule
    public MockitoComponentMockingRule<LetterTemplateSource> componentManager =
        new MockitoComponentMockingRule<LetterTemplateSource>(DefaultLetterTemplateSource.class);

    private ConfigurationSource configurationSource;

    private DocumentAccessBridge documentAccessBridge;

    @Before
    public void before() throws Exception {
        configurationSource =  componentManager.registerMockComponent(ConfigurationSource.class);
        documentAccessBridge = componentManager.registerMockComponent(DocumentAccessBridge.class);

        AttachmentReference ref = new AttachmentReference("LetterTemplate.docx", new DocumentReference("xwiki", "TVPris.Infotexter", "WebHome"));
        AttachmentReferenceResolver attachmentReferenceResolver = componentManager.registerMockComponent(AttachmentReferenceResolver.TYPE_STRING, "current");
        when(attachmentReferenceResolver.resolve("TVPris.Infotexter.WebHome@LetterTemplate.docx"))
            .thenReturn(ref);
        
        when(configurationSource.getProperty("tpk.lettertemplate", "TVPris.Infotexter.WebHome@LetterTemplate.docx"))
            .thenReturn("TVPris.Infotexter.WebHome@LetterTemplate.docx");

        when(documentAccessBridge.getAttachmentContent(eq(ref)))
            .thenReturn(this.getClass().getClassLoader()
                        .getResourceAsStream("lettertemplate.docx"));

        InputStream content = documentAccessBridge.getAttachmentContent(ref);

    }

    private boolean compareInputStreams(InputStream a, InputStream b) throws Exception
    {
        final byte [] ba = new byte[1024];
        final byte [] bb = new byte[1024];

        int na = a.read(ba);
        int nb = b.read(bb);

        while (na >= 0 && nb >= 0) {
            if (na != nb) {
                return false;
            }

            for (int i = 0; i < na; i++) {
                if (ba[i] != bb[i]) {
                    return false;
                }
            }
            
            na = a.read(ba);
            nb = b.read(bb);
        }

        if (na >= 0 || nb >= 0) {
            return false;
        }

        return true;
    }

    @Test
    public void testTemplateSource() throws Exception {
        LetterTemplateSource source = componentManager.getInstance(LetterTemplateSource.class);

        InputStream a = this.getClass().getClassLoader()
            .getResourceAsStream("lettertemplate.docx");
        URI uri = new URI(source.getLetterTemplate());
        File file = new File(uri);
        InputStream b = new FileInputStream(file);

        try {
            assertTrue(compareInputStreams(a, b));
            source.close();
            assertFalse(file.exists());
        } finally {
            a.close();
            b.close();
        }

    }
}
