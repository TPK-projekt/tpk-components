/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.office;

import org.junit.Assert;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;
import org.junit.Rule;

import java.util.List;
import java.util.Map;
import java.util.LinkedList;
import java.util.HashMap;
import java.io.File;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

import org.xwiki.test.mockito.MockitoComponentMockingRule;

import org.xwiki.officeimporter.server.OfficeServerConfiguration;
import org.xwiki.officeimporter.server.OfficeServer;

import se.kreablo.tpk.office.internal.DefaultLetterMaker;

public class DefaultLetterMakerTest {

    private Process libreoffice;

    @Rule
    public MockitoComponentMockingRule<LetterMaker> componentManager =
        new MockitoComponentMockingRule<LetterMaker>(DefaultLetterMaker.class);

    @Before
    public void before() throws Exception {

        ProcessBuilder pb = new ProcessBuilder("libreoffice", "-accept=socket,host=127.0.0.1,port=8100;urp;", "-headless");
        libreoffice = pb.start();
        Thread.sleep(5000);

        LetterTemplateSource letterTemplateSource = componentManager.registerMockComponent(LetterTemplateSource.class);

        when(letterTemplateSource.getLetterTemplate())
            .thenReturn(this.getClass().getClassLoader()
                        .getResource("lettertemplate.docx").toString());

        OfficeServerConfiguration officeServerConfiguration = componentManager.registerMockComponent(OfficeServerConfiguration.class);

        when(officeServerConfiguration.getServerPort()).thenReturn(8100);


    }


    @After
    public void after() {
        libreoffice.destroy();
    }

    @Test
    public void testDefaultLetterMaker() throws Exception
    {
        LetterMaker letterMaker = componentManager.getInstance(LetterMaker.class);
        List<Map<String, String>> replacements = new LinkedList<Map<String, String>>() {

                private void a(String ...args) {
                    Map m = new HashMap(args.length / 2);
                    for (int i = 0; i < args.length; i += 2) {
                        m.put(args[i], args[i + 1]);
                    }
                    this.add(m);
                }

                {
                    a("foobar", "baz");
                    a("foobar", "quiz");
                }
            };
        File pdf = letterMaker.getPDFLetters(replacements);
    }
}
