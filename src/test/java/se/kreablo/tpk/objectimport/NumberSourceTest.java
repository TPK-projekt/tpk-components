/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.objectimport;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Locale;
import java.text.NumberFormat;
import java.text.DecimalFormat;

public class NumberSourceTest {

    @Test
    public void testNumberSource() throws Exception {
        FieldSource<String> source = new FieldSource<String>() {
                private int i = 0;

                private String [] test = { "", "0", "0,1", "1 000 000", "−1 000,9" };

                @Override
                public String await() {
                    return test[i++];
                    
                }
            };

        final NumberFormat numberFormat = NumberFormat.getInstance(Locale.forLanguageTag("sv"));

        FieldSource<Number> numberSource = new NumberSource(source,  numberFormat);

        Assert.assertTrue(numberFormat instanceof DecimalFormat);

        if (numberFormat instanceof DecimalFormat) {
            final DecimalFormat decimalFormat = (DecimalFormat) numberFormat;
            Assert.assertEquals(decimalFormat.getNegativePrefix(), "−");
        }

        Assert.assertNull(numberSource.await());
        Assert.assertEquals((Long)         0l,    numberSource.await());
        Assert.assertEquals((Double)       0.1,   numberSource.await());
        Assert.assertEquals((Long)   1000000l,    numberSource.await());
        Assert.assertEquals((Double)  (-1000.9),  numberSource.await());
    }
}

