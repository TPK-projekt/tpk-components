/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.objectimport;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CycleNumberTest {

    @Test
    public void testCycleNumber1() throws Exception {
        final CycleNumber cycleNumber = new CycleNumber();

        Assert.assertEquals(cycleNumber.getCycleNumber(), 0);

        final SharedCycleItem s1 = new SharedCycleItem(cycleNumber);

        s1.startCycle();
        s1.endCycle();

        Assert.assertEquals(cycleNumber.getCycleNumber(), 1);

        boolean caughtException = false;
        try {
            s1.endCycle();
        } catch (InvalidStateException e) {
            caughtException = true;
        }

        Assert.assertTrue(caughtException);
    }

    @Test
    public void testCycleNumber2() throws Exception {
        final CycleNumber cycleNumber = new CycleNumber();

        Assert.assertEquals(cycleNumber.getCycleNumber(), 0);

        final SharedCycleItem s1 = new SharedCycleItem(cycleNumber);
        final SharedCycleItem s2 = new SharedCycleItem(cycleNumber);

        s1.startCycle();
        s1.endCycle();

        Assert.assertEquals(cycleNumber.getCycleNumber(), 0);

        s2.startCycle();
        s2.endCycle();

        Assert.assertEquals(cycleNumber.getCycleNumber(), 1);

        s2.startCycle();
        s2.endCycle();

        Assert.assertEquals(cycleNumber.getCycleNumber(), 1);

        s1.startCycle();
        s1.endCycle();

        Assert.assertEquals(cycleNumber.getCycleNumber(), 2);

        s1.startCycle();
        s2.startCycle();

        Assert.assertEquals(cycleNumber.getCycleNumber(), 2);

        s1.endCycle();
        Assert.assertEquals(cycleNumber.getCycleNumber(), 2);

        s2.endCycle();
        Assert.assertEquals(cycleNumber.getCycleNumber(), 3);
        
        boolean caughtException = false;
        try {
            s1.endCycle();
        } catch (InvalidStateException e) {
            caughtException = true;
        }

        Assert.assertTrue(caughtException);
    }

    @Test
    public void testCycleNumber3() throws Exception {
        final CycleNumber cycleNumber = new CycleNumber();

        Assert.assertEquals(0, cycleNumber.getCycleNumber());

        final SharedCycleItem s1 = new SharedCycleItem(cycleNumber);
        final SharedCycleItem s2 = new SharedCycleItem(cycleNumber);
        final SharedCycleItem s3 = new SharedCycleItem(cycleNumber);

        s1.startCycle();
        Assert.assertEquals(0, cycleNumber.getCycleNumber());
        s2.startCycle();
        Assert.assertEquals(0, cycleNumber.getCycleNumber());
        s3.startCycle();
        Assert.assertEquals(0, cycleNumber.getCycleNumber());

        s1.endCycle();
        Assert.assertEquals(0, cycleNumber.getCycleNumber());
        s2.endCycle();
        Assert.assertEquals(0, cycleNumber.getCycleNumber());
        s3.endCycle();
        Assert.assertEquals(1, cycleNumber.getCycleNumber());
    }

}
