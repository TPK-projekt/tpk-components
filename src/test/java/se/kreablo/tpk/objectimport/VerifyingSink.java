package se.kreablo.tpk.objectimport;

import java.util.List;
import java.util.ArrayList;
import static java.util.Arrays.asList;

public class VerifyingSink<T> implements FieldSink<T> {

    private int startCycleCount;
    private int endCycleCount;

    private List<T> values = new ArrayList<T>();
    
    @Override
    public void yield(T value) {
            values.add(value);
    }

    @Override
    public void startCycle() {
        startCycleCount++;
    }

    @Override
    public void endCycle() {
        endCycleCount++;
    }

    public int getStartCycles() {
        return startCycleCount;
    }
    public int getEndCycles() {
        return endCycleCount;
    }
    public List<T> getValues() {
        return values;
    }

    @Override
    public String toString() {
        return "starts: " + startCycleCount + ", ends: " + endCycleCount + " values " + values;
    }
}
