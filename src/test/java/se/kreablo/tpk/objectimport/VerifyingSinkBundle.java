
package se.kreablo.tpk.objectimport;

import java.util.List;
import java.util.ArrayList;
import static java.util.Arrays.asList;

public class VerifyingSinkBundle implements CycleItem {

    private final CycleNumber cycleNumber = new CycleNumber();

    private final List<Sink<?>> sinks = new ArrayList<>();

    private int starts;

    private int ends;

    private Object [] record;

    private final List<Object []> records = new ArrayList<>();
    
    public VerifyingSinkBundle() {
        cycleNumber.addListener(this);
    }

    public <T> FieldSink<T> bundle(VerifyingSink<T> vsink) {
        Sink<T> s = new Sink(vsink, sinks.size());
        sinks.add(s);
        return s;
    }

    public <T> FieldConduit<T> conduit(FieldSource<T> source) {
        Sink<T> sink = new Sink<T>(new VerifyingSink<T>(), sinks.size());
        sinks.add(sink);
        return new FieldConduit<T>(source, sink);
    }

    @Override
    public void startCycle() {
        record = new Object[sinks.size()];
        starts++;
    }

    @Override
    public void endCycle() {
        records.add(record);
        record = null;
        ends++;
    }

    public int getStartCycles() {
        return starts;
    }

    public int getEndCycles() {
        return ends;
    }

    public List<Object []> getRecords() {
        return records;
    }
    
    private class Sink<T> extends SharedCycleItem implements FieldSink<T> {

        final VerifyingSink vsink;

        private final int i;

        public Sink(VerifyingSink vsink, int i) {
            super(cycleNumber);
            this.vsink = vsink;
            this.i = i;
        }

        @Override
        public void yield(T value) {
            record[i] = value;
            vsink.yield(value);
        }

        @Override
        public void start() throws ConduitException {
            vsink.startCycle();
        }

        @Override
        public void end() throws ConduitException {
            vsink.endCycle();
        }

    }
}
