/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.objectimport.tpk;

import org.junit.Test;
import org.junit.Rule;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.xwiki.test.mockito.MockitoComponentMockingRule;
import org.xwiki.component.util.DefaultParameterizedType;

import java.nio.charset.Charset;
import java.io.InputStream;
import java.io.InputStreamReader;

import se.kreablo.tpk.objectimport.ConduitException;
import se.kreablo.tpk.objectimport.FieldSource;
import se.kreablo.tpk.objectimport.EndConduitException;
import se.kreablo.tpk.objectimport.tpk.CSVSourceConfig;

public class CSVMeasureSourceTest {

    @Rule
    public final MockitoComponentMockingRule<MeasureSource<CSVSourceConfig>> mocker =
        new MockitoComponentMockingRule(CSVMeasureSource.class, new DefaultParameterizedType(null, MeasureSource.class, CSVSourceConfig.class), "csv");


    @Test
    public void testCSVMeasureSource() throws Exception {
        MeasureSource<CSVSourceConfig> source = mocker.getComponentUnderTest();

        InputStream measurescsv = getClass().getResourceAsStream("/measures.csv");

        assertNotNull(measurescsv);

        source.start(new CSVSourceConfig(new InputStreamReader(measurescsv, Charset.defaultCharset())));

        FieldSource<Integer> code = source.getCodeSource();

        FieldSource<Integer> version = source.getVersionSource();

        FieldSource<String> title = source.getTitleSource();
        
        FieldSource<Integer> price = source.getPriceSource();

        FieldSource<Integer> priceSpecial = source.getPriceSpecialSource();

        FieldSource<String> area = source.getAreaSource();

        FieldSource<String> areaSpecial = source.getAreaSpecialSource();

        FieldSource<?>[] all = {code, version, title, price, priceSpecial, area, areaSpecial };

        assertNotNull(code);
        assertNotNull(version);
        assertNotNull(title);
        assertNotNull(price);
        assertNotNull(priceSpecial);
        assertNotNull(area);
        assertNotNull(areaSpecial);

        for (FieldSource<?> fs : all) {
            fs.startCycle();
        }

        assertEquals((Integer) 101, code.await());
        assertEquals((Integer) 830, price.await());
        assertEquals((Integer) 830, priceSpecial.await());
        assertEquals("Undersökning, diagnostik (101–126, 141–164)", area.await());
        assertEquals("Undersökning, diagnostik (101–126, 141–164)", areaSpecial.await());
        assertEquals("Basundersökning och diagnostik, utförd av tandläkare", title.await());
        
        for (FieldSource<?> fs : all) {
            fs.endCycle();
        }
        for (FieldSource<?> fs : all) {
            fs.startCycle();
        }

        assertEquals((Integer) 103, code.await());
        assertEquals((Integer) 365, price.await());
        assertEquals((Integer) 455, priceSpecial.await());
        assertEquals("Undersökning, diagnostik (101–126, 141–164)", area.await());
        assertEquals("Undersökning, diagnostik (101–126, 141–164)", areaSpecial.await());
        assertEquals("Kompletterande eller akut undersökning, utförd av tandläkare", title.await());

        for (int i = 0; i < 178; i++) {
            for (FieldSource<?> fs : all) {
                fs.endCycle();
            }
            for (FieldSource<?> fs : all) {
                fs.startCycle();
            }
        }
        assertEquals((Integer) 941, code.await());
        assertEquals((Integer) 6585, price.await());
        assertEquals((Integer) 7470, priceSpecial.await());
        assertEquals("", area.await());
        assertEquals("", areaSpecial.await());
        assertEquals("Ortodontisk slutning av entandslucka istället för ersättningsberättigande tandstödd bro när åtgärd 800 eller 801 har utförts inom två år på en av stödtänderna som utbytet beräknas på", title.await());

        boolean caughtException = false;
        try {
            for (int i = 0; i < 178; i++) {
                for (FieldSource<?> fs : all) {
                    fs.endCycle();
                }
                for (FieldSource<?> fs : all) {
                    fs.startCycle();
                }
            }
        } catch (EndConduitException e) {
            caughtException = true;
        }

        assertTrue(caughtException);
        
    }
                                              
}
