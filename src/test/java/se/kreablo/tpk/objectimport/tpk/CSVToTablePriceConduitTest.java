/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.objectimport.tpk;

import org.junit.Test;
import org.junit.Rule;
import org.junit.Before;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.anyObject;
import static org.mockito.Mockito.anyBoolean;
import static org.mockito.Mockito.any;

import org.xwiki.test.mockito.MockitoComponentMockingRule;
import org.xwiki.component.util.DefaultParameterizedType;

import se.kreablo.tpk.objectimport.CountingFieldSource;
import se.kreablo.tpk.objectimport.ConstantFieldSource;
import se.kreablo.tpk.objectimport.TestSource;
import se.kreablo.tpk.objectimport.VerifyingSink;
import se.kreablo.tpk.objectimport.VerifyingSinkBundle;
import se.kreablo.tpk.objectimport.FieldSource;
import se.kreablo.util.Either;
import org.apache.commons.lang3.tuple.Pair;
import static java.util.Arrays.asList;
import java.util.Map;
import java.util.Set;
import java.util.HashSet;
import java.util.List;
import java.util.ArrayList;

public class CSVToTablePriceConduitTest {

    @Rule
    public final MockitoComponentMockingRule<PriceConduit<CSVSourceConfig>> mocker =
        new MockitoComponentMockingRule(CSVToTablePriceConduit.class, new DefaultParameterizedType(null, PriceConduit.class, CSVSourceConfig.class), "csvtotable");

    private PriceSource<CSVSourceConfig> mockPriceSource;

    private PriceSink mockPriceSink;

    private VerifyingSink<Integer> surgeryIdSink = new VerifyingSink<>();
    private VerifyingSink<Either<String, Pair<Boolean, Integer>>> idSink = new VerifyingSink<>();
    private VerifyingSink<Integer> versionSink = new VerifyingSink<>();
    private VerifyingSink<Integer> priceSink = new VerifyingSink<>();
    private VerifyingSinkBundle bundle;
    
    @Before
    public void setup() throws Exception {
        mockPriceSource = mocker.getInstance(new DefaultParameterizedType(null, PriceSource.class, CSVSourceConfig.class), "csv");

        mockPriceSink = mocker.getInstance(PriceSink.class, "table");

        when(mockPriceSource.getSurgeryIdSource()).thenReturn(new CountingFieldSource());
        when(mockPriceSource.getVersionSource()).thenReturn(new ConstantFieldSource(42));

        final TestSource<Integer> ts1 = new TestSource<>(new Integer [] { 10, 20, 30 });
        final TestSource<Integer> ts2 = new TestSource<>(new Integer [] { 15, 25, 35 });
        final TestSource<Integer> ts3 = new TestSource<>(new Integer [] { 18, 28, 38 });

        final FieldSource<Either<String, Pair<Boolean, Integer>>> id1 =
            new ConstantFieldSource<>(Either.left("A1"));
        final FieldSource<Either<String, Pair<Boolean, Integer>>> id2 =
            new ConstantFieldSource<>(Either.right(Pair.of(false, 100)));
        final FieldSource<Either<String, Pair<Boolean, Integer>>> id3 =
            new ConstantFieldSource<>(Either.right(Pair.of(true, 200)));

        final Pair [] ids = {
            Pair.of(id1, ts1),
            Pair.of(id2, ts2),
            Pair.of(id3, ts3)
        };
        
        when(mockPriceSource.getLabeledPriceSources(anyBoolean())).thenReturn(asList(ids));

        surgeryIdSink = new VerifyingSink<>();
        idSink = new VerifyingSink<>();
        versionSink = new VerifyingSink<>();
        priceSink = new VerifyingSink<>();


        bundle = new VerifyingSinkBundle();
        
        when(mockPriceSink.getSurgeryIdSink()).thenReturn(bundle.bundle(surgeryIdSink));
        when(mockPriceSink.getItemIdSink()).thenReturn(bundle.bundle(idSink));
        when(mockPriceSink.getVersionSink()).thenReturn(bundle.bundle(versionSink));
        when(mockPriceSink.getPriceSink()).thenReturn(bundle.bundle(priceSink));
    }

    @Test
    public void testPriceConduit() throws Exception {

        PriceConduit<CSVSourceConfig> conduit = mocker.getComponentUnderTest();

        

        Map<String, Object> ret = conduit.run(null, false, c -> {});

        verify(mockPriceSource).start(anyObject());

        assertEquals(9, surgeryIdSink.getStartCycles());
        assertEquals(9, idSink.getStartCycles());
        assertEquals(9, versionSink.getStartCycles());
        assertEquals(9, priceSink.getStartCycles());

        assertEquals(9, surgeryIdSink.getEndCycles());
        assertEquals(9, idSink.getEndCycles());
        assertEquals(9, versionSink.getEndCycles());
        assertEquals(9, priceSink.getEndCycles());

        Object [][] expectedRecords = {
            { 1, Either.left("A1"), 42, 10 },
            { 2, Either.left("A1"), 42, 20 },
            { 3, Either.left("A1"), 42, 30 },
            { 1, Either.right(Pair.of(false, 100)), 42, 15 },
            { 2, Either.right(Pair.of(false, 100)), 42, 25 },
            { 3, Either.right(Pair.of(false, 100)), 42, 35 },
            { 1, Either.right(Pair.of(true,  200)), 42, 18 },
            { 2, Either.right(Pair.of(true,  200)), 42, 28 },
            { 3, Either.right(Pair.of(true,  200)), 42, 38 }
        };
        
        assertEquals(9, bundle.getStartCycles());
        assertEquals(9, bundle.getEndCycles());

        List<Object[]> records = bundle.getRecords();

        assertEquals(expectedRecords.length, records.size());
        
        Set<List<Object>> a = new HashSet<>();
        Set<List<Object>> b = new HashSet<>();

        for (int i = 0; i < expectedRecords.length; i++) {
            Object [] ea = expectedRecords[i];
            Object [] rb = records.get(i);

            a.add(asList(ea));
            b.add(asList(rb));
        }

        assertEquals(a, b);
        
    }
}
