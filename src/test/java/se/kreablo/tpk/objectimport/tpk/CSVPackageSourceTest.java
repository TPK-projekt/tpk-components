/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.objectimport.tpk;

import org.junit.Test;
import org.junit.Rule;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.xwiki.test.mockito.MockitoComponentMockingRule;
import org.xwiki.component.util.DefaultParameterizedType;

import java.nio.charset.Charset;
import java.io.InputStream;
import java.io.InputStreamReader;

import se.kreablo.tpk.objectimport.ConduitException;
import se.kreablo.tpk.objectimport.FieldSource;
import se.kreablo.tpk.objectimport.EndConduitException;
import se.kreablo.tpk.objectimport.tpk.CSVSourceConfig;

public class CSVPackageSourceTest {

    @Rule
    public final MockitoComponentMockingRule<PackageSource<CSVSourceConfig>> mocker =
        new MockitoComponentMockingRule(CSVPackageSource.class, new DefaultParameterizedType(null, PackageSource.class, CSVSourceConfig.class), "csv");


    @Test
    public void testCSVPackageSource() throws Exception {
        PackageSource<CSVSourceConfig> source = mocker.getComponentUnderTest();

        InputStream measurescsv = getClass().getResourceAsStream("/packages.csv");

        assertNotNull(measurescsv);

        source.start(new CSVSourceConfig(new InputStreamReader(measurescsv, Charset.defaultCharset())));

        FieldSource<Integer> code = source.getCodeSource();

        FieldSource<Integer> version = source.getVersionSource();

        FieldSource<String> textCode = source.getTextCodeSource();

        FieldSource<String> title = source.getTitleSource();

        FieldSource<String> area = source.getAreaSource();

        FieldSource<String> description = source.getDescriptionSource();

        FieldSource<String> descriptionMore = source.getDescriptionMoreSource();

        FieldSource<String> measures = source.getMeasuresSource();

        FieldSource<Integer> price = source.getPriceSource();

        FieldSource<?>[] all = { code, version, textCode, title, area, description, descriptionMore, measures, price };

        assertNotNull(code);
        assertNotNull(version);
        assertNotNull(textCode);
        assertNotNull(title);
        assertNotNull(area);
        assertNotNull(description);
        assertNotNull(descriptionMore);
        assertNotNull(measures);
        assertNotNull(price);

        for (FieldSource<?> fs : all) {
            fs.startCycle();
        }

        assertEquals((Integer) 1, code.await());
        assertEquals((Integer) 1295, price.await());
        assertEquals("A1", textCode.await());
        assertEquals("Undersökning", area.await());
        assertEquals("Basundersökning hos tandläkare och tandstensborttagning", title.await());
        assertEquals("", description.await());
        assertEquals("Beskrivning mer", descriptionMore.await());
        assertEquals("Åtgärder som ingår: 101, 341", measures.await());
        
        for (FieldSource<?> fs : all) {
            fs.endCycle();
        }
        for (FieldSource<?> fs : all) {
            fs.startCycle();
        }

        assertEquals((Integer) 2, code.await());
        assertEquals((Integer) 1080, price.await());
        assertEquals("A2", textCode.await());
        assertEquals("Undersökning", area.await());
        assertEquals("Basundersökning hos tandhygienist och tandstensborttagning", title.await());
        assertEquals("", description.await());
        assertEquals("Åtgärder som ingår: 111, 341", measures.await());

        for (int i = 0; i < 32; i++) {
            for (FieldSource<?> fs : all) {
                fs.endCycle();
            }
            for (FieldSource<?> fs : all) {
                fs.startCycle();
            }
        }

        assertEquals((Integer) 34, code.await());
        assertEquals((Integer) 15345, price.await());
        assertEquals("P1", textCode.await());
        assertEquals("Prisnivå", area.await());
        assertEquals("Prisnivå för 10 vanliga åtgärder", title.await());
        assertEquals("Prisnivå baserad på åtgärder för undersökningar, tandstensborttagning, fyllningar, tandborttagningar, rotfyllning,  och tandstödd krona.", description.await());
        assertEquals("Åtgärder som ingår: 101, 103, 341, 401, 402, 501, 704, 705, 706, 801", measures.await());

        boolean caughtException = false;
        try {
            for (int i = 0; i < 178; i++) {
                for (FieldSource<?> fs : all) {
                    fs.endCycle();
                }
                for (FieldSource<?> fs : all) {
                    fs.startCycle();
                }
            }
        } catch (EndConduitException e) {
            caughtException = true;
        }

        assertTrue(caughtException);
        
    }
                                              
}
