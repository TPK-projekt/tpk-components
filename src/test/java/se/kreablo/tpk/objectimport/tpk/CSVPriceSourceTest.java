/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.objectimport.tpk;

import org.junit.Test;
import org.junit.Rule;
import org.junit.Before;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doAnswer;
import org.mockito.ArgumentCaptor;

import org.xwiki.test.mockito.MockitoComponentMockingRule;
import org.xwiki.component.util.DefaultParameterizedType;

import java.nio.charset.Charset;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Set;
import java.util.HashSet;
import static java.util.Arrays.asList;
import org.apache.commons.lang3.tuple.Pair;
import java.util.List;
import java.util.ArrayList;
import se.kreablo.util.Either;

import se.kreablo.tpk.objectimport.ConduitException;
import se.kreablo.tpk.objectimport.FieldSource;
import se.kreablo.tpk.objectimport.FieldSink;
import se.kreablo.tpk.objectimport.FieldConduit;
import se.kreablo.tpk.objectimport.RecordConduit;
import se.kreablo.tpk.objectimport.VerifyingSink;
import se.kreablo.tpk.objectimport.VerifyingSinkBundle;
import se.kreablo.tpk.objectimport.EndConduitException;
import se.kreablo.tpk.objectimport.StopCycleException;
import se.kreablo.tpk.objectimport.tpk.CSVSourceConfig;

public class CSVPriceSourceTest {

    @Rule
    public final MockitoComponentMockingRule<PriceSource<CSVSourceConfig>> mocker =
        new MockitoComponentMockingRule(CSVPriceSource.class, new DefaultParameterizedType(null, PriceSource.class, CSVSourceConfig.class), "csv");

    @Before
    public void setup() throws Exception {

        CodeChecker<String> packageCodeChecker = mocker.getInstance(new DefaultParameterizedType(null, CodeChecker.class, String.class), "package");
        CodeChecker<Integer> measureCodeChecker = mocker.getInstance(new DefaultParameterizedType(null, CodeChecker.class, Integer.class), "measure");

        NextVersionSource nextVersionSource = mocker.getInstance(NextVersionSource.class);
            
        final String []  vs = { "A1", "B2", "C3" };
        
        final Set<String> validStrings = new HashSet<>(asList(vs));

        final Integer [] vm = { 101, 126, 122 };

        final Set<Integer> validMeasures = new HashSet<>(asList(vm));

        final ArgumentCaptor<String> scode = ArgumentCaptor.forClass(String.class);
        final ArgumentCaptor<Integer> mcode = ArgumentCaptor.forClass(Integer.class);
        
        doAnswer(i -> validStrings.contains(scode.getValue())).when(packageCodeChecker).check(scode.capture());

        doAnswer(i -> validMeasures.contains(mcode.getValue())).when(measureCodeChecker).check(mcode.capture());

        when(nextVersionSource.await()).thenReturn(42);
        doNothing().when(nextVersionSource).startCycle();
        doNothing().when(nextVersionSource).endCycle();

    }

    
    @Test
    public void testCSVPriceSource() throws Exception {
        PriceSource<CSVSourceConfig> source = mocker.getComponentUnderTest();

        InputStream pricescsv = getClass().getResourceAsStream("/prices.csv");

        assertNotNull(pricescsv);

        source.start(new CSVSourceConfig(new InputStreamReader(pricescsv, Charset.defaultCharset())));

        final List<FieldSource<?>> allSources = new ArrayList<>();

        final FieldSource<Integer> surgeryIdSource = source.getSurgeryIdSource();
        final FieldSource<Integer> versionSource = source.getVersionSource();
        final List<Pair<FieldSource<Either<String, Pair<Boolean, Integer>>>,
            FieldSource<Integer>>> priceSources = source.getLabeledPriceSources(false);

        allSources.add(surgeryIdSource);
        allSources.add(versionSource);

        final VerifyingSink<Integer> surgeryIdSink = new VerifyingSink<>();
        final VerifyingSink<Integer> versionSink = new VerifyingSink<>();
        final VerifyingSink<Integer> priceSink = new VerifyingSink<>();
        final VerifyingSink<Either<String, Pair<Boolean, Integer>>> idSink = new VerifyingSink<>();

        assertEquals(6, priceSources.size());

        final VerifyingSinkBundle b = new VerifyingSinkBundle();

        final FieldSink<Integer> bSurgeryIdSink = b.bundle(surgeryIdSink);
        final FieldSink<Integer> bVersionSink = b.bundle(versionSink);
        final FieldSink<Either<String, Pair<Boolean, Integer>>> bIdSink = b.bundle(idSink);
        final FieldSink<Integer> bPriceSink = b.bundle(priceSink);

        final List<RecordConduit> conduits = new ArrayList<>(priceSources.size());
        
        for (Pair<FieldSource<Either<String, Pair<Boolean, Integer>>>, FieldSource<Integer>> ps : priceSources) {
            allSources.add(ps.getLeft());
            allSources.add(ps.getRight());

            final FieldConduit<Integer> surgeryIdConduit =
                new FieldConduit<>(false, surgeryIdSource, true, bSurgeryIdSink);

            final FieldConduit<Integer> versionConduit =
                new FieldConduit<>(false, versionSource, true, bVersionSink);

            final FieldConduit<Either<String, Pair<Boolean, Integer>>> itemIdConduit =
                new FieldConduit<>(false, ps.getLeft(), true, bIdSink);

            final FieldConduit<Integer> priceConduit = new FieldConduit<>(false, ps.getRight(), true, bPriceSink);

            conduits.add(new RecordConduit(surgeryIdConduit, versionConduit, itemIdConduit, priceConduit));
        }

        boolean wasEnded = false;
        while (true) {
            try {
                for (FieldSource<?> s : allSources) {
                    s.startCycle();
                }
                for (RecordConduit r : conduits) {
                    r.tick();
                }
                for (FieldSource<?> s : allSources) {
                    s.endCycle();
                }
            } catch (StopCycleException e) {
                assertTrue(false);
            } catch (EndConduitException e) {
                wasEnded = true;
                break;
            }
        }

        assertTrue(wasEnded);
        assertEquals(9 * priceSources.size(), b.getStartCycles());
        assertEquals(9 * priceSources.size(), b.getEndCycles());

        Object [][] expected = {
            { 1, 42, Either.left("A1"), 1160 },
            { 2, 42, Either.left("A1"), null },
            { 5, 42, Either.left("A1"), 1320 },
            { 6, 42, Either.left("A1"), 1270 },
            { 8, 42, Either.left("A1"), 1250 },
            { 9, 42, Either.left("A1"), null },
            { 10, 42, Either.left("A1"), null  },
            { 11, 42, Either.left("A1"), 1095 },
            { 13, 42, Either.left("A1"), 1460 },
            { 1, 42, Either.left("B2"), 4170 },
            { 2, 42, Either.left("B2"), null },
            { 5, 42, Either.left("B2"), 4320 },
            { 6, 42, Either.left("B2"), 3840 },
            { 8, 42, Either.left("B2"), 3840 },
            { 9, 42, Either.left("B2"), 3840 },
            { 10, 42, Either.left("B2"), 3670 },
            { 11, 42, Either.left("B2"), 4107 },
            { 13, 42, Either.left("B2"), 4230 },
            { 1, 42, Either.left("C3"), null },
            { 2, 42, Either.left("C3"), null },
            { 5, 42, Either.left("C3"), 7615 },
            { 6, 42, Either.left("C3"), 7365 },
            { 8, 42, Either.left("C3"), null },
            { 9, 42, Either.left("C3"), null },
            { 10, 42, Either.left("C3"), null },
            { 11, 42, Either.left("C3"), 7092 },
            { 13, 42, Either.left("C3"), 8010 },
            { 1, 42, Either.right(Pair.of(false, 101)), 775 },
            { 2, 42, Either.right(Pair.of(false, 101)), null },
            { 5, 42, Either.right(Pair.of(false, 101)), 775 },
            { 6, 42, Either.right(Pair.of(false, 101)), 800 },
            { 8, 42, Either.right(Pair.of(false, 101)), 830 },
            { 9, 42, Either.right(Pair.of(false, 101)), 775 },
            { 10, 42, Either.right(Pair.of(false, 101)), 775  },
            { 11, 42, Either.right(Pair.of(false, 101)), 900 },
            { 13, 42, Either.right(Pair.of(false, 101)), 850 },
            { 1, 42, Either.right(Pair.of(false, 126)), null },
            { 2, 42, Either.right(Pair.of(false, 126)), null },
            { 5, 42, Either.right(Pair.of(false, 126)), null },
            { 6, 42, Either.right(Pair.of(false, 126)), null },
            { 8, 42, Either.right(Pair.of(false, 126)), 1115 },
            { 9, 42, Either.right(Pair.of(false, 126)), null },
            { 10, 42, Either.right(Pair.of(false, 126)), null },
            { 11, 42, Either.right(Pair.of(false, 126)), null },
            { 13, 42, Either.right(Pair.of(false, 126)), null },
            { 1, 42, Either.right(Pair.of(false, 122)), 250 },
            { 2, 42, Either.right(Pair.of(false, 122)), null },
            { 5, 42, Either.right(Pair.of(false, 122)), 220 },
            { 6, 42, Either.right(Pair.of(false, 122)), 250 },
            { 8, 42, Either.right(Pair.of(false, 122)), 260 },
            { 9, 42, Either.right(Pair.of(false, 122)), 220 },
            { 10, 42, Either.right(Pair.of(false, 122)), 220 },
            { 11, 42, Either.right(Pair.of(false, 122)), null },
            { 13, 42, Either.right(Pair.of(false, 122)), 220 }
        };

        List<Object[]> records = b.getRecords();

        assertEquals(expected.length, records.size());
        
        Set<List<Object>> sa = new HashSet<>();
        Set<List<Object>> sb = new HashSet<>();

        for (int i = 0; i < expected.length; i++) {
            Object [] ea = expected[i];
            Object [] rb = records.get(i);

            sa.add(asList(ea));
            sb.add(asList(rb));
        }

        Set<List<Object>> db = new HashSet<>(sb);
        db.removeAll(sa);

        assertEquals(new HashSet<>(), db);

        Set<List<Object>> da = new HashSet<>(sa);
        da.removeAll(sb);

        assertEquals(new HashSet<>(), da);

        assertEquals(sa, sb);
        
    }

}

