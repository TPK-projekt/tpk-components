/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.objectimport.tpk;

import org.junit.Test;
import org.junit.Rule;
import org.junit.Before;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doAnswer;
import org.mockito.ArgumentCaptor;

import org.xwiki.test.mockito.MockitoComponentMockingRule;
import org.xwiki.component.util.DefaultParameterizedType;

import java.nio.charset.Charset;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Set;
import java.util.HashSet;
import static java.util.Arrays.asList;
import org.apache.commons.lang3.tuple.Pair;
import java.util.List;
import java.util.ArrayList;
import se.kreablo.util.Either;

import se.kreablo.tpk.objectimport.ConduitException;
import se.kreablo.tpk.objectimport.FieldSource;
import se.kreablo.tpk.objectimport.FieldSink;
import se.kreablo.tpk.objectimport.FieldConduit;
import se.kreablo.tpk.objectimport.RecordConduit;
import se.kreablo.tpk.objectimport.VerifyingSink;
import se.kreablo.tpk.objectimport.VerifyingSinkBundle;
import se.kreablo.tpk.objectimport.EndConduitException;
import se.kreablo.tpk.objectimport.StopCycleException;
import se.kreablo.tpk.objectimport.tpk.CSVSourceConfig;

public class CSVSurgerySourceTest {

    @Rule
    public final MockitoComponentMockingRule<SurgerySource<CSVSourceConfig>> mocker =
        new MockitoComponentMockingRule(CSVSurgerySource.class, new DefaultParameterizedType(null, SurgerySource.class, CSVSourceConfig.class), "csv");

    @Before
    public void setup() throws Exception {

        GeographicInfoService geo = mocker.getInstance(GeographicInfoService.class, "google");

        GeographicInfo g1 = new GeographicInfo();
        g1.setLatitude(1.0);
        g1.setLongitude(1.5);
        g1.setMunicipality("mun1");
        g1.setSublocality("sub1");
        g1.setCounty("county1");
        GeographicInfo g2 = new GeographicInfo();
        g2.setLatitude(2.0);
        g2.setLongitude(2.5);
        g2.setMunicipality("mun2");
        g2.setSublocality("sub2");
        g2.setCounty("county2");
        GeographicInfo g3 = new GeographicInfo();
        g3.setLatitude(3.0);
        g3.setLongitude(3.5);
        g3.setMunicipality("mun3");
        g3.setSublocality("sub3");
        g3.setCounty(null);
        GeographicInfo g4 = new GeographicInfo();
        g4.setLatitude(4.0);
        g4.setLongitude(4.5);
        g4.setMunicipality("mun4");
        g4.setSublocality("sub4");
        g4.setCounty("county4");
        GeographicInfo g5 = new GeographicInfo();
        g5.setLatitude(5.0);
        g5.setLongitude(5.5);
        g5.setMunicipality("mun5");
        g5.setSublocality("sub5");
        g5.setCounty("county5");
        GeographicInfo g6 = new GeographicInfo();
        g6.setLatitude(6.0);
        g6.setLongitude(6.5);
        g6.setMunicipality("mun6");
        g6.setSublocality("sub6");
        g6.setCounty("county6");
        GeographicInfo g7 = new GeographicInfo();
        g7.setLatitude(7.0);
        g7.setLongitude(7.5);
        g7.setMunicipality("mun7");
        g7.setSublocality("sub7");
        g7.setCounty("county7");
        GeographicInfo g8 = new GeographicInfo();
        g8.setLatitude(8.0);
        g8.setLongitude(8.5);
        g8.setMunicipality("mun8");
        g8.setSublocality("sub8");
        g8.setCounty("county8");
        GeographicInfo g9 = new GeographicInfo();
        g9.setLatitude(9.0);
        g9.setLongitude(9.5);
        g9.setMunicipality("mun9");
        g9.setSublocality("sub9");
        g9.setCounty("county9");

        when(geo.getFromAddress("Östra Järnvägsgatan 18", "Sävsjö", "57631")).thenReturn(g1);
        //when(geo.getFromAddress("Distansgatan 2", "Västra Frölunda", "42174")).thenReturn(g2);
        when(geo.getFromAddress("Väderilsgatan 50", "Göteborg", "41836")).thenReturn(g3);
        when(geo.getFromAddress("Borgmästargränd 1d", "Jönköping", "55320")).thenReturn(g4);
        when(geo.getFromAddress("Kyrkogatan 21", "Sundsvall", "85232")).thenReturn(g5);
        when(geo.getFromAddress("Parkvägen 7", "Österbybruk", "74831")).thenReturn(g6);
        when(geo.getFromAddress("Stupvägen 1", "Sollentuna", "19142")).thenReturn(g7);
        when(geo.getFromAddress("Stora gatan 41", "Västerås", "72212")).thenReturn(g8);
        when(geo.getFromAddress("Baltzarsgatan 31", "Malmö", "21136")).thenReturn(g9);
    }

    @Test
    public void testCSVSurgerySource() throws Exception {
        SurgerySource<CSVSourceConfig> source = mocker.getComponentUnderTest();

        InputStream surgerycsv = getClass().getResourceAsStream("/surgeries.csv");

        assertNotNull(surgerycsv);

        source.start(new CSVSourceConfig(new InputStreamReader(surgerycsv, Charset.defaultCharset())));

        VerifyingSinkBundle b = new VerifyingSinkBundle();

        FieldConduit<Integer> id = b.conduit(source.getIdSource());

        FieldConduit<String> name = b.conduit(source.getNameSource());

        FieldConduit<String> company =  b.conduit(source.getCompanySource());

        FieldConduit<Double> latitude =  b.conduit(source.getLatitudeSource());

        FieldConduit<Double> longitude = b.conduit(source.getLongitudeSource());

        FieldConduit<String> address = b.conduit(source.getAddressSource());

        FieldConduit<String> postal = b.conduit(source.getPostalCodeSource());

        FieldConduit<String> city = b.conduit(source.getCitySource());

        FieldConduit<String> muni = b.conduit(source.getMunicipalitySource());

        FieldConduit<String> county = b.conduit(source.getCountySource());

        FieldConduit<String> sublocal = b.conduit(source.getSublocalitySource());

        FieldConduit<String> category = b.conduit(source.getCategorySource());
        

        final RecordConduit r = new RecordConduit(id, name,company, latitude,
                                                  longitude, address, postal,
                                                  city, muni, county,sublocal,
                                                  category);

        r.run();

        Object [][] expected = {
            { (Integer) 1, "Mottagning 1" , "Vårdgivare 1", (Double) 1.0, (Double) 1.5, "Östra Järnvägsgatan 18", "Sävsjö", "57631", "mun1", "county1", "sub1", "Privat" },
            { (Integer) 2, "Mottagning 2" , "Vårdgivare 2", null,         null,         "Distansgatan 2", "Västra Frölunda", "42174", null, "Västra Götalands län", null, "Privat" },
            { (Integer) 5, "Mottagning 3" , "Vårdgivare 3", (double) 3.0, (double) 3.5, "Väderilsgatan 50", "Göteborg", "41836", "mun3", "Västra Götalands län", "sub3", "Privat" },
            { (Integer) 6, "Mottagning 4" , "Vårdgivare 4", (double) 4.0, (double) 4.5, "Borgmästargränd 1d", "Jönköping", "55320", "mun4", "county4", "sub4", "Privat" },
            { (Integer) 8, "Mottagning 5" , "Vårdgivare 5", (double) 5.0, (double) 5.5, "Kyrkogatan 21", "Sundsvall", "85232", "mun5", "county5", "sub5", "Privat" },
            { (Integer) 9, "Mottagning 6" , "Vårdgivare 6", (double) 6.0, (double) 6.5, "Parkvägen 7", "Österbybruk", "74831", "mun6", "county6", "sub6", "Privat" },
            { (Integer) 10, "Mottagning 7" , "Vårdgivare  7", (double) 7.0, (double) 7.5, "Stupvägen 1", "Sollentuna", "19142", "mun7", "county7", "sub7", "Landsting" },
            { (Integer) 11, "Mottagning 8" , "Vårdgivare  8", (double) 8.0, (double) 8.5, "Stora gatan 41", "Västerås", "72212", "mun8", "county8", "sub8", "Privat" },
            { (Integer) 13, "Mottagning 9" , "Vårdgivare 9", (double) 9.0, (double) 9.5, "Baltzarsgatan 31", "Malmö", "21136", "mun9", "county9", "sub9", "Privat" }
        };


        List<Object[]> records = b.getRecords();

        assertEquals(expected.length, records.size());
        
        Set<List<Object>> sa = new HashSet<>();
        Set<List<Object>> sb = new HashSet<>();

        for (int i = 0; i < expected.length; i++) {
            Object [] ea = expected[i];
            Object [] rb = records.get(i);

            List<Object> aa = asList(ea);
            List<Object> ab = asList(rb);

            for (int j = 0; j < aa.size(); j++) {
                assertEquals(aa.get(j), ab.get(j));
            }

            sa.add(aa);
            sb.add(ab);
        }

        Set<List<Object>> db = new HashSet<>(sb);
        db.removeAll(sa);

        assertEquals(new HashSet<>(), db);

        Set<List<Object>> da = new HashSet<>(sa);
        da.removeAll(sb);

        assertEquals(new HashSet<>(), da);

        assertEquals(sa, sb);

        
    }
}
