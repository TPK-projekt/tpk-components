/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.objectimport;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.ArrayList;
import static java.util.Arrays.asList;

public class RecordConduitTest {


    @Test
    public void testRecordConduit() throws Exception {

        String [] a1 = {"foo1", "bar1", "baz1"};
        String [] a2 = {"foo2", "bar2", "baz2"};
        String [] a3 = {"foo3", "bar3", "baz3"};
        String [] a4 = {"foo4", "bar4", "baz4"};

        TestSource<String> s1 = new TestSource<>(a1);
        TestSource<String> s2 = new TestSource<>(a2);
        TestSource<String> s3 = new TestSource<>(a3);
        TestSource<String> s4 = new TestSource<>(a4);


        TestSink t1 = new TestSink();
        TestSink t2 = new TestSink();
        TestSink t3 = new TestSink();
        TestSink t4 = new TestSink();

        FieldSource<?>[] sources = {s1, s2, s3, s4};
        FieldSink<?>[]   sinks   = {t1, t2, t3, t4};

        RecordConduit rc = new RecordConduit(sources, sinks);

        List<Exception> exceptions = rc.run();

        Assert.assertEquals(0, exceptions.size());
        Assert.assertEquals(asList(a1), t1.values);
        Assert.assertEquals(asList(a2), t2.values);
        Assert.assertEquals(asList(a3), t3.values);
        Assert.assertEquals(asList(a4), t4.values);
    }

    private static class TestSink implements FieldSink<String> {
        public List<String> values = new ArrayList<>();
        @Override
        public void yield(String v) {
            values.add(v);
        }
        @Override
        public void startCycle() {
        }

        @Override
        public void endCycle() {
        }
        
    }
}
