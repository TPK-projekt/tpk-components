/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package se.kreablo.tpk.objectimport;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class NonNullSourceTest {

    @Test
    public void testNonNullSource() throws Exception {

        FieldSource<String> source1 = new ConstantFieldSource<String>("hello");
        FieldSource<String> source2 = new ConstantFieldSource<String>(null);

        FieldSource<String> nonNullSource1 = new NonNullSource(source1);
        FieldSource<String> nonNullSource2 = new NonNullSource(source2);

        Assert.assertNull(source2.await());
        Assert.assertEquals("hello", nonNullSource1.await());
        boolean caughtException = false;
        try {
            nonNullSource2.await();
        } catch (StopCycleException e) {
            caughtException = true;
        }

        Assert.assertTrue(caughtException);
    }
}
