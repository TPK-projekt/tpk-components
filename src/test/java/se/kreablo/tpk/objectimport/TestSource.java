package se.kreablo.tpk.objectimport;

public class TestSource<T> implements FieldSource<T> {
    final T [] values;
    private int i = 0;
    public TestSource(T [] values) {
        this.values = values;
    }

    @Override
    public T await() {
        return values[i];
    }

    @Override
    public void startCycle() throws ConduitException {
        if (i >= values.length) {
            throw new EndConduitException();
        }
    }

    @Override
    public void endCycle() {
        i++;
    }
}
